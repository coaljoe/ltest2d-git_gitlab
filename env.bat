@echo off
rem setlocal
rem set PATH=%PATH%;bin\lib\dll
set HOME=%USERPROFILE%
set PATH=%CD%\dist;%PATH%
rem set PATH=%PATH%;%CD%\bin\lib\freetype;%CD%\dist
rem set PATH=%PATH%;%HOME%\k\progs\go\bin
rem set PATH=%PATH%;%HOME%\k\progs\winbuilds\bin
rem set LIBRARY_PATH=%CD%\bin\lib\freetype
rem set GOPATH=%CD%\..\go;%CD%
rem if "%GOROOT%" == "" (
rem   set GOROOT=%HOME%\k\progs\go
rem )
rem set GOMAXPROCS=0
rem set GODEBUG=cgocheck=2
set DEBUG=1

echo %LIBRARY_PATH%

if "%LOGLEVEL%" == "" (
  rem set LOGLEVEL=INFO
  set LOGLEVEL=DEBUG
)

@rem set mingw env
rem set PATH=C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin;%PATH%
rem if exist C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin (
rem set PATH=C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin;%PATH%
rem ) else if exist C:\users\k\k\progs\mingw-w64\x86_64-6.2.0-posix-seh-rt_v5-rev1\mingw64\bin (
rem   set PATH=C:\users\k\k\progs\mingw-w64\x86_64-6.2.0-posix-seh-rt_v5-rev1\mingw64\bin;%PATH%
rem ) else if exist C:\Users\k\k\progs\winbuilds\bin (
rem   set PATH=C:\Users\k\k\progs\winbuilds\bin;%PATH%
rem )

@rem set apitrace path
set PATH=C:\Users\k\k\progs\apitrace\bin;%PATH%

rem endlocal
