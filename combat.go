package main_module

import (
	. "kristallos.ga/rx/math"
)

type CombatHostI interface {
	// Transform
	cellPosX() int
	cellPosY() int
	pos2() Vec2
	getView() ViewI
	getObjType() string
	getRealm() RealmType
	getHealth() *Health
	GetPlayer() *Player
	getCombat() *Combat
	explode()
}

// component
type Combat struct {
	// TODO add ids?
	host   CombatHostI
	target CombatHostI
	//health *Health // link
	//fireRate float64
	defenceRating int
	weapons       []*Weapon

	//targetedBy 	  map[*Combat]bool
}

//func newCombat(host CombatHostI, health *Health) *Combat {
func newCombat(host CombatHostI) *Combat {
	c := &Combat{
		host: host,
		//health: health,
		//fireRate: 1.0,
		defenceRating: 0,
	}
	c.weapons = append(c.weapons, newWeapon(c))
	c.weapons = append(c.weapons, newWeapon(c))

	return c
}

func (c *Combat) start() {}

func (c *Combat) takeDamage(amt int) {
	//h := c.health
	h := c.host.getHealth()
	h.takeHealth(amt)
	//v := h.health() - amt
	//h.setHealth(v)
	if !h.isAlive() {
		c.host.explode()
		//c.host.destroy()
		//panic("fixme: not implemented")
		//pub(ev_entity_destroy, GetEntity(c))
	}
}

func (c *Combat) getWeapons() []*Weapon {
	return c.weapons
}

/*
func (c *Combat) canAttack(t CombatHostI) bool {
	if c.host.getRealm() != t.getRealm() {
		return false
	}
	return true
}
*/
//func (c *Combat) canAttack(t *Combat) bool {
func (c *Combat) canAttack(t CombatHostI) bool {
	_log.Dbg("%F")
	/*
		if c.host.getRealm() != t.host.getRealm() {
			return false
		}
	*/

	//tRealm := t.host.getRealm()
	tRealm := t.getRealm()
	p("tRealm:", tRealm)

	//pdump(c.weapons)

	for _, weapon := range c.weapons {
		//p("-> weapon:", weapon)
		// ... So it can attack this realm
		if weapon.canAttackRealm(tRealm) {
			return true
		}
	}

	return false
}

// Try to set target for combat and all weapons
func (c *Combat) setTarget(t CombatHostI) {
	c.target = t

	for _, weapon := range c.weapons {
		weapon.setTarget(t)
	}
}

func (c *Combat) unsetTarget() {
	c.target = nil
}

/*
func (c *Combat) unsetTarget() {
	(ttc.target).getTtHost().removeTargetedBy(ttc.target)
}

func (c *Combat) addTargetedBy(att *Combat) {
	c.targetedBy[att] = true
}

func (c *Combat) removeTargetedBy(att *ecs.Entity) {
	delete(tth.targetedBy, att)
}

func (c *Combat) cleanTargetedBy() {
	_log.Dbg("-- cleanTargetBy:")
	_log.Dbg("len targetedBy ", len(tth.targetedBy))
	for att, cl := range tth.targetedBy {
		//println("derp")
		//cl.target = nil
		p(att, cl.target)

		c_Combat(cl.target).unsetTargets()
		c_Combat(att).unsetTargets()

		cl.unsetTarget()
		//tth.removeTargetedBy(att)
		//cl.SetTarget(nil)
		var _ = att
	}
	_log.Dbg("len targetedBy ", len(tth.targetedBy))
	_log.Dbg("-- end")
}
*/

// Do/perform basic attack checks
func (c *Combat) attackChecks(t CombatHostI) bool {
	host := c.host
	target := t

	// Same object
	if host == target {
		return false
	}
	// Same camp
	if host.GetPlayer().camp.Id == target.GetPlayer().camp.Id {
		return false
	}

	return true
}

// Attack all targets
func (c *Combat) attackTargets() {
	_log.Dbg("%F")

	// Attack targets
	println("attack target, IsAlive", c.target.getHealth().isAlive())
	for i, w := range c.weapons {
		_ = i
		//p(wh.weapon.CanFire())
		println("-> i:", i)
		p("w:", w, "t:", w.target)

		canAttack := false
		reason := ""
		_ = reason

		if !w.hasTarget() {
			reason = "hasTarget"
		} else if !w.isTargetLocked() {
			reason = "isTargetLocked"
		} else if !w.isTargetInRadius() {
			reason = "isTargetInRadius"
		} else if !w.canFire() {
			reason = "canFire"
		} else if !w.target.getHealth().isAlive() {
			reason = "target.getHealth().isAlive"
		} else {
			canAttack = true
		}

		if !canAttack {
			println("can't attack")
			println("reason:", reason)
		} else {
			println("can attack")
		}

		if canAttack {
			// Check weapon is ready
			if w.ready() {
				//sleepsec(wh.weapon.AttackDelay())
				w.fire()
			}
		}
	}
}

func (c *Combat) destroy() {
	for _, w := range c.weapons {
		w.destroy()
	}
}

func (c *Combat) update(dt float64) {
	for _, w := range c.weapons {
		w.update(dt)
	}
}
