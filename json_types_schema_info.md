## Component ###

(json)

- fire_delay

(json)

#### Weapon ####

| name              | type            | optional | is_type | default value | info
| ---               | ---             | ---      | ---     | ---           | ---
| type              | string          |          |         |               | ?
| name              | string          |          |         |               | ?
| caliber           | float           |          |         |               | caliber
| fire_rate         | int?            | ?        |         | 60 (?)        | shots/min
| max_rounds        | int?            | ?        |         |               | ?
| maintenance_cost  | int?            | ?        |         |               | ?
| ammo              | int?            | ?        |         |               | ?
| ammo_charge       | int?            | ?        |         |               | ?
| ammotype          | string          | ?        |         |               | ?
| ammo_type         | string          | ?        |         |               | ?
| _ammo_type        | string (type)   | ?        | Y       |               | ?
| _ammo_types       | [string (type)] | ?        | Y ?     |               | accept ammo types / kinds ?
| ?_ammo_kinds      | [?]             | ?        |         |               |
| attack_delay      | float           | ?        |         | 0             | ?
| realm             | string (mask?)  | ?        |         | ?             | RealmType / Mask (?)

not used (?):
  maintenance_cost, max_rounds


#### AmmoType ####
#### ShotType ####

| name                 | type            | optional | is_type | info
| ---                  | ---             | ---      | ---     | ---
| _type                | string (type)   |          | Y ?     | shot_type
| _id                  | string (type)   |          | Y ?     | id / name / typename ?
| type                 | string          |          |         | ?
| name                 | string          |          |         | ?
| caliber              | float           |          |         | caliber
| cost                 | int             |          |         | ?
| move_speed           | int             |          |         | ?
| shot_move_speed      | int             |          |         | shot move speed
| reliability          | float           |          |         | ?
| accuracy             | float           |          |         | ?
| weight               | float           |          |         | ?
| affect_radius        | int             |          |         | ?
| radius               | float           |          |         | radius of action / hit radius / attack radius
| attack_radius        | float           |          |         | radius of action / hit radius / attack radius
| locate_radius        | float           |          |         | target locate radius
| target_locate_radius | float           |          |         | target locate radius
| attack_rating        | int             |          |         | attack rating

TODO: rename shot variables / properties to shot_ ?
TODO: ? rename AmmoType to ShotType
TODO: ? rename locate_radius to target_locate_radius
TODO: ? rename radius  to attack_radius

	caliber          float64    // калибр
	cost             int        // стоимость
	moveSpeed        float64    // скорость перед.
	affectRadius     int        // радиус поражения
	affectRadiusFade float64    // скорость затухания
	attackRating     int        // оценка атаки
	radius           float64    // радиус действия
	locateRadius     float64    // радиус обнаружения (используется MovingBhv)
	hitType          HitType    // тип попадания
	affectType       AffectType // тип поражения
	reliability      float64    // надежность
	accuracy         float64    // точность
	weight           float64    // вес
	projectileType   string     // тип снаряда

#### Examples ####

default/basic shot type:

```
{
	"_type": "shot_type"
    "_id": "default_shot_type",
    "type": "default_shot_type",
    "name": "default shot type",
    "caliber": 76,
    "cost": 0,
    "move_speed": 10,
    "shot_move_speed": 10
    "reliability": 1.0,
    "accuracy": 1.0,
    "weight": 10,
    "affect_radius": 1,
    "radius": 10
    "attack_radius": 10
    "locate_radius": 10
    "target_locate_radius": 10,
    "attack_rating": 1
}
```

### Info ###

Locations:

- Unit: res://units
- Weapon: res://weapons
- ShotType: res://weapons/shot_types

? unit components: res/unit_components
                   res/unit_components/turrets
// or unit parts
