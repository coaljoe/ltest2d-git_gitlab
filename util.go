package main_module

import (
	//"fmt"
	"os"
	"time"

	_debug "kristallos.ga/lib/debug"
)


var p = _debug.P
var pp = _debug.Pp
var pf = _debug.Pf
var pv = _debug.Pv
var pz = _debug.Pz
var dump = _debug.Dump
var pdump = _debug.Pdump
var dumpDepth = _debug.DumpDepth

var __p = p
var _ = __p

var __pp = pp
var _ = __pp

var __ps = _debug.Ps


//var __disable_p = false // XXX test

func cknil(c interface{}) {
	if c == nil {
		panic("Check nil failed.")
	}
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func sleepsec(t float64) {
	// Sleep float seconds
	time.Sleep(time.Duration(t*1000) * time.Millisecond)
}

// Exists returns whether the given file or directory exists or not
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
