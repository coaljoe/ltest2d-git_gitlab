package main_module

import (
	. "kristallos.ga/rx/math"
	"testing"
)

func TestPosition(t *testing.T) {
	// Monkey patch some values
	game.field.w = 64
	game.field.h = 64

	dPos := Vec3{1778, 98, 0}
	//dPos := Vec3{1778, 70, 0}
	_ = dPos

	dummyObj := newObj("dummy")
	dummyObj.SetPos(Vec3{14, 98, 0})

	pos := newPosition(dummyObj)
	pos.setDPos(dPos)
	wrapDPos := pos.getWrappedRelDPos()

	p("wrapDPos:", wrapDPos)

	want := Vec3{-14, 98, 0}

	if !wrapDPos.Equal(want) {
		t.Fail()
	}
}
