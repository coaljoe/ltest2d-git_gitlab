#!/bin/bash

## usage: rejoin_tiles.sh "l4"

#montage -mode concatenate -tile 8x6 `ls -1 "$1"_*.png | sort -V` PNG24:/tmp/rejoined.png

file_list=""

for i in {0..47}
do
	f=$1_$i.png
	#echo "$1_$i.png"
	echo -n "$f"

	if [ -f $f ]; then
		#echo "ok"
		file_list+="$f "
	else
		echo -n " -> null"
		file_list+="null: "
	fi

	echo
done

echo
echo "file_list:"
echo "$file_list"

montage -mode concatenate -tile 8x6 -geometry 28x28 -background black $file_list PNG24:/tmp/rejoined.png

echo 
echo "done"
