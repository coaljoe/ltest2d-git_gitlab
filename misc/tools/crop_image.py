#!/usr/bin/env python
import sys
from PIL import Image

# Find image data extents
def get_image_data_extents(im):
    print("image_data_dim")
    #pix = im.get_data()
    w, h = im.size
    min_x = w
    max_x = 0
    min_y = h
    max_y = 0
    for y in range(0, h):
        for x in range(0, w):
            print("x,y", x, y)
            p = im.getpixel((x, y))
            print(p)

            # Non-transp
            if p[3] != 0:
                if x > max_x:
                    max_x = x
                if y > max_y:
                    max_y = y
                if x < min_x:
                    min_x = x
                if y < min_y:
                    min_y = y

    print("min_x", min_x)
    print("max_x", max_x)
    print("min_y", min_y)
    print("max_y", max_y)

    return min_x, max_x, min_y, max_y


im = Image.open(sys.argv[-1])
im_w, im_h = im.size

min_x, max_x, min_y, max_y = get_image_data_extents(im)

tile_size = 28
need_x = 4
need_y = 4
have_x = int(im_w / tile_size)
have_y = int(im_h / tile_size)

print("have_x", have_x)
print("have_y", have_y)

# Tiles
sh_y = int((have_y - need_y) / 2)
print("sh_y", sh_y)

coords = [
        # left
        0,
        # upper
        sh_y * tile_size,
        # right
        im_w,
        # lower
        (sh_y + need_y) * tile_size,
    ]
print("coords:", coords)

# XXX
# Patch coords
if coords[1] > min_y:
    coords[1] = min_y
    print("patched coords:", coords)

cropped_im = im.crop(coords)

print(cropped_im.size)
cropped_im.show()
cropped_im.save("/tmp/z.png")
