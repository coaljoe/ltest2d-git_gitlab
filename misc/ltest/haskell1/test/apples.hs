module Main where
import Text.Printf

type GoID = Int
type Dt   = Float

data GameObject = GameObject
    { goID   :: GoID
    , render :: IO ()
    , update :: Dt -> GameObject }

type Level   = Int
type Elapsed = Float

data Apple = Apple
    { level   :: Level
    , elapsed :: Elapsed }

updateApple :: Apple -> Dt -> Apple
updateApple (Apple level elapsed) dt =
    let elapsed' = elapsed + dt
    in if elapsed' >= 5.0
        then Apple (level+1) (elapsed' - 5.0)
        else Apple  level     elapsed'

apple :: Apple -> GoID -> GameObject
apple a goid =
    GameObject
    { goID   = goid
    , render = printf "apple: id %d, level %d\n" goid (level a)
    , update = flip apple goid . updateApple a }

banana :: GoID -> GameObject
banana goid =
    GameObject
    { goID   = goid
    , render = printf "banana: id %d\n" goid
    , update = const $ banana goid }

main = do
    mapM_ render . take 5 $ iterate (flip update 3) (apple (Apple 1 0) 17)
    mapM_ render . take 5 $ iterate (flip update 3) (banana 17)