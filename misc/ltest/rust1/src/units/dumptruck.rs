#![feature(struct_inherit, struct_variant)]

use unit::{ExtUnit, Unit};
use obj::{Obj};

pub struct Truck : ExtUnit {
  payload: &'static str,
  dumpTime: f32
}

impl Truck {
  pub fn new(name: &'static str) -> Truck {
    Truck { payload: "unspecified", dumpTime: 0.0, speed: 0.0,
            obj: Obj::new(name) }
  }
}

impl Unit for Truck {
  fn get_obj(&self) -> Obj {
    self.obj
  }
}
