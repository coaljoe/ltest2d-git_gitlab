use obj::{Obj};

/* todo: unitcomponent */
/* todo: weapon */
#[deriving(Show)]
pub struct Turret {
  pub name: &'static str,
  pub numGuns: uint,
  pub weight: f32,
  pub obj: Obj,
}
/*
static LTsTurretClass: &'static UnitTypeClass = &UnitTypeClass {
  name: "LT's turret",
  weight: 500.0,
  maxSpeed: 0.0,
  accSpeed: 0.0,
};
*/

static turret_num_guns: uint = 1;

impl Turret {
  pub fn new_ltsTurret() -> Turret {
    Turret
    {
      name: "LT's turret",
      numGuns: turret_num_guns, // static
      weight: 500.0,
      obj: Obj::new("A Turret"),
    }
  }
}
