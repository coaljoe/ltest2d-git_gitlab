use units::truck::{Truck};

#[deriving(Show)]
pub struct TruckView {
  pub model: &'static str,
}

impl TruckView {
  pub fn new() -> TruckView {
    TruckView { model: "test_model" }
  }
  pub fn render(&self, m: &Truck) {
  	println!("truckview.render()");
    //println!("tankview.render(), self: {}", self);
  }
}