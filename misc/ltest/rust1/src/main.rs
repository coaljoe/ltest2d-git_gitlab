#![feature(struct_inherit, struct_variant, globs)]

extern crate debug;
extern crate cgmath;

use std::fmt::Show;
use std::any::{Any, AnyRefExt};
use std::vec::Vec;
use cgmath::Vector2;

//extern crate units;
//use units::lighttank;

use obj::{Obj};
use unit::*;
//use units::{Tank, Truck};
//use units::lighttank::{Tank};
use units::truck::{Truck};
use units::tank::{Tank};

mod obj;
mod unit;
mod units;
mod mountpoint;
mod render;
mod views;

// Logger function for any type that implements Show.
fn log<T: Any+Show>(value: &T) {
    let value_any = value as &Any;

    // try to convert our value to a String.  If successful, we want to
    // output the String's length as well as its value.  If not, it's a
    // different type: just print it out unadorned.
    match value_any.downcast_ref::<String>() {
        Some(as_string) => {
            println!("String ({}): {}", as_string.len(), as_string);
        }
        None => {
            println!("{}", value);
        }
    }
}

// This function wants to log its parameter out prior to doing work with it.
fn do_work<T: Show+'static>(value: &T) {
    log(value);
    // ...do some other work
}

fn main() {
  //let mut u1 = makeUnit(utype::dumpTruck) as Truck;
  //let mut u2 = makeUnit(utype::lightTank) as Tank;
  //let mut u1: Truck = makeUnitG(&Truck);
  let mut u1: Tank = Tank::new_lightTank("Light Tank1");
  //let mut u2: Tank = Tank::new_lightTank("Light Tank2");
  let mut u2: Truck = Truck::new_dumpTruck("Dump Truck1");
  println!("hello world test1");
  let ob1 = Obj { id: 0, name: "unnamed", pos: Vector2::new(0f32, 0f32)};
  let s = "test".to_string();
  log(&s);
  log(&ob1);
  println!("ob1 {}", ob1);
  //println!("u1 {}", u1);

  let my_string = "Hello World".to_string();
  log(&my_string);

  let my_i8: i8 = 100;
  do_work(&my_i8);
/*
  let mut units: Vec<Box<Unit>> = Vec::new();
  let mut uz = u1 as Unit;
  units.push(box uz);
*/
  /*for u in units.iter() {
    log(&*u);
  }*/
/*
  let mut units: Vec<Box<Tank>> = Vec::new();
  units.push(box u1);
  units.push(box u1);

  for u in units.iter() {
    log(&(*u).obj);
  }
*/
  let mut units: Vec<Box<UnitLike>> = Vec::new();
  units.push(box u1);
  units.push(box u2);
  //units.push(u2 as Unit);

  for u in units.iter() {
    println!("-> {:?}", *u)
    log(&u.get_obj());
    //log(&Unit::get_obj_static_trait(u).get_obj());
    u.render();
  }

  
}



