use std::cell::{Cell,RefCell};
use std::rc::Rc;

#[deriving(Show)]
struct A { v: int }

fn test_rc() {
    let mut x = Rc::new(RefCell::new(A{v:1i}));
    let mut q = x.clone();
    let mut p = x.clone();
    println!("{} {} {}",x.borrow().v,q.borrow().v,p.borrow().v);
    x.borrow_mut().v = 2;
    println!("{} {} {}",x.borrow().v,q.borrow().v,p.borrow().v);
    // x = Rc::new(Cell::new(2i));
    // println!("{} {} {}",x.v,q.v,p.v);
}

fn test_box() {
    let mut x = A{v:1i};
    let mut q = box x;
    let mut p = box x;
    println!("{} {} {}",x.v,q.v,p.v);
    x.v = 2;
    println!("{} {} {}",x.v,q.v,p.v);
}

fn main() {
    test_rc();
    test_box();
}