use std::cell::{RefCell,Ref,RefMut};
use std::rc::Rc;

struct Entity {
    x: int
}

struct Tile {
    entity: Option<Rc<RefCell<Entity>>>
}

impl Tile {
    pub fn try_read_entity<'a>(&'a self) -> Option<Ref<'a, Entity>> {
        let en = self.entity.as_ref().unwrap().borrow();
        println!(".en.x = {}", en.x);
        // self.entity.as_ref().map(|e| println!(".en.x = {}", e.borrow().x));
        self.entity.as_ref().map(|e| e.borrow()) // return 
    }
}

fn main()
{
    let mut en = Entity { x: 1 };
    let mut en_boxed = Rc::new(RefCell::new(en));
    en_boxed.borrow_mut().x = 2;
    let mut tile;
    {
        tile = Tile { entity: Some(en_boxed.clone()) }; // clone is important
    }
    en_boxed.borrow_mut().x = 3; // worcs after clone
    tile.try_read_entity();
    en.x = 2;

    tile.try_read_entity();
}