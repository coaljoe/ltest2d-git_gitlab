type
  G = generic a
    a.x is int

  A = object
    x: int

  B = object
    x: int

proc test1(a: G) =
  echo "test1"

proc test2(az, bz: G) =
  echo "test2"

var a1 = A(x: 1)
var b1 = B(x: 2)

test1(a1)
test1(b1)
test2(a1, a1) # works if the same type
test2(a1, b1) # type error

## the error
discard """
 Error: type mismatch: got (A, B)
 but expected one of:
 test_g.test2(a: G, b: G)
"""
