var maxId = 0
var maxUnnamedId = 0

type
  Obj* = ref object of TObject
    id*: int
    name*: string

proc initObj*(a: Obj, name: string = "") =
  echo "::initObj"
  a.id = maxId + 1
  let xname = if name.len > 0: name else: "unnamed#" & $(maxUnnamedId + 1)
  a.name = xname
  inc maxId
  inc maxUnnamedId
