import dept

type
  ConstDept* = ref object of Dept

proc newConstDept*(): ConstDept =
  new result
  initDept(result, name = "Construction Department")
  result.description = "Department of Construction."

method addTask*(m: ConstDept, t: DeptTask) =
  #dep.addTask(cast[Dept](m), t)
  #echo type(cast[Dept](m))
  #cast[Dept](m).addTask(t)
  #Dept.addTask(m, t)
  m.addTask1(t)
  echo "added..."
