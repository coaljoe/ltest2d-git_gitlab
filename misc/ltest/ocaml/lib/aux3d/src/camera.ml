open Printf
open Gg
open Node

(*module CameraF(M : Node) = struct*)
module CameraF = 
  functor (M : Node) -> struct
  (*include Node*)
  type node = M.t
  type camera = {
    mutable fov : float;
    mutable aspect : float;
    mutable znear : float;
    mutable zfar : float;
    mutable ortho : bool;
    mutable target : V3.t;
    mutable lookTarget : bool; 
    mutable _zoom : float;
    mutable ortho_planes : float * float * float * float;
  }

  let create = {fov = 45.; aspect = 1.; znear = 0.1; zfar = 100.;
                ortho = false; target = V3.zero; lookTarget = false;
                _zoom = 1.; ortho_planes = (0., 0., 0., 0.)}

  let print s = printf "CameraNode (name=%s)\n" "NOTSET"

  let setupView fov' aspect' znear' zfar' ?(ortho'=false) m () = 
    m.fov <- fov';
    m.aspect <- aspect';
    m.znear <- znear';
    m.zfar <- zfar';
    m.ortho <- ortho';
    (* reset scale *)
    m._zoom <- 1.

  let getZoom s = s._zoom
  let setZoom v s =
    s._zoom <- v;
    let ortho_scale = s.fov
    and aspect_ratio = s.aspect in
    let xaspect = aspect_ratio
    and yaspect = 1.0 in
    let xaspect = xaspect *. ortho_scale /. (aspect_ratio*.2.) in
    let yaspect = yaspect *. ortho_scale /. (aspect_ratio*.2.) in
    s.ortho_planes <- (-.xaspect *. s._zoom, xaspect *. s._zoom, -.yaspect *. s._zoom, yaspect *. s._zoom);
    s._zoom

  let zoom ?v s () =
    match v with | None -> getZoom | Some v -> setZoom v
end
module Camera = CameraF(Node)

(*
module Camera = CameraF(Node)
module CameraF = 
  functor (node: Node) -> struct
*)
