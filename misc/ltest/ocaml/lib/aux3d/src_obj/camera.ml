open Gg
open Node

class camera =
object (s)
  inherit node as super
  val mutable fov = 45.
  val mutable aspect = 1.
  val mutable znear = 0.1
  val mutable zfar = 100.
  val mutable ortho = false
  val mutable target = V3.zero
  val mutable lookTarget = None
  val mutable _zoom = 1.
  val mutable ortho_planes = (0., 0., 0., 0.)

  method setupView fov' aspect' znear' zfar' ?(ortho'=false) () = 
    fov <- fov';
    aspect <- aspect';
    znear <- znear';
    zfar <- zfar';
    ortho <- ortho';
    (* reset scale *)
    _zoom <- 1.;

  method setZoom v =
    _zoom <- v;
    let ortho_scale = fov
    and aspect_ratio = aspect in
    let xaspect = aspect_ratio
    and yaspect = 1.0 in
    let xaspect = xaspect *. ortho_scale /. (aspect_ratio*.2.) in
    let yaspect = yaspect *. ortho_scale /. (aspect_ratio*.2.) in
    ortho_planes <- (-.xaspect *. _zoom, xaspect *. _zoom, -.yaspect *. _zoom, yaspect *. _zoom);
    _zoom
  method getZoom = _zoom
  method zoom ?v () =
    match v with | None -> s#getZoom | Some v -> s#setZoom v

  method print = Printf.printf "CameraNode (name=%s)\n" "NOTSET"
end

(*
let (/) a b = a /. b
let (//) a b = a / b
(* let () a b = a *. b *)
let (\+) a b = a +. b
*)
