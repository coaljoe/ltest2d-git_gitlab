open Gg

class transform =
object (s)
  val mutable pos = V3.v 0.1 0.1 0.1
  val mutable rot = V3.v 0. 0. 0.
  val mutable ori = Quat.v 1. 1. 1. 1.
  val mutable scale = V3.v 1. 1. 1.

  method transform px py pz rx ry rz =
    pos <- V3.v px py pz;
    rot <- V3.v rx ry rz;
    (*
    if sx || sy || sz then
      scale <- V3.v sx sy sz;
    *)

  method x = V3.x pos
  method y = V3.y pos
  method z = V3.z pos
  method rx = V3.x rot
  method ry = V3.y rot
  method rz = V3.z rot
  method pos = pos
  method set_pos x = pos <- x
end

class node =
object (s)
  inherit transform as super
  val mutable visible = true

  method visible = visible
end
