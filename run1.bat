@echo off

rem setlocal
call env.bat
for /f "delims=" %%i in ('hg id -i') do set _rev=%%i

@rem always clean
del ltest2d.exe 2> nul

@rem go run -v src\main.go
cd main && ^
go build -v -ldflags="-X 'main.BuildRevision=%_rev%'" -o ../ltest2d.exe && ^
cd .. && ^
ltest2d.exe -dev %*