package main_module

import (
	"bytes"
	"encoding/gob"
	"io/ioutil"
	"os"
)

// Map file metadata header
type MapInfoHeader struct {
	// Data format string
	FormatString string
	// Data format version
	FormatVersionMajor int
	FormatVersionMinor int
}

// Map building information
// for respawning/recreating buildings
type MapBuildingRecord struct {
	// ?
	// id int
	// Position to respawn
	Cx, Cy int
	// Building parameters
	Name string // bname
	CampId CampId
	// ? Player
	// XXX player/ai slot?
	// XXX team/group
	// XXX not assigned on save, free to join
	//playerId int
	// XXX
	PlayerSlotId int // -1 == nil/unassigned?
}

// .map file loader
type MapLoader struct {
	// XXX
	saveBuildings bool
}

func newMapLoader() *MapLoader {
	ml := &MapLoader{
		saveBuildings: true,
	}

	return ml
}

func (ml *MapLoader) save(path string) {
	_log.Inf("%F path:", path)

	fd, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		panic(err)
	}

	buf := &bytes.Buffer{}
	// Writing
	enc := gob.NewEncoder(buf)

	/*
	// Write field
	err = enc.Encode(game.field)
	if err != nil {
		panic(err)
	}
	*/
	
	//// Write blocks

	// MapInfoHeader
	{
		mapHdr := &MapInfoHeader{
			FormatString: "Map file v0.1",
			FormatVersionMajor: 0,
			FormatVersionMinor: 1,
		}

		err = enc.Encode(mapHdr)
		if err != nil {
			panic(err)
		}
	}

	// Field
	{
		// Write field data
		//data := buf.Bytes()
		data, err := game.field.saveData()
		if err != nil {
			panic(err)
		}

		buf.Write(data)
	}

	// Buildings
	{
		// Write buildings as records
		recs := make([]MapBuildingRecord, 0)
		
		for _, el := range game.buildingsys.getElems() {
			b := el.(*Building)

			rec := ml.buildingToMapBuildingRecord(b)

			recs = append(recs, rec)	
		}

		// Write
		err = enc.Encode(recs)
		if err != nil {
			panic(err)
		}
	}

	////

	/*
	_, err = fd.Write(data)
	if err != nil {
		panic(err)
	}
	*/

	// Write to file
	b := buf.Bytes()
	_, err = fd.Write(b)
	if err != nil {
		panic(err)
	}

	// Close
	if err := fd.Sync(); err != nil {
		panic(err)
	}
	fd.Close()

	_log.Inf("done %F")
}

func (ml *MapLoader) load(path string) {
	_log.Inf("%F path:", path)

	if ok, err := exists(path); !ok {
		if err != nil {
			panic(err)
		}
		pp("file not found: ", path)
	}
	
	data, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	buf := &bytes.Buffer{}
	// Reading
	dec := gob.NewDecoder(buf)

	buf.Write(data)

	///// Read blocks

	// MapInfoHeader
	{
		p("process MapInfoHeader...")
		
		var mapHdr *MapInfoHeader
		dec.Decode(&mapHdr)

		//pp(mapHdr)
	}

	// Field
	{
		p("process Field...")
	
		//game.field.loadData(data)

		/*
		// XXX read rest of buf data
		b := buf.Bytes()
	
		game.field.loadData(b)
		*/
		game.field.loadData(buf)
		
		// XXX finally
		game.field.spawn() // fixme?
	}

	// Buildings
	{
		p("process Buildings...")
	
		var recs []MapBuildingRecord
		dec.Decode(&recs)

		//pp(recs)

		// Place buildings
		for _, rec := range recs {
			p("restoring building from rec...")
			p("rec:", rec)
			
			b := makeBuilding(rec.Name, rec.CampId)
			b.placeAt(rec.Cx, rec.Cy)
			
			if rec.PlayerSlotId != -1 {
				pl := game.playersys.getPlayerByPlayerSlotId(rec.PlayerSlotId)
				b.assignPlayer(pl)
			}
			
			// XXX finally
			SpawnEntity(b)
		}
	}

	////
	
	_log.Inf("done %F")
}

// Convert map building to MapBuildingRecord
func (ml *MapLoader) buildingToMapBuildingRecord(b *Building) MapBuildingRecord {

	playerSlotId := -1
	
	if b.player != nil {
		playerSlotId = b.player.playerSlotId
	}

	rec := MapBuildingRecord{
		Cx: b.cx,
		Cy: b.cy,
		Name: b.name,
		CampId: b.campId,
		PlayerSlotId: playerSlotId,
	}
	
	return rec
}
