package main_module

import (
	//"fmt"
)

// Cell pos
type CPos struct {
	x, y int
}
func (c CPos) X() int { return c.x }
func (c CPos) Y() int { return c.y }

// Cell rect
type CRect struct {
	x, y, w, h int
}

func (r1 CRect) overlap(r2 CRect) bool {
	//return !(r1.x+r1.w < r2.x || r1.y+r1.h < r2.y ||
	//	r1.x > r2.x+r2.w || r1.y > r2.y+r2.h)

	// Touching borders doesn't count, only overlapping
	return !(r1.x+r1.w <= r2.x || r1.y+r1.h <= r2.y ||
		r1.x >= r2.x+r2.w || r1.y >= r2.y+r2.h)
}

/*
func (c CPos) String() string {
    return fmt.Sprintf("<CPos x: %d, y: %d>", c.x, c.y)
}
*/
