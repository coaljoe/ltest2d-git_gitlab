package main_module

import (
	ps "kristallos.ga/lib/pubsub"
	. "kristallos.ga/rx/math"
)

const (
	ev_entity_destroy ps.EventType = iota
	ev_unit_spawn
	ev_unit_destroy
	//ev_unit_move
	ev_unit_step
	ev_weaponhost_fire
	ev_projectile_hit
	ev_building_spawn
	ev_building_destroy
	ev_mouse_button_event
)

type EvProjectileHit struct {
	pos      Vec3
	ammotype *AmmoType
	//emitter  *ecs.Entity
	//target   *ecs.Entity
}

type EvUnitStep struct {
	//unit *ecs.Entity
	dCx int
	dCy int
}

func pub(eventType ps.EventType, data interface{}) {
	_log.Inf("Event: pub; type:", eventType, "data:", data)
	ps.Publish(eventType, data)
}

func sub(eventType ps.EventType, fn ps.Callback) {
	_log.Inf("Event: sub; type:", eventType, "fn:", fn)
	ps.Subscribe(eventType, fn)
}

type EventS = ps.EventS

func PubS(eventType string, data ...interface{}) {
	ps.PublishS(eventType, data...)
}

func SubS(eventType string, fn ps.CallbackS) {
	ps.SubscribeS(eventType, fn)
}
