// Height map for field generation.
package main_module

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"math"
	"os"
	"strings"
	"strconv"

	"github.com/drbig/perlin"
	"github.com/ojrac/opensimplex-go"
)

type HeightMapOptions struct {
	Alpha   float64 // Alpha Factor
	Beta    float64 // Beta Factor
	Octaves int     // Number of octaves
	Seed    int64   // Random Seed
	Smooth  bool    // "smooth (continuous) gradient"

	NoiseScale float64 // Noise Scale
	//LevelWeights [8]float64 // Posterization level weights
	SeaAmt float64 // Sea/Ground amount

	SaveFixedStep bool // Save images with fixed step values rather than values from regions

	Filter bool // Filter heightmap
}

// Default HeightMap options.
func newHeightMapOptions() HeightMapOptions {
	return HeightMapOptions{
		Alpha: 1.5,
		Beta:  2,
		//Alpha: 3.5,
		//Beta:  4,
		Octaves: 6,
		//Octaves: 2,
		Seed:   41,
		Smooth: true,
		//Smooth: false,
		//NoiseScale: 4.0,
		//NoiseScale: 1.0,
		//NoiseScale: 2.0,
		NoiseScale: 0.25,
		//LevelWeights: [8]float64{1.0, 1.0, 1.0, 1.0,
		//	1.0, 1.0, 1.0, 1.0},
		//SeaAmt: 0.5,
		//SeaAmt: 1.0,
		SeaAmt:        0.0,
		SaveFixedStep: true,
		Filter:        true,
	}
}

type HeightMap struct {
	data        [][]int // -n.. 0.. +n; 0 - sea/ground level(?); posterized
	dataOrig    [][]int // Smooth, unchanged hm, used for debug; normalized to 0..256 range
	w, h        int
	levelStep   int // 256/8 // 32
	numLevels   int // 8
	minLevel    int // -4
	maxLevel    int // +3 (0 included)
	regions     [8]int
	opt         HeightMapOptions
	initialized bool
}
//func (hm HeightMap) DataOrig() [][]int { return hm.dataOrig }
func (hm *HeightMap) DataOrig() [][]int { return hm.dataOrig }
func (hm *HeightMap) Opt() HeightMapOptions { return hm.opt }

func newHeightMap() HeightMap {
	hm := HeightMap{
		levelStep:   256 / 8, // / 32,
		initialized: false,
	}
	hm.numLevels = 256 / hm.levelStep
	//hm.minLevel = -(hm.numLevels / 2)
	//hm.maxLevel = hm.numLevels / 2
	hm.minLevel = int(ZLevel_Min)
	hm.maxLevel = int(ZLevel_Max)
	// Regions
	hm.regions = [8]int{
		/*
			10, // sea3
			20, // sea2
			30, // sea1
			40, // sea0
			50, // ground0
			60, // ground1
			70, // ground2
			80, // ground3
		*/
		/*
			32,     // sea3
			32 * 2, // sea2
			32 * 3, // sea1
			32 * 4, // sea0
			32 * 5, // ground0
			32 * 6, // ground1
			32 * 7, // ground2
			32 * 8, // ground3
		*/
		/*
			int(0.125 * 256), // sea3
			int(0.25 * 256),  // sea2
			int(0.375 * 256), // sea1
			int(0.5 * 256),   // sea0
			int(0.625 * 256), // ground0
			int(0.75 * 256),  // ground1
			int(0.875 * 256), // ground2
			int(1.0 * 256),   // ground3
		*/
		/*
			int(0.125 * 256), // sea3
			int(0.25 * 256),  // sea2
			int(0.375 * 256), // sea1
			int(0.5 * 256),   // sea0
			int(0.625 * 256), // ground0
			int(0.75 * 256),  // ground1
			int(0.875 * 256), // ground2
			255,              // ground3
		*/
		/*
			// Standard values
			int(0.125 * 256), // sea3
			int(0.25 * 256),  // sea2
			int(0.375 * 256), // sea1
			int(0.5 * 256),   // sea0
			160,              //160, // ground0
			176,              //192,  // ground1
			205,              // ground2
			255,              // ground3
		*/

		// Experimental values
		8,   //32, // sea3
		20,  //64,  // sea2
		30,  //96, // sea1
		80,  //96,       //128, // sea0
		125, //160 - 16, //160, // ground0
		145, //176,      //192,  // ground1
		155, //205,      // ground2
		180, // ground3
		//255, // ground3
		/*
			145, //160 - 16, //160, // ground0
			165, //176,      //192,  // ground1
			175, //205,      // ground2
			255, // ground3
		*/
		/*
			166,              // ground1
			168,              // ground2
			255,              // ground3
		*/
		//int(1.0 * 256),   // ground3
	}
	return hm
}

// Set new size
func (hm *HeightMap) setSize(w, h int) {
	_log.Inf("%F w:", w, "h:", h)

	hm.w = w
	hm.h = h
	
	// Fill data
	//data := hm.data
	hm.data = make([][]int, w)
	for i := range hm.data {
		hm.data[i] = make([]int, h)
	}
	//pp(hm.data)
	//pp(data[w][h])
	//pp(data[w-1][h-1])
	//pp(len(data))
	//hm.data = data

	// Initialize dataOrig
	hm.dataOrig = make([][]int, w)
	for i := range hm.dataOrig {
		hm.dataOrig[i] = make([]int, h)
	}
}

func (hm *HeightMap) generate(w, h int, opt HeightMapOptions) {

	hm.opt = opt
	hm.setSize(w, h)

	// ./perlin -c -a 2 -b 2 -h 128 -w 128 -n 10 -s 3 -r 41 ex.png
	//g := perlin.NewGenerator(2, 2, 10, 41)
	//g := perlin.NewGenerator(1.5, 2, 6, 41)
	//g := perlin.NewGenerator(2, 2, 4, 41)
	g := perlin.NewGenerator(opt.Alpha, opt.Beta, opt.Octaves, opt.Seed)

	// From: https://www.gamedev.net/blogs/entry/2138456-seamless-noise/
	tileableNoise := func(x, y, w, h float64) float64 {
		return (g.Noise2D(x, y)*(w-x)*(h-y) +
			g.Noise2D(x-w, y)*(x)*(h-y) +
			g.Noise2D(x-w, y-h)*(x)*(y) +
			g.Noise2D(x, y-h)*(w-x)*(y)) / (w * h)
		/*
			(
				F(x, y) * (w - x) * (h - y) +
				F(x - w, y) * (x) * (h - y) +
				F(x - w, y - h) * (x) * (y) +
				F(x, y - h) * (w - x) * (y)
			) / (w * h)
		*/
	}
	_ = tileableNoise

	/*
		noiseScale := opt.NoiseScale //4.0
		sx := noiseScale / float64(hm.w)
		sy := noiseScale / float64(hm.h)
	*/

	/*
		noiseScale := opt.NoiseScale //4.0
		sx := noiseScale / float64(hm.w)
		sy := noiseScale / float64(hm.h)
	*/

	noise := opensimplex.New(hm.opt.Seed)

	vMin := 255
	vMax := 04
	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			var _ = `
			//rmin, rmax := -1.0, 1.0
			// Normalize fw/fh range to -1, 1
			//nx := -1.0 + float64(x)/(float64(hm.w)/2.0)
			//ny := -1.0 + float64(y)/(float64(hm.h)/2.0)
			/*
				nx := float64(x) * sx
				ny := float64(y) * sy
			*/

			/*
				local s=x/bufferwidth
				local t=y/bufferheight

				local nx=x1+s*(x2-x1)
				local ny=y1+s*(y2-y1)
			*/
			x1, x2 := 0.0, 1.0
			y1, y2 := 0.0, 1.0
			//x1, x2 := 0.0, 0.5
			//y1, y2 := 0.0, 0.5
			//x1, x2 := 0, 16
			//y1, y2 := 0, 16
			//x1, x2 := 0, 8
			//y1, y2 := 0, 8
			//x1, x2 := 0, 2
			//y1, y2 := 0, 2
			//x1, x2 := 0, 4
			//y1, y2 := 0, 4
			//x1, x2 := 0, hm.w
			//y1, y2 := 0, hm.h

			s := float64(x) / float64(hm.w)
			t := float64(y) / float64(hm.h)

			/*
				nx := float64(x1) + s*float64(x2-x1)
				//ny := float64(y1)+s*float64(y2-y1)
				ny := float64(y1) + t*float64(y2-y1)
			*/

			nx := x1 + s*(x2-x1)
			ny := y1 + t*(y2-y1)

			var nv float64
			if opt.Smooth {
				//nv = (g.Noise2D(nx, ny) + 1.0) / 2.0
				//nv = (tileableNoise(nx, ny, float64(hm.w), float64(hm.h)) + 1.0) / 2.0
				//nv = (tileableNoise(nx, ny, float64(x2-x1), float64(y2-y1)) + 1.0) / 2.0
				nv = tileableNoise(nx, ny, x2-x1, y2-y1)
			} else {
				//nv = g.Noise2D(nx, ny)
			}
			//nv = math.Pow(nv, 2.38)
			`

			var _ = `
			// Simplex 4d method
			// Noise range
			//x1, x2 := 0.0, 2.0
			//y1, y2 := 0.0, 2.0
			x1, x2 := 0.0, 10.0
			y1, y2 := 0.0, 10.0
			dx := x2 - x1
			dy := y2 - y1

			// Sample noise at smaller intervals
			s := float64(x) / float64(hm.w)
			t := float64(y) / float64(hm.h)

			freq := 7.0
			//freq := 1.0
			_ = freq

			// Calculate our 4D coordinates
			nx := x1 + math.Cos(s*2*math.Pi)*dx/(2*math.Pi)
			ny := y1 + math.Cos(t*2*math.Pi)*dy/(2*math.Pi)
			nz := x1 + math.Sin(s*2*math.Pi)*dx/(2*math.Pi)
			nw := y1 + math.Sin(t*2*math.Pi)*dy/(2*math.Pi)

			//nv := noiseGen.Eval4(nx, ny, nz, nw)
			//nv := noise.Eval4(freq*nx, freq*ny, freq*nz, freq*nw)
			/*
				nv := 1*noise.Eval4(1*nx, 1*ny, 1*nz, 1*nw) +
					0.5*noise.Eval4(2*nx, 2*ny, 2*nz, 2*nw) +
					0.25*noise.Eval4(4*nx, 4*ny, 4*nz, 4*nw)
			*/
			`

			// Parameters
			octaves := 5
			persistence := 0.5
			lacunarity := 2.0

			amplitude := 1.0
			frequency := 1.0
			noiseHeight := 0.0

			for i := 0; i < octaves; i++ {
				// Noise range
				//x1, x2 := 0.0, 2.0
				//y1, y2 := 0.0, 2.0
				x1, x2 := 0.0, 10.0
				y1, y2 := 0.0, 10.0
				dx := x2 - x1
				dy := y2 - y1

				// Sample noise at smaller intervals
				s := float64(x) / float64(hm.w) * frequency
				t := float64(y) / float64(hm.h) * frequency

				freq := 7.0
				//freq := 1.0
				_ = freq

				// Calculate our 4D coordinates
				nx := x1 + math.Cos(s*2*math.Pi)*dx/(2*math.Pi)
				ny := y1 + math.Cos(t*2*math.Pi)*dy/(2*math.Pi)
				nz := x1 + math.Sin(s*2*math.Pi)*dx/(2*math.Pi)
				nw := y1 + math.Sin(t*2*math.Pi)*dy/(2*math.Pi)

				sv := noise.Eval4(nx, ny, nz, nw)

				noiseHeight += sv * amplitude

				amplitude *= persistence
				frequency *= lacunarity
			}

			/*
			   // keep track of the max and min values found
			   if (heightValue > mapData.Max) mapData.Max = heightValue;
			   if (heightValue < mapData.Min) mapData.Min = heightValue;
			*/

			nv := noiseHeight

			// Normalize nv to 0, 1 range
			nvNorm := (nv - -1.0) / (1.0 - -1.0)
			_ = nvNorm
			//nvNorm = math.Pow(nvNorm, 2.4)
			//nvNorm = math.Pow(nvNorm, 2)
			//nvNorm = math.Pow(nvNorm, 2)
			//nvNorm = math.Pow(nvNorm, 8)
			//p(nv, nvNorm)
			//v := int(128.0 - (nv*256)/2)
			v := int(nvNorm * 256)
			//p(v)
			hm.dataOrig[x][y] = v

			if v < vMin {
				vMin = v
			}
			if v > vMax {
				vMax = v
			}
		}
	}
	//pp(hm.dataOrig)

	// Normalize dataOrig to 0..255 range
	//if false {
	if true {
		//range_ := vMax - vMin
		//pp(vMin, vMax, range_)
		rangeMin := 0
		rangeMax := 255
		for x := 0; x < hm.w; x++ {
			for y := 0; y < hm.h; y++ {
				v := hm.dataOrig[x][y]
				p("v1:", v)
				//v = (v-vMin)/range_
				//vNorm := (v - vMin) / (vMax - vMin)
				//vNorm := (v - 0) / (255 - 0)
				//vNorm = (255-0) * (v-min)/(max-min) + 0
				vNorm := (rangeMax-rangeMin)*(v-vMin)/(vMax-vMin) + rangeMin
				p("v2:", vNorm)
				hm.dataOrig[x][y] = vNorm
				//if v == 233 {
				//	pp(2)
				//}
			}
		}
		//pp(2)
	}

	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			v := hm.dataOrig[x][y]

			success := false
			for i, rv := range hm.regions {
				//if v <= rv {
				if v >= rv {
					//p("YYY2", i, v, rv)
					//if v >= rv {
					//hm.data[x][y] = i
					//defaultLevel := 4
					//hm.data[x][y] = i - defaultLevel
					hm.data[x][y] = hm.ltoz(i)
					success = true
					//break
				} else if i == 0 {
					hm.data[x][y] = hm.ltoz(i)
					success = true
				}
			}
			//p("YYY", v, hm.data[x][y], hm.regions)
			//pp(v, hm.data[x][y], hm.regions)
			if !success {
				pp("hm value can't be handled by regions; v:", v, "regions:", hm.regions)
			}
			//hm.data[x][y] += 4

			// Adjust noise value with SeaAmt
			// Sea/Ground coeff.
			//sAmt := 1.0
			//sAmt := 0.0
			sAmt := opt.SeaAmt
			//sAmt = 1.5
			//gAmt = 2.0 - sAmt
			gAmt := 1.0 - sAmt
			_ = gAmt
			/*
				var nvSeaAdj float64
				if nv < 0.0 {
					nvSeaAdj = nv * sAmt
				} else {
					nvSeaAdj = nv * gAmt
				}
			*/
			//nvSeaAdj := nv*sAmt + nv*gAmt
			//nvSeaAdj := 0.5 + nv
			//nvSeaAdj := -1.0 + nv
			//nvSeaAdj := -1.0 + nv
			//nvSeaAdj := nv
			//_ = nvSeaAdj

			/*
				nvSeaAdj := nv
				k := -0.5
				if nvSeaAdj < 0.0 {
					nvSeaAdj = maxf(k+nvSeaAdj, -0.0)
				} else {
					nvSeaAdj = maxf(k+nvSeaAdj, 1.0)
				}
			*/

			// Rewrite v
			//v = int(128.0 - (nvSeaAdj*256)/2)
			// Normalize to 0, 255 range
			//v = int(((nvSeaAdj - -1.0) / (1.0 - -1.0)) * 255)

			/*
				nlevels := float64(hm.numLevels)
				v0 := sAmt * (1.0 / nlevels)
				v1 := sAmt * (1.0 / nlevels)
				v2 := sAmt * (1.0 / nlevels)
				v3 := sAmt * (1.0 / nlevels)
				//v0 = v1 = v2 = v3 = 0

				v4 := gAmt * (1.0 / nlevels)
				v5 := gAmt * (1.0 / nlevels)
				v6 := gAmt * (1.0 / nlevels)
				v7 := gAmt * (1.0 / nlevels)
			*/

			if false {
				// Posterize
				//fmt.Printf("%v ", v)
				// Map values in -4 .. +4 range, 0 is ground level 0
				//levelStep := 256/8 // 32
				//defaultLevel := 4
				defaultLevel := 4
				//defaultLevel := 3
				v = (v / hm.levelStep) - defaultLevel
			}

			// XXX upscale (for testing)
			//v += 3
			//v += 4
			//hm.data[x][y] = v
			//f.cells[x][y].z = v
		}
	}

	hm.saveData(vars.tmpDir+"/_hm_unfiltered.png", hm.data)
	//res := hm.fill(27, 112, 0xff, true, 20)
	//res := hm.fill(188, 57, 0xff, true, 20)
	//pp(res)

	// Filter data
	//if true {
	if hm.opt.Filter {
		for y := 0; y < hm.h; y++ {
			for x := 0; x < hm.w; x++ {
				// Filter single ground block tiles in water (tileId = 47)
				center := hm.getData(x, y)
				top := hm.getData(x, y-1)
				bottom := hm.getData(x, y+1)
				left := hm.getData(x-1, y)
				right := hm.getData(x+1, y)
				topLeft := hm.getData(x-1, y-1)
				topRight := hm.getData(x+1, y-1)
				bottomLeft := hm.getData(x-1, y+1)
				bottomRight := hm.getData(x+1, y+1)

				lev := int(ZLevel_Ground0)
				//if ZLevel(center).isGround() &&
				if center == lev &&
					top < lev && bottom < lev &&
					left < lev && right < lev &&
					topLeft < lev && topRight < lev &&
					bottomLeft < lev && bottomRight < lev {
					hm.data[x][y] = int(ZLevel_Sea0)
					//pp(2)
				}

				// Filter corner single pixels
				// . . .
				// . f .
				// . . x
				// XXX TODO add double corners and triple, quad too
				if center == lev && top < lev && bottom < lev &&
					left < lev && right < lev {
					if bottomRight == lev && bottomLeft < lev &&
						topLeft < lev && topRight < lev {
						hm.data[x][y] = int(ZLevel_Sea0)
					} else if bottomLeft == lev && bottomRight < lev &&
						topLeft < lev && topRight < lev {
						hm.data[x][y] = int(ZLevel_Sea0)
					} else if topLeft == lev && topRight < lev &&
						bottomLeft < lev && bottomRight < lev {
						hm.data[x][y] = int(ZLevel_Sea0)
					} else if topRight == lev && topLeft < lev &&
						bottomLeft < lev && bottomRight < lev {
						hm.data[x][y] = int(ZLevel_Sea0)
					}
				}

				// Filter 4-tile ground blocks in water
				// XXX TODO flood-fill volume based filter?
				//if topLeft
				if ZLevel(center) == ZLevel_Ground0 {
					//res := hm.fill(x, y, int(ZLevel_Ground0), true)
					limit := 20
					res := hm.fill(x, y, 0xff, true, limit)
					p("XXX", x, y)
					//if x != 50 && y != 0 {
					if len(res) > 0 && len(res) < limit {
						p(len(res))
						/*
							if x != 50 && x != 52 && x != 83 {
								pp(res)
							}
						*/
						for _, p := range res {
							hm.data[p.x][p.y] = int(ZLevel_Sea0)
						}

						if true {
							// Clear everything inside bounding rect that isn't sea
							if len(res) > 1 {
								minX, maxX := math.MaxInt32, 0
								minY, maxY := math.MaxInt32, 0
								for _, p := range res {
									if p.x < minX {
										minX = p.x
									}
									if p.x > maxX {
										maxX = p.x
									}
									if p.y < minY {
										minY = p.y
									}
									if p.y > maxY {
										maxY = p.y
									}
								}
								p("res:", res)
								p("range x:", minX, maxX)
								p("range y:", minY, maxY)
								for y := minY; y <= maxY; y++ {
									for x := minX; x <= maxX; x++ {
										v := hm.data[x][y]
										/*
											if ZLevel(v) == ZLevel_Ground1 {
												res2 := hm.fill(x, y, 0xff, true, limit)
												pp(res2)
											}
										*/
										if !ZLevel(v).IsSea() {
											newV := int(ZLevel_Sea0)
											hm.data[x][y] = newV
										}
									}
								}
								//pp(2)
							}
						}
					}

				}
			}
		}
		hm.saveData(vars.tmpDir+"/_hm_filtered.png", hm.data)
	}

	//pp(hm.data)
	//p(hm.data)
	//pp(hm.dataOrig)
	hm.initialized = true
	//pp(2)
	//pdump(hm.data)
}

func (hm *HeightMap) ztol(z int) int {
	return z + (hm.numLevels / 2)
}

func (hm *HeightMap) ltoz(l int) int {
	return l - (hm.numLevels / 2)
}

// Generate new image from str/params ?
// default fill value: ground ?
// fill value for -1: water (level 0) ?
// supports patterns like (?):
//   gen:64x64:fill:0
//   gen:64x64:fill:ground0
func (hm *HeightMap) genImage(genParams string) image.Image {

	_log.Inf("%F genParams:", genParams)

	//var im image.Image
	//_ = im

	s := genParams

	if !strings.HasPrefix(s, "gen:") {
		pp("error: not gen params string ?")
	}

	x := strings.Split(s, ":")

	sizeS := x[1]

	fmt.Println("sizeS:", sizeS)

	z := strings.Split(sizeS, "x")
	sizeX, err := strconv.Atoi(z[0])
	if err != nil {
		panic(err)
	}
	sizeY, err := strconv.Atoi(z[1])
	if err != nil {
		panic(err)
	}

	fmt.Println("sizeX:", sizeX)
	fmt.Println("sizeY:", sizeY)

	//fillValue :=

	//v := 128
	
	levelStep := 256 / 8 // 32
	/*
	defaultLevel := 4
	xv := (v / levelStep) - defaultLevel

	v2 := 0

	xv2 := hm.ztol(v2)
	xv2a := hm.ztol(v2) * levelStep

	fmt.Println("v:", v)
	fmt.Println("xv:", xv)

	fmt.Println("v2:", v2)
	fmt.Println("xv2:", xv2)
	fmt.Println("xv2a:", xv2a)

	//xv3 := uint8(xv2a)
	*/
	
	var c color.Color

	if len(x) > 2 {
		
		// Values from opts
		valS := x[3]
		fmt.Println("valS:", valS)
		
		var zval ZLevel
		
		val, err := strconv.Atoi(valS)
		if err == nil {
			zval = decodeZLevelFromInt(val)
		} else {
			zval = decodeZLevelFromString(valS)
		}
		
		fmt.Println("zval:", zval)
		
		//pp(2)
	
		// XXX
		//zv := 0 // value (ground0) / 0
		//zv := 1 // value (ground1) / 1
		zv := int(zval)
		xc := hm.ztol(zv) * levelStep
	
		c = color.NRGBA{uint8(xc), uint8(xc), uint8(xc), 255}
		
	} else {
	
		// ZLevel_Ground0
		// ZLevel = 0
		defaultFillValue := 128
		
		fmt.Println("defaultFillValue:", defaultFillValue)
		
		xc := defaultFillValue
		
		c = color.NRGBA{uint8(xc), uint8(xc), uint8(xc), 255}
	}

	fillValue := c

	fmt.Println("fillValue:", fillValue)

	im := image.NewNRGBA(image.Rect(0, 0, sizeX, sizeY))
	_ = im

	// backfill entire background surface with color mygreen
	draw.Draw(im, im.Bounds(), &image.Uniform{c}, image.ZP, draw.Src)

	if true {
		path := "/tmp/test_x.png"
		f, err := os.Create(path)
		if err != nil {
			fmt.Println("cant create file:", path)
			panic(err)
		}
		defer f.Close()

		png.Encode(f, im)	
	}

	//pp(2)

	//return nil
	return im
}

// Load and set new size from image
func (hm *HeightMap) load(path string, resetSize bool) {
	_log.Inf("%F path:", path)

	//pp(2)

	genImage := false
	//genImage := true

	// XXX
	if strings.HasPrefix(path, "gen:") {
		genImage = true
	}

	var im image.Image

	//var r *os.File
	//var err error

	if !genImage {
		r, err := os.Open(path)
		if err != nil {
			println("cant read file: " + path)
			panic(err)
		}
		// Finally ?
		defer r.Close()
		
		//var err error
		im, _, err = image.Decode(r)
		if err != nil {
			panic(err)
		}
	} else {
		// XXX
		im = hm.genImage(path)
	}

	//pp(2)


	tmp := im.Bounds()
	iw, ih := tmp.Dx(), tmp.Dy()

	p("iw:", iw, "ih:", ih)

	if resetSize {
		// Set new dimensions
		hm.setSize(iw, ih)
	}

	hm.loadImageData(im)
}

// Load image data to heightmap
func (hm *HeightMap) loadImageData(im image.Image) {
	_log.Inf("%F im != nil:", im != nil)

	tmp := im.Bounds()
	iw, ih := tmp.Dx(), tmp.Dy()
	if hm.w != iw || hm.h != ih {
		pp("image and hightmap sizes don't match")
	}

	// Convert to grayscale
	im2 := image.NewGray(im.Bounds())
	draw.Draw(im2, im.Bounds(), im, image.Pt(0, 0), draw.Src)

	// XXX fixme: should be y, x instead?
	for y := 0; y < ih; y++ {
		for x := 0; x < iw; x++ {
			v := int(im2.GrayAt(x, y).Y)
			origV := v
			//v := 0
			//p("v:", v)

			// XXX: fixme use value pack/reconstruct functions
			// Map values in -4 .. +4 range, 0 is ground level 0
			levelStep := 256 / 8 // 32
			defaultLevel := 4
			v = (v / levelStep) - defaultLevel
			hm.data[x][y] = v
			//f.cells[x][y].z = ZLevel(v)

			// XXX also save dataOrig
			hm.dataOrig[x][y] = origV
		}
	}
}

func (hm *HeightMap) saveData(path string, data [][]int) {
	_log.Inf("%F path:", path)

	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	rect := image.Rect(0, 0, hm.w, hm.h)
	im := image.NewGray(rect)

	for y := 0; y < hm.h; y++ {
		for x := 0; x < hm.w; x++ {
			//p(hm.data[x][y], uint8(hm.data[x][y]))
			// Reconstructed value
			rv := -1
			if hm.opt.SaveFixedStep {
				//rv = 128 + (data[x][y] * hm.levelStep)
				rv = hm.ztol(data[x][y]) * hm.levelStep
			} else {
				ri := hm.ztol(data[x][y])
				p("2>>>", ri, data[x][y])
				rv = hm.regions[ri]
				p(">>>", data[x][y], ri, rv)
			}
			c := color.Gray{Y: uint8(rv)}
			im.SetGray(x, y, c)
		}
	}
	//pp(2)

	png.Encode(f, im)

	_log.Inf("heightmap saved to: ", path)
}

func (hm *HeightMap) save(path string, saveOrig bool) {
	_log.Inf("%F path:", path)

	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	rect := image.Rect(0, 0, hm.w, hm.h)
	im := image.NewGray(rect)

	/*
		// XXX fixme: should be y, x instead?
		for y := 0; y < hm.h; y++ {
			for x := 0; x < hm.w; x++ {
				if !saveOrig {
					//p(hm.data[x][y], uint8(hm.data[x][y]))
					// Reconstructed value
					rv := 128 + (hm.data[x][y] * hm.levelStep)
					c := color.Gray{Y: uint8(rv)}
					im.SetGray(x, y, c)
				} else {
					v := hm.dataOrig[x][y]
					c := color.Gray{Y: uint8(v)}
					im.SetGray(x, y, c)
				}
			}
		}
	*/

	for y := 0; y < hm.h; y++ {
		for x := 0; x < hm.w; x++ {
			if !saveOrig {
				//p(hm.data[x][y], uint8(hm.data[x][y]))
				// Reconstructed value
				rv := -1
				if hm.opt.SaveFixedStep {
					//rv = 128 + (hm.data[x][y] * hm.levelStep)
					rv = hm.ztol(hm.data[x][y]) * hm.levelStep
				} else {
					ri := hm.ztol(hm.data[x][y])
					p("2>>>", ri, hm.data[x][y])
					rv = hm.regions[ri]
					p(">>>", hm.data[x][y], ri, rv)
				}
				c := color.Gray{Y: uint8(rv)}
				im.SetGray(x, y, c)
			} else {
				v := hm.dataOrig[x][y]
				c := color.Gray{Y: uint8(v)}
				im.SetGray(x, y, c)
			}
		}
	}
	//pp(2)

	png.Encode(f, im)

	_log.Inf("heightmap saved to: ", path)
}

// Warning: ground elevation color/value is inverted
func (hm *HeightMap) saveColor(path string) {
	_log.Inf("%F path:", path)

	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	rect := image.Rect(0, 0, hm.w, hm.h)
	im := image.NewRGBA(rect)

	for y := 0; y < hm.h; y++ {
		for x := 0; x < hm.w; x++ {
			//p(hm.data[x][y], uint8(hm.data[x][y]))
			// Reconstructed value
			rv := -1
			if hm.opt.SaveFixedStep {
				//rv := 128 + (hm.data[x][y] * hm.levelStep)
				rv = hm.ztol(hm.data[x][y]) * hm.levelStep
			} else {
				ri := hm.ztol(hm.data[x][y])
				rv = hm.regions[ri]
			}
			//c := color.Gray{Y: uint8(rv)}
			//im.SetGray(x, y, c)
			var c color.RGBA
			//c := color.RGBA{uint8(rv), uint8(rv), uint8(rv), 255} // Set gray
			div := 2 // 8
			_ = div
			if ZLevel(hm.data[x][y]).IsSea() {
				//c = color.RGBA{uint8(rv / div), uint8(rv / div), uint8(rv), 255}
				c = color.RGBA{0, 0, 128 + uint8(rv), 255}
			} else if ZLevel(hm.data[x][y]).IsGround() {
				//rvInv := rv - int(ZLevel_GroundMax)
				//rvInv := int(ZLevel_GroundMax+ZLevel_Ground0) - rv
				rvInv := (int(ZLevel_GroundMax)*hm.levelStep + int(ZLevel_Ground0)*hm.levelStep) - rv
				rvInvAbs := iabs(rvInv)
				_ = rvInvAbs
				//p("rv:", rv, "rvInv:", rvInv, "rvInvAbs:", rvInvAbs)
				//c = color.RGBA{uint8(rvInv / div), uint8(rvInv), uint8(rvInv / div), 255}
				//c = color.RGBA{128 + uint8(rvInv/div), 128 + uint8(rvInv), 128 + uint8(rvInv/div), 255}
				c = color.RGBA{0, 128 + uint8(rvInv), 0, 255} // XXX 128 is arbitrary, not checked properly
				//c = color.RGBA{128 + uint8(rvInvAbs / div), 128 + uint8(rvInvAbs), 128 + uint8(rvInvAbs / div), 255}
			} else {
				pp("unknown ZLevel:", hm.data[x][y])
			}
			im.SetRGBA(x, y, c)
		}
	}

	png.Encode(f, im)
	//pp(2)

	_log.Inf("color heightmap saved to: ", path)
}

func (hm *HeightMap) saveTxt(path string) {
	_log.Inf("%F path:", path)

	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	// Write header
	f.WriteString(fmt.Sprintf("# hm.w: %d, hm.h: %d\n", hm.w, hm.h))
	f.WriteString("# 0")
	for x := 1; x < hm.w; x++ {
		f.WriteString(fmt.Sprintf("%3d", x))
	}
	f.WriteString("\n\n\n")

	// Write data
	for y := 0; y < hm.h; y++ {
		for x := 0; x < hm.w; x++ {
			f.WriteString(fmt.Sprintf(" %2d", hm.data[x][y]))
		}
		f.WriteString("\n")
	}

	_log.Inf("heightmap txt saved to: ", path)
}

func (hm *HeightMap) clear() {
	if !hm.initialized || len(hm.data) == 0 {
		pp("heightmap is not initialized;",
			hm.initialized, len(hm.data))
	}
	//pp(len(hm.data), hm.w, hm.h, hm.w*hm.h)
	//pp(hm.data)
	//pp(hm.data[0][0])
	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			//p(x, y)
			hm.data[x][y] = 0
		}
	}
}

func (hm *HeightMap) getData(x, y int) int {
	xWrap := pmod(x, hm.w)
	yWrap := pmod(y, hm.h)
	return hm.data[xWrap][yWrap]
}

// Recursive version (flood fill)
func (hm *HeightMap) fill(x, y, repl int, rollback bool, limit int) []CPos {
	_log.Dbg("%F", x, y, repl)

	minX, maxX := 0, hm.w-1
	minY, maxY := 0, hm.h-1
	targ := hm.getData(x, y)
	p("targ:", targ)
	count := 0
	//limit := 9
	res := make([]CPos, 0)
	var ff func(x, y int)
	ff = func(x, y int) {
		var ok bool
		if x >= minX && x <= maxX &&
			y >= minY && y <= maxY {
			ok = true
		}
		p := hm.getData(x, y)
		__p("getPixel:", x, y, p, targ)
		if ok && p == targ {
			//b.SetPx(x, y, repl)
			__p("count:", count)
			if count > limit {
				return
			} else {
				pos := CPos{x, y}
				res = append(res, pos)
				__p("add:", pos, "count:", count)
				hm.data[x][y] = repl
				count++
			}
			/*
				// 4 way
				ff(x-1, y)
				ff(x+1, y)
				ff(x, y-1)
				ff(x, y+1)
			*/
			// 8 way
			ff(x+1, y)
			ff(x-1, y)
			ff(x, y+1)
			ff(x, y-1)
			ff(x+1, y+1)
			ff(x-1, y-1)
			ff(x-1, y+1)
			ff(x+1, y-1)
		}
	}
	ff(x, y)

	if rollback {
		for _, p := range res {
			hm.data[p.x][p.y] = targ
		}
		// Check
		if true {
			for y := 0; y < hm.h; y++ {
				for x := 0; x < hm.w; x++ {
					if hm.data[x][y] == repl {
						pp("rollback failed:", x, y, hm.data[x][y], repl, res)
					}
				}
			}
		}
	}

	_log.Dbg("%F done")
	return res
}
