#!/usr/bin/env python

import os, sys
import json
import pyparsing
from pprint import pprint
from jsonmerge import merge


f = open(sys.argv[1], "r")
data = f.read()
f.close()

includes = []

## parse includes
for l in data.splitlines():
    if l.startswith("#include"):
        v = l.split()[1].strip("\"")
        print("v:", v)
        includes.append(v)

#print(data)

print("includes:")
print(includes)

f = open(includes[0], "r")
data2 = f.read()
f.close()
    
#result = merge(data, data2)
#result = merge(data2, data)

#def remove_comments(x):
#    ret = ""
#   for s in x.split("\n"):
#       ret+=(s[:s.find("//")])
#    return ret

commentFilter = pyparsing.cppStyleComment.suppress()

#xdata = remove_comments(data)
#xdata2 = remove_comments(data2)

xdata = commentFilter.transformString(data)
xdata2 = commentFilter.transformString(data2)

## Remove incldes

def remove_includes(x):
    ret = ""
    for s in x.splitlines():
        if s.startswith("#include"):
            continue
        ret += s + "\n"
    return ret


xdata = remove_includes(xdata)
xdata2 = remove_includes(xdata2)

print("xdata:")
print(xdata)
print("xdata2:")
print(xdata2)

xdatajs = json.loads(xdata)
xdata2js = json.loads(xdata2)

result = merge(xdatajs, xdata2js)
#result = merge(xdata2js, xdatajs)

print
print("result:")
pprint(result, width=40)

fout = open("/tmp/out.json", "w")
out_s = json.dumps(result)
#fout.write(result)
fout.write(out_s)
fout.close()
