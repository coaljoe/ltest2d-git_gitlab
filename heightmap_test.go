package main_module

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHeightMap(t *testing.T) {
	initTest()

	fsize := 64
	fw, fh := fsize, fsize
	hmW := fw
	hmH := fh
	//hmScale := 4.0
	hmOpt := newHeightMapOptions()
	hm := newHeightMap()
	hm.generate(hmW, hmH, hmOpt)
	hm.save(vars.tmpDir+"/hm_test.png", false)
	hm.saveTxt(vars.tmpDir + "/hm_test.txt")
	//hm.clear()

	// Copy hm data (XXX: doesn't work with 2d slices)
	//hmDataCopy := make([][]int, len(hm.data))
	//copy(hmDataCopy, hm.data)
	//hmDataCopy = append([][]int(nil), hm.data...)

	// Copy hm data
	hmDataCopy := make([][]int, hm.w)
	for i := range hmDataCopy {
		hmDataCopy[i] = make([]int, hm.h)
	}
	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			hmDataCopy[x][y] = hm.data[x][y]
		}
	}
	//pp(len(hm.data), len(hmDataCopy))
	assert.Equal(t, len(hm.data), len(hmDataCopy), "wrong data lenght")
	assert.Equal(t, hm.data[0][0], hmDataCopy[0][0], "bad data content")
	// Extra test
	/*
		p(hmDataCopy[0][0], hm.data[0][0])
		hmDataCopy[0][0] = 11
		p(hmDataCopy[0][0], hm.data[0][0])
		pp(2)
	*/

	hm.clear()
	hm.load(vars.tmpDir + "/hm_test.png", false)
	for x := 0; x < hm.w; x++ {
		for y := 0; y < hm.h; y++ {
			//p("xy:", x, y)
			//p(hm.data[x][y], hmDataCopy[x][y])
			if hm.data[x][y] != hmDataCopy[x][y] {
				t.Error("HeightMap load() verification failed")
				t.FailNow()
			}
		}
	}
}
