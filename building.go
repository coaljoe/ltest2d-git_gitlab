package main_module

import (
	. "kristallos.ga/rx/math"
)

// entity
type Building struct {
	*Obj
	*PlayerRecord
	id         int
	name       string
	guiName    string
	// Can be unset / nil
	//player     *Player
	// Permanent campId building type
	campId     CampId
	cx, cy     int
	dimX, dimY int
	//maskX, maskY int
	areaX, areaY int
	killScore    int
	//playerRecord *PlayerRecord
	unitProducer *UnitProducer
	// components
	health       *Health
	combat       *Combat
	bunker       *Bunker
	factory      *Factory
}
func (b *Building) Name() string { return b.name }
func (b *Building) CampId() CampId { return b.campId }

//func newBuilding(campId CampId) *Building {
func newBuilding() *Building {
	_log.Inf("%F")

	b := &Building{
		Obj:       newObj("building"),
		PlayerRecord: newPlayerRecord(nil),
		id:        _ider.GetNextId("Building"),
		//player:    p,
		health:    newHealth(),
		cx:        -1,
		cy:        -1,
		killScore: 100,
	}

	//b.view = newBuildingView(b)
	game.buildingsys.addElem(b)
	return b
}

func (b *Building) getHealth() *Health { return b.health }
func (b *Building) getCombat() *Combat { return b.combat }
func (b *Building) getComponents() []ComponentI {
	return []ComponentI{b.health, b.combat, b.bunker, b.factory}
}

func (b *Building) start() {
	StartEntity(b)
}


/*
func newBuildingForPlayer(p *Player) {
	_log.Inf("%F p:", p)

	//campId := p.campId

	//b := newBuilding(campId)
	b := newBuilding()
	b.assignPlayer(p)
}
*/

// nil means no player was assigned / no owner (?)
func (b *Building) assignPlayer(p *Player) {
	_log.Inf("%F p:", p)

	/*
	if p != nil {
		// XXX fixme?
		//b.campId = p.camp.id
	} else {
		//b.campId = CampId_None
	}
	*/

	// XXX check building/camp compatibility?

	//b.PlayerRecord.assignPlayer(p)
	b.player = p
	//b.playerRecord.setPlayer(p)
}

func (b *Building) canPlaceAt(cx, cy int) bool {
	_log.Inf("%F cx:", cx, "cy:", cy)

	game.field.checkCoords(cx, cy)

	for y := 0; y < b.areaY; y++ {
		for x := 0; x < b.areaX; x++ {
			// XXX fixme: use b.buildingId?
			px := cx + x
			py := cy + y
			
			c := game.field.GetCell(px, py)

			// Check place
			if c.buildingId != -1 {
				//p("dump building:")
				//dump(b)
				//p("dump conflicting building:")
				//dump(c.getBuilding())
				//pp("error: can't place building here, buildingId is not -1:",
				//	c.buildingId, "\npx, py:", px, py, "cx, cy:", cx, cy)
				p("false: can't place building here, buildingId is not -1:",
					c.buildingId, "\npx, py:", px, py, "cx, cy:", cx, cy)
				return false
			}

			if !c.Walkable() {
				p("false: cell isn't walkable: cx, cy:", cx, cy)
				return false
			}
		}
	}

	return true
}

func (b *Building) placeAt(cx, cy int) {
	_log.Inf("%F cx:", cx, "cy:", cy)
	
	game.field.checkCoords(cx, cy)

	if !b.canPlaceAt(cx, cy) {
		pp("can't place building at pos:", cx, cy)
	}

	b.cx = cx
	b.cy = cy

	for y := 0; y < b.areaY; y++ {
		for x := 0; x < b.areaX; x++ {
			// XXX fixme: use b.buildingId?
			px := b.cx + x
			py := b.cy + y
			//game.field.cells[px][py].buildingId = b.id
			//c := &game.field.cells[px][py]
			c := game.field.GetCell(px, py)

			c.buildingId = b.id

			if x < b.dimX && y < b.dimY {
				c.buildingMask = true
			}

			c.open = false
		}
	}

	b.Obj.SetPos(Vec3{float64(b.cx) * cell_size, float64(b.cy) * cell_size, 0})

	if HasPlayer() {
		//game.fowsys.fow.fillCircle(b.cx, b.cy, 10, false)
		GetPlayer().GetFow().fillCircle(b.cx, b.cy, 10, false)
	}
	//getPlayer().getFow().fillCircle(b.cx, b.cy, 3, false)
	PubS("ev_building_place", b)
}

// Find suitable place for building near point at cx, cy
// and place it
func (b *Building) placeNear(cx, cy int) {
	_log.Inf("%F cx:", cx, "cy:", cy)

	game.field.checkCoords(cx, cy)

	if b.canPlaceAt(cx, cy) {
		b.placeAt(cx, cy)
	}


	f := game.field
	c := f.GetCell(cx, cy)
	_ = c

	depth := 1
	//maxDepth := 2
	maxDepth := 10

	// Result position
	resX := 0
	resY := 0
	found := false

	for {
		p("depth:", depth)

		if depth > maxDepth {
			p("maxDepth reached:", maxDepth)
			break
		}
	
		cells := f.getCellNeighboursDepth(cx, cy, depth)
		_ = cells

		p("len cells:", len(cells))

		/*
		p(f.w, f.h)
		p(cells)
		if depth == 2 {
			//pp(2)
		}
		*/

		// Scan through cells

		for _, x := range cells {
			px := x.cx
			py := x.cy

			//p("px:", px)
			//p("py:", py)

			// XXX can be placed at many positions,
			// better if they would be compared
			if b.canPlaceAt(px, py) {
				p("found place:", px, py)
				found = true
				resX = px
				resY = py
				break
			}
		}

		depth++
	}

	p("found:", found)
	p("resX:", resX)
	p("resY:", resY)

	//pp(2)

	if !found {
		pp("can't find suitable position for building")
	} else {
		b.placeAt(resX, resY)
	}

	//pp(2)
}

func (b *Building) cleanupPlace() {
	_log.Dbg("%F")
	for y := 0; y < b.areaY; y++ {
		for x := 0; x < b.areaX; x++ {
			px := b.cx + x
			py := b.cy + y
			c := game.field.GetCell(px, py)
			c.buildingId = -1
			c.buildingMask = false
			c.open = true
		}
	}
}

func (b *Building) destroy() {
	p("Building.destroy:", b.id)
	b.Obj.destroy()
	b.health.destroy()

	// XXX FIXME should not be here?
	// only destroy calls here?
	//game.buildingsys.listElems()
	game.buildingsys.removeElem(b)
	//game.buildingsys.listElems()
	//p(game.buildingsys.getElems())
	//pp(2)
}

func (b *Building) Explode() {
	p("Building.explode")
	b.player.score += b.killScore
	//b.playerRecord.player.score += b.killScore
	//playSound("res/audio/SFX_Explosion_03.wav", vol_sfx)
	if game.hud.selBuilding == b {
		game.hud.unselectBuilding()
	}
	b.cleanupPlace()

	makeExplosion := func(px, py int) { // position of sprites' center
		anim := newAnimatedSprite()
		anim.removeAfter = true
		anim.loop = false
		//anim.load("tmp/smoke/out/r1/x.png", 10)
		//anim.load("res/objects/smoke/smoke1/smoke1.png", 10)
		//anim.load("res/fx/explosion1/explosion1.png", 6)
		anim.load("res/fx/explosion1/explosion1_big.png", 6)
		//anim.frameTime = 100
		//anim.frameTime = 200
		//anim.frameTime = 1000
		//anim.frameTime = 5000
		anim.frameTime = 120
		//anim.originX = 28
		//anim.originY = 60
		//anim.posX = 100
		//anim.posY = 100
		//anim.posX = px
		//anim.posY = py
		anim.posX = px - anim.tileWidth/2
		anim.posY = py - anim.tileHeight/2

		// XXX FIXME? should be done automatically?
		//game.spritesys.addElem(anim)
	}

	for y := 0; y < b.areaY; y++ {
		for x := 0; x < b.areaX; x++ {
			//xcorr :=
			px := (b.cx + x) * cell_size
			py := (b.cy + y) * cell_size
			px += cell_size / 2
			py += cell_size / 2
			makeExplosion(px, py)
		}
	}

	// XXX FIXME should this be done in the view?
	//sh1 := newShed(2, func(s *Shed) {
	//p(anim)
	//pp(2)
	//})
	//_ = sh1
	//pp(2)
	DestroyEntity(b)
}
