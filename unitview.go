package main_module

import (
	"fmt"
	//"os"
	"strings"

	//"github.com/veandco/go-sdl2/img"
	//"github.com/veandco/go-sdl2/sdl"
)

type UnitView struct {
	*View
	anim        *AnimatedSprite
	turretViews map[string]*TurretView
	m           *Unit
}

func newUnitView(m *Unit) *UnitView {
	v := &UnitView{
		View:        newView(),
		turretViews: make(map[string]*TurretView),
		m:           m,
	}
	return v
}

func (v *UnitView) load() {
	_log.Dbg("%F")

	realmName := strings.ToLower(v.m.realm.name())
	if v.m.isStaticUnit() {
		// XXX fixme?
		realmName = "static"
		//pp(2)
	}

	//imagePath := "test.png"
	//imagePath := "res/buildings/bunker/reds/image.png"
	imagePath := fmt.Sprintf("res/units/%s/%s/%s/image.png",
		realmName, v.m.name,
		strings.ToLower(v.m.campId.Name()))

	//pp(imagePath)

	/*
	image, err := img.Load(imagePath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		pp(3)
	}
	//defer image.Free()
	v.image = image

	texture, err := renderer.CreateTextureFromSurface(image)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		pp(4)
	}
	//defer texture.Destroy()
	v.tex = texture
	*/

	if ok, _ := exists(imagePath); ok {
		v.anim = newAnimatedSprite()
		if !v.m.isStaticUnit() {
			v.anim.load(imagePath, 24)
		} else {
			v.anim.load(imagePath, 1)
		}
		v.anim.playing = false
		v.anim.originX = v.anim.tileWidth / 2
		v.anim.originY = v.anim.tileHeight / 2
	}

	v.loaded = true

	_log.Dbg("done %F")
}

func (v *UnitView) destroy() {
}

func (v *UnitView) spawn() {
	if !v.loaded {
		v.load()
	}

	// TurretViews
	{
		for _, tv := range v.turretViews {
			//tv.load()
			tv.spawn()
		}
	}
}

func (v *UnitView) draw() {
	//pp("unitview draw")

	if !v.m.isStaticUnit() {
		dirStepDeg := 15.0
		normAngle := normalizeAngle(v.m.position.dir.angle())
		p("Z", v.m.position.dir.angle(), normAngle, int(normAngle/dirStepDeg))
		v.anim.frame = int(normAngle / dirStepDeg)
	}

	//dst := sdl.Rect{0, 0, 120, 120}
	//var dst sdl.Rect

	//dst.H = 120
	//dst.W = 120

	//dst.W = int32(v.m.dimX * cell_size)
	//dst.H = int32(v.m.dimY * cell_size)

	//nominalY := v.m.areaY * cell_size
	//actualY := int(v.image.H)
	//ycorr := actualY - nominalY

	ycorr := 0

	//if ycorr != 0 {
	//	p(ycorr)
	//}

	//dst.W = int32(v.image.W)
	//dst.H = int32(v.image.H)

	px, py := int(v.m.PosX()), int(v.m.PosY())-ycorr
	//sx, sy := worldToScreenPos(px, py)
	//dst.X = int32(sx)
	//dst.Y = int32(sy)

	//if ycorr != 0 {
	//	p("2>>>", dst)
	//}

	//renderer.Copy(v.tex, nil, &dst)

	if v.anim != nil {
		v.anim.posX = px
		v.anim.posY = py
	}

	// TurretViews
	{
		for _, tv := range v.turretViews {
			//tv.load()
			tv.draw()
		}
	}
}

func (v *UnitView) update(dt float64) {

}
