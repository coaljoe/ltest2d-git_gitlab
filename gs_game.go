package main_module

import (
	//. "kristallos.ga/rx/math"
	//"fmt"

	xg "kristallos.ga/xgui"
)

type GameGameState struct {
	*GameState
}

func newGameGameState() *GameGameState {
	_log.Inf("%F")

	gs := &GameGameState{
		GameState: newGameState("game"),
	}
	
	return gs
}

func (gs *GameGameState) start() {
	_log.Inf("%F")

	//game.createNewDefaultGame()
	// XXX fixme
	if !game.gameCreated {
		game.start()
	}

	//game.gui.enabled = false
	game.gui.SetEnabled(true)
	//game.gui.gameSheet.Show()
	//game.gui.Xgi().SheetSys.SetActiveSheet(game.gui.GameSheet().(*xg.Sheet))
	game.gui.Xgi().SheetSys.SetActiveSheet(game.gui.GameSheet().(xg.SheetI))
	game.hud.enabled = true
	game.hud.start()

	f := game.field
	_ = f

	//tsTest1()

	game.field.spawn()
	//fmt.Printf("%# v", pretty.Formatter(game.field.cells))
	//pp(2)

	// Clean
	/*
		for y := 0; y < f.h; y++ {
			for x := 0; x < f.w; x++ {
				f.cells[x][y].tileId = 0
			}
		}
	*/

	var _ = `
	p("tileIds:")
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			fmt.Printf("%4d", f.cells[x][y].tileId)
		}
		println()
	}
	p("zs:")
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			fmt.Printf("%4d", f.cells[x][y].z)
		}
		println()
	}
	//pp(2)
	`

	if false {
		game.vp.SetPos(0, 120*cell_size)
		game.vp.update(0.1)
		p(game.vp.shx, game.vp.shy)
		p(game.vp.shx/cell_size, game.vp.shy/cell_size)
		p(game.field.w, game.field.h)
		p("overflowY:", game.vp.overflowY, game.vp.overflowY/cell_size)
		p("underflowY:", game.vp.h-game.vp.overflowY, (game.vp.h-game.vp.overflowY)/cell_size)
		p("h:", game.vp.h, game.vp.h/cell_size)
		//px, py := 0, 126*cell_size
		px, py := 0, 4*cell_size
		sx, sy := worldToScreenPos(px, py)
		p("p_:", px, py, py/cell_size)
		p("s_:", sx, sy, sy/cell_size)
		pp(2)
	}
	
	// XXX Test sets
	
	//tsTest2()
	//tsTest3()
	tf := game.testfw
	_ = tf
	//tf.runModule("TsTest3")
	
	if false {
		b4 := MakeBuildingForPlayer("housing", GetPlayer())
		//b4.placeAt(55, 10)
		b4.placeNear(55, 10)
		SpawnEntity(b4)
		//pp(2)
	}

	//playMusic("res/music/test.mp3")
	//l := newLevel1()
	//l.start()
	//game.playLevel("level1")
	//game.playLevel(vars.playLevel)

	// Finally
	game.gui.GameSheet().UpdateMinimapData()
}

func (gs *GameGameState) stop() {
	//game.finishLevel()
	game.stop()
}

func (gs *GameGameState) draw() {
	game.field.view.draw()
	// XXX special
	if game.hud.view != nil {
		game.hud.view.drawBuildingSelectionFrames()
	}
}

func (gs *GameGameState) postDraw() {
	game.field.view.drawFow()
}

func (gs *GameGameState) update(dt float64) {
	game.field.update(dt)
}
