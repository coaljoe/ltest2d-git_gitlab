package main_module

// component
type Factory struct {
}

func newFactory() *Factory {
	f := &Factory{}

	return f
}

func (f *Factory) start() {}
