package main_module

import (
	"fmt"
	//"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	//"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type FieldView struct {
	m     *Field
	atlas *Atlas
	//tilesPix   map[string]*sdl.Texture
	tilesPix    map[string]int
	tileSize    int
	fowView     *FowView
	drawModeFow bool
	//minimapTex *sdl.Texture
	d_drawGrid bool
}

func newFieldView(m *Field) *FieldView {
	v := &FieldView{
		m:     m,
		atlas: newAtlas(),
		//tilesPix: make(map[string]*sdl.Texture, 0),
		tilesPix:    make(map[string]int),
		tileSize:    cell_size,
		fowView:     newFowView(m),
		drawModeFow: false,
		d_drawGrid: false,
	}

	var _ = `
	// Load minimap mockup
	surf, err := img.Load("res/gui/images/hm_color.png")
	if err != nil {
		panic(err)
	}

	tex, err := renderer.CreateTextureFromSurface(surf)
	if err != nil {
		panic(err)
	}

	v.minimapTex = tex
	`

	return v
}

func (v *FieldView) load() {
	/*
	//files, err := filepath.Glob("res/field/tiles/winter/l*.png")
	//files, err := filepath.Glob("res/field/tiles/winter_new/l*.png")
	//files, err := filepath.Glob("res/field/tiles/winter_new2/l*.png")
	//files, err := filepath.Glob("res/field/tiles/winter_new3/l*.png")
	//files, err := filepath.Glob("res/field/tiles/winter_new4/l*.png")
	//files, err := filepath.Glob("res/field/tiles/winter_new5/l*.png")
	//files, err := filepath.Glob("res/field/tiles/winter_new6/l*.png")
	files, err := filepath.Glob("res/field/tiles/winter_new7/[lrs]*.png")
	*/
	
	//files, err := filepath.Glob("res/field/tiles/winter_new7/[lrs]*.png")
	files, err := filepath.Glob("res/field/tiles/winter_new7/[rs]*.png")
	if err != nil {
		panic(err)
	}

	files = append(files, "res/field/tiles/misc/no_tile.png")
	files = append(files, "res/field/tiles/misc/blank_tile.png")

	//pp(files)
	if len(files) == 0 {
		pp("no files found")
	}

	for _, file := range files {
		/*
			image, err := img.Load(file)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
				pp(3)
			}
			//defer image.Free()

			texture, err := renderer.CreateTextureFromSurface(image)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
				pp(4)
			}
			//defer texture.Destroy()
			//v.tex = texture
		*/

		//v.loaded = true
		//v.tilesPix = append(v.tilesPix, texture)
		fileNoExt := strings.TrimSuffix(filepath.Base(file), path.Ext(file))
		//p(file)
		//p(path.Base(file))
		//p(filepath.Base(file))
		//pp(fileNoExt)
		idx := v.atlas.addImageFromPath(file, fileNoExt)
		v.tilesPix[fileNoExt] = idx
	}

	// XXX add tilemap/comb/atlas?
	comb_files, err := filepath.Glob("res/field/tiles/winter_new7_comb/[l]*_comb.png")
	if err != nil {
		panic(err)
	}

	for _, file := range comb_files {
		// Strip ext
		name := strings.TrimSuffix(filepath.Base(file), path.Ext(file))
		name = strings.TrimSuffix(name, "_comb")
		
		idxMap := v.atlas.addTileImageFromPath(file, 28, 28, name)

		for k, val := range idxMap {
			v.tilesPix[k] = val
		}
	}

	//pp(v.tilesPix)

	for _, c := range v.m.GetCells() {
		p("tileId:", c.tileId)
	}
	//pp(2)

	v.atlas.build()
	v.atlas.saveImage(vars.tmpDir + "/ltest2d_atlas.png")

	//pp(2)
}

func (v *FieldView) removeTile(cx, cy int) {
	cell := v.m.GetCell(cx, cy)
	p("removeTile z:", cell.z)
	if cell.z.IsGround() {
		cell.z -= 1
		if cell.z < 0 {
			cell.z = 0
		}
	} else {
		cell.z -= 1
		if cell.z < ZLevel_SeaMax {
			cell.z = ZLevel_SeaMax
		}
	}

	// Finally
	//v.autotile(false)
	//v.autotile(true)
	v.m.autotile(cx-1, cy-1, 3, 3, true)
}

func (v *FieldView) tileMapIdxToXY(idx, tilesX int) (int, int) {
	x := idx % tilesX
	y := idx / tilesX
	return x, y
}

func (v *FieldView) zToL(z ZLevel) string {
	/*
		switch z {
		case ZLevel_Sea3:
			return "l1"
		case ZLevel_Sea2:
			return "l2"
		case ZLevel_Sea1:
			return "l3"
		case ZLevel_Sea0:
			return "l4"
		case ZLevel_Ground0:
			return "l5"
		case ZLevel_Ground1:
			return "l6"
		case ZLevel_Ground2:
			return "l7"
		case ZLevel_Ground3:
			return "l8"
		default:
			p("unknown z:", z)
			panic(2)
		}
	*/
	l := v.m.hm.ztol(int(z))
	lStr := fmt.Sprintf("l%d", l)
	return lStr
}

func (v *FieldView) spawn() {
	v.load()
	v.fowView.spawn()
}

func (v *FieldView) drawFow() {
	v.drawModeFow = true
	v.draw()
	v.drawModeFow = false
}

func (v *FieldView) drawCell(x, y int, dst sdl.Rect) {
	if v.drawModeFow {
		if !game.fowsys.disableFowDraw {
			v.fowView.drawCell(x, y, dst)
		}
		return
	}
	cell := &v.m.cells[x][y]
	if cell.tileId != -1 &&
		cell.river == RiverState_None &&
		cell.shore == ShoreState_None {
		l := v.zToL(cell.z)
		//renderer.Copy(v.tilesPix[l + "_01"], nil, &dst)
		//pixId := "l5_" + strconv.Itoa(v.m.cells[x][y].tileId)
		//pixId := l + "_" + strconv.Itoa(cell.tileId)
		tx, ty := v.tileMapIdxToXY(cell.tileId, 8)
		pixId := fmt.Sprintf("%s_tile_%d_%d", l, tx, ty)
		//pp("pixId:", pixId)
		idx, ok := v.tilesPix[pixId]
		if !ok {
			//p("bad pixId:", pixId)
			var ok2 bool
			//idx, ok2 = v.tilesPix["no_tile"]
			idx, ok2 = v.tilesPix["blank_tile"]
			if !ok2 {
				pp("debug tile key error")
			}
			//pp(2)
		}

		if idx != -1 {
			//renderer.Copy(v.tilesPix[pixId], nil, &dst)
			//v.atlas.drawImageIdx(v.tilesPix[pixId], int(dst.X), int(dst.Y))
			v.atlas.drawImageIdx(idx, int(dst.X), int(dst.Y))
		}
	}

	/*
		// Render rivers
		if v.m.cells[x][y].river != RiverState_None {
			renderer.Copy(v.tilesPix["r_8"], nil, &dst)
		}
	*/
	// Render rivers
	if cell.river != RiverState_None {
		pixId := "r_" + strconv.Itoa(cell.tileId)
		if _, ok := v.tilesPix[pixId]; !ok {
			p("bad pixId:", pixId)
			pp(2)
		} else {
			//renderer.Copy(v.tilesPix[pixId], nil, &dst)
			v.atlas.drawImageIdx(v.tilesPix[pixId], int(dst.X), int(dst.Y))
		}
	}

	if cell.shore != ShoreState_None {
		pixId := "s_" + strconv.Itoa(cell.tileId)
		topCell := game.field.GetCell(x, y-1)
		bottomCell := game.field.GetCell(x, y+1)

		if cell.shore == ShoreState_Corner {
			//if (topCell.tileId != cell.tileId) || !topCell.shore {
			if topCell.shore == ShoreState_Shore {
				pixId += "_bottom_tip"
				//} else if (bottomCell.tileId != cell.tileId) || !bottomCell.shore {
			} else if bottomCell.shore == ShoreState_Shore {
				pixId += "_top_tip"
			}
		}
		if cell.tileId == 36 || cell.tileId == 28 {
			if _, ok := v.tilesPix[pixId]; !ok {
				p("bad pixId:", pixId)
				pp(2)
			} else {
				//renderer.Copy(v.tilesPix[pixId], nil, &dst)
				v.atlas.drawImageIdx(v.tilesPix[pixId], int(dst.X), int(dst.Y))
			}
		}
	}
	
	if v.d_drawGrid {
		c := ColorBlack
		c.A = 80
		l := cell_size
		err := renderer.SetDrawBlendMode(sdl.BLENDMODE_BLEND)
		if err != nil {
			panic(err)
		}
		DrawHLine(int(dst.X), int(dst.Y) + (l - 1), l, c)
		DrawVLine(int(dst.X) + (l - 1), int(dst.Y), l, c)
		err = renderer.SetDrawBlendMode(sdl.BLENDMODE_NONE)
		if err != nil {
			panic(err)
		}
	}
}

func (v *FieldView) draw() {
	//return
	//p("fieldview.draw:")
	//pp(v.tilesPix)
	if game.vp.w > v.m.w*cell_size || game.vp.h > v.m.h*cell_size {
		p(game.vp.w, ">", v.m.w*cell_size, "||", game.vp.h, ">", v.m.h*cell_size)
		pp("error: game viewport size is too small for current field size")
	}

	renderedCells := 0
	//for y := 0; y < v.m.h; y++ {
	//	for x := 0; x < v.m.w; x++ {
	//for y := maxi(game.vp.shy/v.tileSize, 0); y < (game.vp.shy+game.vp.h)/v.tileSize+1; y++ {
	for y := maxi(game.vp.shy/v.tileSize, 0); y < mini((game.vp.shy/v.tileSize+game.vp.h/v.tileSize)+2, v.m.h); y++ {
		for x := maxi(game.vp.shx/v.tileSize, 0); x < mini(((game.vp.shx+game.vp.w)/v.tileSize)+2, v.m.w); x++ {
			//p("fieldview.draw x:", x, "y:", y)
			px := int32(x*v.tileSize - game.vp.shx)
			py := int32(y*v.tileSize - game.vp.shy)
			dst := sdl.Rect{px, py, int32(v.tileSize), int32(v.tileSize)}
			//renderer.Copy(v.tilesPix["l6_01"], nil, &dst)
			//if v.m.cells[x][y].z == ZLevel_Ground0 {
			v.drawCell(x, y, dst)
			renderedCells += 1
		}
	}
	//p("renderedCells", renderedCells)

	//renderer.Copy(v.tilesPix[0], nil, nil)

	//dst := sdl.Rect{0, 0, 120, 120}
	//renderer.Copy(v.tilesPix[0], nil, &dst)

	if vars.wrapField {
		//overflowX := pmod(int(math.Round(v.x))+v.w, game.field.w*cell_size)
		//overflowX := (game.vp.shx + game.vp.w) - game.field.w*cell_size
		overflowX := game.vp.overflowX
		//p("2>>>", overflowX)
		if overflowX > 0 {
			//for y := 0; y < 1; y++ {
			//for y := 0; y <= game.vp.h/v.tileSize; y++ {
			//for y := 0; y < v.m.h; y++ {
			for y := game.vp.shy / v.tileSize; y < (game.vp.shy/v.tileSize+game.vp.h/v.tileSize)+2; y++ {
				for x := 0; x < 1+overflowX/v.tileSize; x++ {
					px := int32(x*v.tileSize - game.vp.shx)
					py := int32(y*v.tileSize - game.vp.shy)
					dst := sdl.Rect{px, py, int32(v.tileSize), int32(v.tileSize)}
					dst = sdl.Rect{int32(vars.resX - v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
					//dst = sdl.Rect{int32((vars.resX - overflowX) + x*v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
					dst = sdl.Rect{int32((game.vp.w - overflowX) + x*v.tileSize), int32(y*v.tileSize - game.vp.shy), int32(v.tileSize), int32(v.tileSize)}
					//v.drawCell(0, 0, dst)
					//v.drawCell(x, 0, dst)
					if v.m.hasCoords(x, y) {
						v.drawCell(x, y, dst)
						renderedCells += 1
					}
				}
			}
		}

		overflowY := game.vp.overflowY
		if overflowY > 0 {
			//for y := 0; y < v.m.h; y++ {
			for y := 0; y < (overflowY/v.tileSize)+1; y++ {
				//for y := v.m.h - (overflowY / v.tileSize); y < v.m.h; y++ {
				//for y := overflowY / v.tileSize; y < v.m.h; y++ {
				//for x := 0; x < game.vp.w/v.tileSize; x++ {
				// From vp.CellPosX to vp.CellPosX + vp.WidthCells
				for x := int(game.vp.x / float64(v.tileSize)); x < (int(game.vp.x/float64(v.tileSize))+int(game.vp.w/v.tileSize))+2; x++ {
					//for x := 0; x < 10; x++ {
					px := int32(x*v.tileSize - game.vp.shx)
					//py := int32(y*v.tileSize - game.vp.shy)
					//py := int32(y*v.tileSize + overflowY - game.vp.shy)
					py := int32(y*v.tileSize + (game.vp.h - overflowY))
					dst := sdl.Rect{px, py, int32(v.tileSize), int32(v.tileSize)}
					//p("3>>>", dst)
					//dst = sdl.Rect{int32(vars.resX - v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
					//dst = sdl.Rect{int32((vars.resX - overflowX) + x*v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
					//dst = sdl.Rect{int32((game.vp.w - overflowX) + x*v.tileSize), int32(y*v.tileSize - game.vp.shy), int32(v.tileSize), int32(v.tileSize)}
					//dst = sdl.Rect{int32(x * v.tileSize), int32((game.vp.h - overflowY) + y*v.tileSize - game.vp.shy), int32(v.tileSize), int32(v.tileSize)}
					//v.drawCell(0, 0, dst)
					//v.drawCell(x, 0, dst)
					//p("ZZZ", x, y, v.m.getCell(x, y), v.m.hasCellCoord(x, y))
					if v.m.hasCoords(x, y) {
						v.drawCell(x, y, dst)
						renderedCells += 1
					}
				}
			}
		}

		// Draw bottom right (D) segment (placed at top left corner of the map)
		//
		// A | B
		// --+--
		// C | D
		if overflowX > 0 && overflowY > 0 {
			for y := 0; y < (overflowY/v.tileSize)+1; y++ {
				//for y := v.m.h - (overflowY / v.tileSize); y < v.m.h; y++ {
				//for y := overflowY / v.tileSize; y < v.m.h; y++ {
				//for x := 0; x < game.vp.w/v.tileSize; x++ {
				// From vp.CellPosX to vp.CellPosX + vp.WidthCells
				//for x := int(game.vp.x / float64(v.tileSize)); x < (int(game.vp.x/float64(v.tileSize))+int(game.vp.w/v.tileSize))+1; x++ {
				for x := 0; x < (int(game.vp.w/v.tileSize))+1; x++ {
					//for x := 0; x < 10; x++ {
					//for x := 0; x < v.m.w; x++ {
					//qpx := int32(x*v.tileSize - game.vp.shx)
					px := int32((game.vp.w - overflowX) + x*v.tileSize)
					//py := int32(y*v.tileSize - game.vp.shy)
					//py := int32(y*v.tileSize + overflowY - game.vp.shy)
					py := int32(y*v.tileSize + (game.vp.h - overflowY))
					dst := sdl.Rect{px, py, int32(v.tileSize), int32(v.tileSize)}
					//p("3>>>", dst)
					//v.drawCell(0, 0, dst)
					//v.drawCell(x, 0, dst)
					//p("ZZZ", x, y, v.m.hasCellCoord(x, y))
					//if v.m.hasCellCoord(x, y) {
					v.drawCell(x, y, dst)
					renderedCells += 1
					//}
				}
			}
		}

		/*
			if game.vp.underflowX != 0 {
				for y := 0; y < v.m.h; y++ {
					for x := 0; x < 1+iabs(game.vp.underflowX)/v.tileSize; x++ {
						//px := int32(x*v.tileSize - game.vp.shx)
						//py := int32(y*v.tileSize - game.vp.shy)
						//dst := sdl.Rect{px, py, int32(v.tileSize), int32(v.tileSize)}
						//dst := sdl.Rect{int32(vars.resX - v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
						//dst := sdl.Rect{int32(-game.vp.underflowX - x * v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
						dst := sdl.Rect{int32(-game.vp.underflowX - x * v.tileSize), int32(y*v.tileSize - game.vp.shy), int32(v.tileSize), int32(v.tileSize)}
						//dst = sdl.Rect{int32((vars.resX - overflowX) + x*v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
						//dst = sdl.Rect{int32((game.vp.w - overflowX) + x*v.tileSize), int32(y*v.tileSize - game.vp.shy), int32(v.tileSize), int32(v.tileSize)}
						//v.drawCell(0, 0, dst)
						//v.drawCell(x, 0, dst)
						v.drawCell(x, y, dst)
					}
				}
			}
		*/
		/*
			if game.vp.underflowX != 0 {
				pxStart := int32(0*v.tileSize - game.vp.shx - game.vp.w)
				p("pxStart >>>", pxStart, game.vp.shx)
				for y := 0; y < v.m.h; y++ {
					for x := 0; x < game.vp.w/v.tileSize; x++ {
						//px := int32(x*v.tileSize - game.vp.shx - v.tileSize)
						px := int32(x*v.tileSize - game.vp.shx - game.vp.w + 16)
						//p("px >>>", px, game.vp.shx)
						py := int32(y*v.tileSize - game.vp.shy)
						//dst := sdl.Rect{px, py, int32(v.tileSize), int32(v.tileSize)}
						dst := sdl.Rect{px, py, int32(v.tileSize), int32(v.tileSize)}
						//dst := sdl.Rect{int32(vars.resX - v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
						//dst := sdl.Rect{int32(-game.vp.underflowX - x * v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
						//dst := sdl.Rect{int32(-game.vp.w + x * v.tileSize), int32(y*v.tileSize - game.vp.shy), int32(v.tileSize), int32(v.tileSize)}
						//dst := sdl.Rect{int32(-game.vp.w + x * v.tileSize), int32(y*v.tileSize - game.vp.shy), int32(v.tileSize), int32(v.tileSize)}
						//dst = sdl.Rect{int32((vars.resX - overflowX) + x*v.tileSize), 0, int32(v.tileSize), int32(v.tileSize)}
						//dst = sdl.Rect{int32((game.vp.w - overflowX) + x*v.tileSize), int32(y*v.tileSize - game.vp.shy), int32(v.tileSize), int32(v.tileSize)}
						//v.drawCell(0, 0, dst)
						//v.drawCell(x, 0, dst)
						//v.drawCell(x, y, dst)
						cx := (game.field.w - game.vp.w/v.tileSize) + x
						//p("cx:", cx, cx+x)
						//v.drawCell((game.field.w * v.tileSize - game.vp.w / v.tileSize) + x, y, dst)
						v.drawCell(cx, y, dst)
					}
				}
			}
		*/
	}

	if true {
		v.fowView.draw()
	}
}
