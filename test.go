package main_module

import (
	"fmt"
)

var _test_initialized = false

func initTest() {
	fmt.Println("-- init test begin --")
	if !_test_initialized {
		//gamedir := "/home/j/dev/misc/ltest/go"
		//os.Chdir(gamedir)
		//initAll()
		InitApp()
		InitBaseGame()

		vars.dev = true

		// Init test game
		game.createNewTestGame()
		game.newgametool.initNewTestGame()
		//game.newgametool.startGame()
		game.start() // XXX fixme?

		_test_initialized = true
	}
	fmt.Println("-- init test done --")
}
