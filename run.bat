@echo off
setlocal
call env.bat
for /f "delims=" %%i in ('hg id -i') do set _rev=%%i

@rem always clean
@rem go install -v rx rx/math lib/ecs lib/xlog lib/sr lib/pubsub 
del ltest2d.exe 2> nul

@rem go run -v src\main.go
go build -v -ldflags="-X 'main.BuildRevision=%_rev%'" -o ltest2d.exe

if "%1" == "-trace" (
  goto trace
) else if "%1" == "-test1" (
  goto test1
) else (
  goto run
)

:trace
shift
apitrace trace -o trace.trace ltest2d.exe %1 %2 %3 %4 %5
goto end

:test1
@rem test a single test/pattern
@rem usage: run -test1 TestPathfind
go test -v ltest -test.run %2 %3 %4 %5
goto end

:run
ltest2d.exe -dev %*
goto end

:end
endlocal
