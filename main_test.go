package main_module

import (
	"os"
	"path"
	"runtime"
	"testing"
)

func TestMain(m *testing.M) {
	println("TestMain()")
	//pp(2)
	//_ = m

	_, filename, _, _ := runtime.Caller(0)
	//println("filename:", filename)
	dir := path.Join(path.Dir(filename))
	err := os.Chdir(dir)
	if err != nil {
		panic(err)
	}

	curDir, err2 := os.Getwd()
	if err != nil {
		panic(err2)
	}

	println("curDir:", curDir)

	// Set new appRoot
	println("set new appRoot")
	appRoot := curDir

	println("appRoot: ", appRoot)
	os.Chdir(appRoot)

	vars.appRoot = appRoot

	//pp(2)

	initTest()
	ret := m.Run()
	os.Exit(ret)
}
