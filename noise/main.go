package noise

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
)

func main() {
	fmt.Println("main()")
	//v := Pnoise2(0.1, 0.1, 0, 0)
	v := Pnoise2(0.1, 0.1, 1, 1)
	fmt.Println("v:", v)

	//w, h := 128, 128
	//w, h := 256, 256
	w, h := 256, 128

	path := "/tmp/out.png"
	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	rect := image.Rect(0, 0, w, h)
	im := image.NewGray(rect)

	noiseScale := 10.0
	//aspect := 2.0
	aspect := float64(w) / float64(h)

	sx := noiseScale / float64(w)
	sy := noiseScale / float64(h)

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			nx := float64(x) * sx
			ny := float64(y) * sy / aspect
			//nv := Pnoise2(nx, ny, w, h)
			//nv := Pnoise2(nx, ny, w/2, h/2)
			//nv := Pnoise2(nx, ny, 0, 0)
			nv := Pnoise2(nx, ny, 5, 5)
			//nv := Pnoise2(nx, ny, 6, 6)

			// Normalize nv to 0, 1 range
			nvNorm := (nv - -1.0) / (1.0 - -1.0)

			rv := int(nvNorm * 256)
			c := color.Gray{Y: uint8(rv)}
			im.SetGray(x, y, c)
		}
	}

	png.Encode(f, im)

	println("done")
}
