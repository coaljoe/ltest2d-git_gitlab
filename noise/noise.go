package noise

// The output is in -1..1 range.
func PeriodicPerlinNoise2(x, y float64, px, py int) float64 {
	v := Pnoise2(x, y, px, py)
	return v
}
