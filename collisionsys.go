package main_module

var _ = `

import ()

type CollisionSys struct {
	*GameSystem
}

func newCollisionSys() *CollisionSys {
	s := &CollisionSys{}
	s.GameSystem = newGameSystem("CollisionSys", "Collision system", s)
	return s
}

func (s *CollisionSys) onCollision(o1, o2 *Obj) {
	p("CollisionSys.OnCollision", o1.id, o2.id)
	p(o1.objtype, o2.objtype)

	/*
		if (o1.objtype == "enemy" && o2.objtype == "ship") ||
			(o1.objtype == "ship" && o2.objtype == "enemy") {
			//o2.doDestroy()
			//game.objsys.getObjById(o1.id)
			pv(o1)
			pv(o2)
			var enemy *Enemy
			if o1.objtype == "enemy" {
				//enemy = game.entitysys.getEntityById(o1.id).(*Enemy)
				enemyI := game.entitysys.getEntityById(o1.id)
				if enemyI != nil {
					enemy = enemyI.(*Enemy)
				}
			} else if o2.objtype == "enemy" {
				//enemy = game.entitysys.getEntityById(o2.id).(*Enemy)
				enemyI := game.entitysys.getEntityById(o2.id)
				if enemyI != nil {
					enemy = enemyI.(*Enemy)
				}
			} else {
				pp(2)
			}
			if enemy != nil {
				enemy.explode()
				if !game.ship.immortal {
					game.ship.explode()
				}
			}
			//destroyEntity(o2)
		} else if (o1.objtype == "enemy" && o2.objtype == "shot") ||
			(o1.objtype == "shot" && o2.objtype == "enemy") {
			enemyi := game.entitysys.getEntityById(o2.id)
			if enemyi != nil {
				enemy := enemyi.(*Enemy)
				//enemy.combat.takeDamage(50)
				enemy.combat.takeDamage(100)
			}
			shoti := game.entitysys.getEntityById(o1.id)
			if shoti != nil {
				shot := shoti.(*Shot)
				shot.explode()
			}
			//pp(2)
		} else if (o1.objtype == "enemy" && o2.objtype == "enemy-shot") ||
			(o1.objtype == "enemy-shot" && o2.objtype == "enemy") {
			// do nothing
		} else if (o1.objtype == "ship" && o2.objtype == "enemy-shot") ||
			(o1.objtype == "enemy-shot" && o2.objtype == "ship") {
			if !game.ship.immortal {
				game.ship.explode()
			}
		} else if (o1.objtype == "shot" && o2.objtype == "ship") ||
			(o1.objtype == "ship" && o2.objtype == "shot") {
			// do nothing
		} else if o1.objtype == "enemy" && o2.objtype == "enemy" {
			// do nothing
		} else if o1.objtype == "ship" && o2.objtype == "ship" {
			// do nothing
		} else {
			pp("unknown objtypes:", o1.objtype, o2.objtype)
		}
	*/
}

func (s *CollisionSys) update(dt float64) {
}
`
