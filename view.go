package main_module

import (
	//"rx"
	"github.com/veandco/go-sdl2/sdl"
)

//var modelCache map[string]*rx.Model

type ViewI interface {
	//getNode(name string) *rx.Node
	draw()
	load()
	spawn()
	destroy()
	update(dt float64) // replace with UpdateableI
	//getView() ViewI
}

type View struct {
	//model   *rx.Model
	image   *sdl.Surface
	tex     *sdl.Texture
	spawned bool
	loaded  bool
}

func newView() *View {
	v := &View{
		//model: rx.NewModel(),
	}
	/*
		// init modelCache
		if len(modelCache) == 0 {
			modelCache = make(map[string]*rx.Model)
		}
	*/
	return v
}

/*
func (v *View) getView() ViewI {
	return v
}
*/

/*
func (v *View) getNode(name string) *rx.Node {
	//return v.model.GetNode("body")
	v.model.PrintNodes()
	//pp(2)
	return v.model.GetNode(name)
}
*/
