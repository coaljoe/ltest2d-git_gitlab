package main_module

import (
	"fmt"
)

// ?
/*
type Decoder struct {
type DataDecoder struct {
type DataLoader struct {

}
*/

func decodeZLevelFromString(s string) ZLevel {
	switch s {
	case "sea0":
		return ZLevel_Sea0
	case "sea1":
		return ZLevel_Sea1
	case "sea2":
		return ZLevel_Sea2
	case "sea3":
		return ZLevel_Sea3
	case "ground0":
		return ZLevel_Ground0
	case "ground1":
		return ZLevel_Ground1
	case "ground2":
		return ZLevel_Ground2
	case "ground3":
		return ZLevel_Ground3
	default:
		panic(fmt.Sprintf("error: unknown ZLevel string s=%s", s))
	}
}

// XXX ?
//	ZLevel_Sea0    ZLevel = -1 // l0
//	ZLevel_Sea1    ZLevel = -2 // l1
//	ZLevel_Sea2    ZLevel = -3 // l2
//	ZLevel_Sea3    ZLevel = -4 // l3
//	ZLevel_Ground0 ZLevel = 0  // l4
//	ZLevel_Ground1 ZLevel = 1  // l5
//	ZLevel_Ground2 ZLevel = 2  // l6
//	ZLevel_Ground3 ZLevel = 3  // l7
func decodeZLevelFromInt(v int) ZLevel {
	switch v {
	case -1:
		return ZLevel_Sea0
	case -2:
		return ZLevel_Sea1
	case -3:
		return ZLevel_Sea2
	case -4:
		return ZLevel_Sea3
	case 0:
		return ZLevel_Ground0
	case 1:
		return ZLevel_Ground1
	case 2:
		return ZLevel_Ground2
	case 3:
		return ZLevel_Ground3
	default:
		panic(fmt.Sprintf("error: unknown ZLevel int v=%d", v))
	}
}
