package main

import "lib/ecs"

var (
	_World = newWorld()
)

type World struct {
	*ecs.World
}

func newWorld() *World {
	w := &World{
		ecs.NewWorld(),
	}
	return w
}

var _ = `
func (w *World) _CreateEntity() *ecs.Entity {
	/*
		en := w.CreateEntity()
		en_wrapped := NewEntity()
		en_wrapped.Entity = en
	*/
	en := ecs.NewEntity()
	w.AddEntity(en)
	return en
}
`
