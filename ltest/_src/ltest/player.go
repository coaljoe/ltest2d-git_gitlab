package main

import (
	"fmt"
)

type Player struct {
	id    int
	ptype PlayerType
	name  string
	camp  *Camp
	ai    *PlayerAi
	// Stats
	//militaryScore    int
	//statsUpdateTime  float64
	//statsUpdateTimer *Timer
	vc       VarsCache
	disabled bool
}

func newPlayer(ptype PlayerType) *Player {
	p := &Player{
		id:    _ider.GetNextId("Player"),
		ptype: ptype,
		name:  "Unknown",
		//statsUpdateTime:  2, //10,
		//statsUpdateTimer: newTimer(true),
		vc:       newVarsCache(),
		disabled: false,
	}
	if ptype == PlayerType_AI {
		p.ai = newPlayerAi(p)
	}

	// Automatically add to PlayerSys
	//game.playersys.addPlayer(p) // Fixme: remove?
	return p
}

func (p *Player) isHuman() bool {
	if p.ptype == PlayerType_Human {
		return true
	}
	return false
}

func (p *Player) isAi() bool {
	return !p.isHuman()
}

func (p *Player) String() string {
	return fmt.Sprintf("Player<name: %s>", p.name)
}

func (p *Player) getFow() *Fow {
	return _FowSys.getFowForPlayer(p)
}

func (p *Player) isDefaultPlayer() bool {
	return p == getPlayer()
}

var _ = `
func (p *Player) getMilitaryScore() int {
	// Return vars cache
	//vcId := fmt.Sprintf("militaryRating_%d", p.id)
	vcId := "militaryRating"
	if ok, val := p.vc.getNotExpired(vcId); ok {
		return val.(int)
	}

	// Https://www.reddit.com/r/civ/comments/1d0lz5/how_the_ai_computes_your_military_strength_based/
	//pp(pai.p)
	units := game.unitsys.getUnitsForPlayer(p)
	//pp(units)
	//units := game.unitsys.getUnits()
	unitsPower := 0.0
	unitsNum := 0
	for _, u := range units {
		uc := c_Unit(u)
		/*
			_, maxAr := c_Combat(u).getAttackRatingRange()
			//unitPower := maxAr
			unitPower := math.Pow(float64(maxAr), 1.5)
		*/
		unitPower := 0.0
		// Sum of all units' weapons power (attackRating^1.5)
		// So the unit with few poverful weapons will get bigger score
		// Because it can attack from both weapons(?)
		for _, w := range c_Combat(u).getWeapons() {
			unitPower += math.Pow(float64(w.attackRating()), 1.5)
		}

		// Navy units are twice less powerful in military score
		// (like in original method from a civ game)
		// XXX: what about air units?
		if uc.realm == RealmType_Navy {
			unitPower /= 2.0
		}

		//unitPower := math.Pow(float64(maxAr), 1.5) * math.Pow(uc.maxSpeed, 0.3)
		unitsPower += unitPower
		unitsNum += 1
		__p(uc.utype)
		__p(uc.getPlayer())
	}

	//pp(unitsPower, unitsNum)

	militaryScore := int(unitsPower)

	// Save vars cache
	p.vc.put(vcId, 10, militaryScore)

	return militaryScore
}
`

func (p *Player) getMilitaryScore() int {
	// Return vars cache
	//vcId := fmt.Sprintf("militaryRating_%d", p.id)
	vcId := "militaryScore"
	if ok, val := p.vc.getNotExpired(vcId); ok {
		return val.(int)
	}

	mspu := p.getMilitaryScorePerUnit()
	//pp(mspu)
	unitsNum := 0
	unitsPower := 0.0
	for id, ms := range mspu {
		u := _UnitSys.getUnitById(id)
		uc := c_Unit(u)

		unitPower := ms

		unitsPower += unitPower
		unitsNum += 1
		__p(uc.utype)
		__p(uc.getPlayer())
	}

	//pp(unitsPower, unitsNum)

	militaryScore := int(unitsPower)

	// Save vars cache
	p.vc.put(vcId, 10, militaryScore)

	return militaryScore
}

func (p *Player) getMilitaryScorePerUnit() map[int]float64 {
	// Return vars cache
	vcId := "militaryScorePerUnit"
	if ok, val := p.vc.getNotExpired(vcId); ok {
		return val.(map[int]float64)
	}

	r := make(map[int]float64, 0)

	// Https://www.reddit.com/r/civ/comments/1d0lz5/how_the_ai_computes_your_military_strength_based/
	//pp(pai.p)
	units := game.unitsys.getUnitsForPlayer(p)
	//pp(units)
	//units := game.unitsys.getUnits()
	for _, u := range units {
		uc := c_Unit(u)
		unitPower := uc.getUnitMilitaryScore()
		r[u.Id()] = unitPower
	}

	// Save vars cache
	p.vc.put(vcId, 10, r)

	return r
}

/*
func (p *Player) _updateStats() {
	ms := p.getMilitaryScore()
	p.militaryScore = ms
}
*/

func (p *Player) update(dt float64) {
	if p.disabled {
		return
	}
	if p.ai != nil {
		p.ai.update(dt)
	}
	//pp(pai.statsUpdateTimer.dt())
	/*
		if p.statsUpdateTimer.dt() > p.statsUpdateTime {
			p._updateStats()
			p.statsUpdateTimer.restart()
		}
	*/
}

///////////////
// PlayerType

type PlayerType int

const (
	PlayerType_Human PlayerType = iota
	PlayerType_AI
)

func (p PlayerType) String() string {
	switch p {
	case PlayerType_Human:
		return "Human"
	case PlayerType_AI:
		return "AI"
	default:
		panic("unknown PlayerType")
	}
}
