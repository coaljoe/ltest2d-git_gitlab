// Units realm types.
// All buildings have ground realm type(?).
package main

import (
	"fmt"
	"strings"
)

type RealmType int

const (
	RealmType_Ground RealmType = iota
	RealmType_Air
	RealmType_Navy
)

func realmTypeFromString(s string) RealmType {
	s_lc := strings.ToLower(s)
	switch s_lc {
	case "ground":
		return RealmType_Ground
	case "air":
		return RealmType_Air
	case "navy":
		return RealmType_Navy
	default:
		panic("unknown realmType; s: " + s_lc)
	}
}

func (rt RealmType) name() string {
	switch rt {
	case RealmType_Ground:
		return "Ground"
	case RealmType_Air:
		return "Air"
	case RealmType_Navy:
		return "Navy"
	default:
		panic(fmt.Sprintf("unknown realm %v", rt))
	}
}
