package main

import (
	"fmt"
	"os"
	"reflect"
	"time"

	"github.com/davecgh/go-spew/spew"
)

// Alias for use in places where 'p' doesn't work
var __p = p
var _ = __p

var p = fmt.Println
var pf = fmt.Printf

func cknil(c interface{}) {
	if c == nil || reflect.ValueOf(c).IsNil() {
		panic("Check nil failed.")
	}
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func pv(args ...interface{}) {
	fmt.Printf("pv: %#v\n", args)
}

func pp(args ...interface{}) {
	fmt.Println(args)
	panic("pp")
}

//func pf(args ...interface{}) {
//	fmt.Printf(args...)
//}

func sleepsec(t float64) {
	// Sleep float seconds
	time.Sleep(time.Duration(t*1000) * time.Millisecond)
}

func dump(s interface{}) {
	cs := spew.NewDefaultConfig()
	cs.ContinueOnMethod = true
	cs.MaxDepth = 3
	//cs.MaxDepth = 5
	cs.Dump(s)
	//spew.Dump(s)
}

func dumpDepth(s interface{}, depth int) {
	cs := spew.NewDefaultConfig()
	cs.ContinueOnMethod = true
	cs.MaxDepth = depth
	cs.Dump(s)
	//spew.Dump(s)
}

func pdump(s interface{}) {
	dump(s)
	panic("pdump")
}

// Exists returns whether the given file or directory exists or not
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

/***** Log *****/

//var logger
/*
func init() {
  println("util.log init")
}
*/

/*
func Log1(msg string) {
	var buf bytes.Buffer
	logger := log.New(&buf, "log: ", log.Lshortfile)
	logger.Print(msg)

	fmt.Print(&buf)
}
*/
