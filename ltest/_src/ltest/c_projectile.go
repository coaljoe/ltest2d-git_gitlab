package main

import (
	"lib/ecs"
	. "rx/math"
)

var ProjectileT = &Projectile{}

// Component
type Projectile struct {
	*Obj
	*Position
	ptype         string // Shell, burst, rocket, bomb, custom?
	sPos        Vec3
	dPos        Vec3
	moveSpeed     float64
	pathCurveness float64
	ammotype      *AmmoType
	emitter       *ecs.Entity // Link to emitter entity
	target        *ecs.Entity // Link to target
	//moveStartTime  float64
	View ViewI
}

func newProjectile(en *ecs.Entity, sPos, dPos Vec3, speed,
	pathCurveness float64, ammotype *AmmoType, emitter *ecs.Entity,
	target *ecs.Entity) *Projectile {

	p := &Projectile{
		//Obj:           newObj(en, "projectile obj"),
		ptype:         ammotype.projectileType,
		sPos:        sPos,
		dPos:        dPos,
		moveSpeed:     speed,
		pathCurveness: pathCurveness,
		ammotype:      ammotype,
		emitter:       emitter,
		target:        target,
	}
	en.AddComponent(p)
	p.Obj = newObj(en, "projectile obj")
	p.Position = newPosition(en)
	p.SetPos(sPos)
	// Go
	p.setDPos(dPos)
	p.speed = speed
	return p
}

func (p *Projectile) spawn() {
	//p.View = newProjectileView(GetEntity(p), p)
	p.View = makeProjectileView(GetEntity(p), p)
	p.View.spawn()
}

func (p *Projectile) delete() {
	// Deleting obj
	//p.Obj.delete()

	// Removing self from fxsys
	fxsys := game.getSystem("FxSys")
	fxsys.removeElem(GetEntity(p))
}

func (p *Projectile) update(dt float64) {

	// Finised travel
	if !p.isMoving() {
		//p.delete() // XXX call explicit?
		//p.View.destroy()
		pub(ev_projectile_hit,
			EvProjectileHit{
				pos: p.Pos(), ammotype: p.ammotype, emitter: p.emitter,
				target: p.target,
			})
		println("deleting projectile, eid=", GetEntity(p).Id())
		//deleteEntity(GetEntity(p))
		deleteProjectile(GetEntity(p))
		//pp(2)
		return
	}

	/*
		if b.moveStartTime == 0 {
			b.moveStartTime = _now()
		}

			// Total time of travel A -> B [constant]
			tt := distancePos2(Vec2{b.sPos.X(), b.sPos.Y()},
				Vec2{b.dPos.X(), b.dPos.Y()}) / b.moveSpeed
			t := (_now() - b.moveStartTime) / tt
			nextPos := Lerp3(b.sPos, b.dPos, t)

			if roundPrec(t, 2) < 1.0 {
				b.SetPos(nextPos)
			} else {
				b.delete()
				b.View.destroy()
				pub(ev_bullet_hit, b)
				return
			}
	*/
	p.View.update(dt) // ???
}

func makeProjectile(sPos, dPos Vec3, speed,
	pathCurveness float64, ammotype *AmmoType, emitter *ecs.Entity,
	target *ecs.Entity) *ecs.Entity {

	en := newEntity()
	_ = newProjectile(en, sPos, dPos, speed, pathCurveness, ammotype,
		emitter, target)

	// Add to fx system
	game.getSystem("FxSys").addElem(en)
	return en
}

func deleteProjectile(en *ecs.Entity) {
	//game.getSystem("FxSys").removeElem(en)
	//p := en.Get(ProjectileT).(*Projectile)
	//p.delete()
	deleteEntity(en)
}

func makeProjectileView(en *ecs.Entity, p *Projectile) *ProjectileView {
	if p.ptype == "shell" || p.ptype == "rocket" {
		v := newProjectileView(en, p)
		return v
	} else {
		panic("unknown ptype: " + p.ptype)
	}
}
