package main

var _ = `

import (
  "testing"
)

func sep() {
  println("#")
}

func TestBuildingMgr(t *testing.T) {
  b1 := MakeBuilding("housing", CRedCamp).(*Building)
  b2 := MakeBuilding("factory", CRedCamp).(*Building)

  sep()
  
  // Test add
  bm := NewBuildingMgr()
  bm.AddBuilding(b1)
  bm.AddBuilding(b2)

  bm.ListBuildings()

  sep()

  // Test remove
  bm.RemoveBuilding(b2)

  bm.ListBuildings()

  sep()

  // Test double add
  bm.AddBuilding(b1)

  bm.ListBuildings()

  sep()
}
`
