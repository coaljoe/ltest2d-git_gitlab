package main

type ContainerItem interface {
	Show()
}

type ContainerI interface {
	AddItem(ContainerItem)
	ListItems()
}

type Container struct {
	items    []ContainerItem
	capacity int
}

func newContainer(capacity int) *Container {
	return &Container{
		capacity: capacity,
	}
}

func (c *Container) addItem(it ContainerItem) {
	c.items = append(c.items, it)
}

func (c *Container) removeItem(it interface{}) {
	println("Container.RemoveItem not implemented")
}

func (c *Container) listItems() {
	println("items:")
	for i, v := range c.items {
		println("-> ", i, v)
		v.Show()
	}
}

func (c *Container) hasItems() bool {
	if len(c.items) > 0 {
		return true
	} else {
		return false
	}
}
