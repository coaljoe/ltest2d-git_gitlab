// Модуль игровых подсистем.
//
package main

type GameSystemI interface {
	Name() string
	update(dt float64)
	addElem(el interface{})
	removeElem(el interface{})
	hasElem(el interface{}) bool
	getElems() []interface{}
	listElems()
}

// XXX rename to SubSystem?
type GameSystem struct {
	*System
}

func (s *GameSystem) Name() string { return s.name }

func newGameSystem(name, desc string, _s GameSystemI) *GameSystem {
	s := &GameSystem{
		System: newSystem(name, desc),
	}
	//s.Register()
	game.addSystem(name, _s)
	return s
}

func (s *GameSystem) Register() {
	//game.AddSystem(s)
}

/*
func (s *GameSystem) Update(dt float64) {
}
*/
