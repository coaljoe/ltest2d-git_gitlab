// Unit system.
// Особeнности:
// - static units не хранятся в этой системе(?)
package main

import (
	"lib/ecs"
	ps "lib/pubsub"
	. "rx/math"
)

type UnitSys struct {
	units map[*ecs.Entity]bool
}

func newUnitSys() *UnitSys {
	s := &UnitSys{
		units: make(map[*ecs.Entity]bool, 0),
	}
	// Events
	sub(ev_entity_destroy, s.onEntityDestroy)
	sub(ev_unit_spawn, s.onUnitSpawn)
	sub(ev_unit_destroy, s.onUnitDestroy)
	return s
}

func (s *UnitSys) onEntityDestroy(ev *ps.Event) {
	en := ev.Data.(*ecs.Entity)
	for u := range s.units {
		if u.Id() == en.Id() {
			// Fire ev_unit_destroy on this unit
			pub(ev_unit_destroy, u)
		}
	}
}

func (s *UnitSys) onUnitSpawn(ev *ps.Event) {
	u := ev.Data.(*ecs.Entity)
	s.addUnit(u)
}

func (s *UnitSys) onUnitDestroy(ev *ps.Event) {
	u := ev.Data.(*ecs.Entity)
	s.removeUnit(u)
	// Call destructors
	deleteEntity(u)
	// Nullify unit, fixme?
	u = nil
}

func (s *UnitSys) addUnit(u *ecs.Entity) {
	if s.hasUnit(u) {
		panic("cant add same unit twice")
	}
	s.units[u] = true
}

func (s *UnitSys) removeUnit(u *ecs.Entity) {
	if !s.hasUnit(u) {
		panic("cant remove unit, not found")
	}

	delete(s.units, u)
}

func (s *UnitSys) hasUnit(u *ecs.Entity) bool {
	_, ok := s.units[u]
	return ok
}

func (s *UnitSys) getUnits() []*ecs.Entity {
	r := make([]*ecs.Entity, 0)
	for u := range s.units {
		r = append(r, u)
	}
	return r
}

func (s *UnitSys) getUnitById(id int) *ecs.Entity {
	for u, _ := range s.units {
		if u.Id() == id {
			return u
		}
	}
	pp("no unit found; id:", id)
	return nil
}

func (s *UnitSys) isUnitVisibleForPlayer(u *ecs.Entity, pl *Player) bool {
	fow := pl.getFow()
	uc := c_Unit(u)
	cx, cy := uc.cpos().x, uc.cpos().y
	// The unit is in not visible cell
	if !fow.isCellVisible(cx, cy) {
		return false
	}
	return true
}

func (s *UnitSys) getVisibleUnitsForPlayer(pl *Player) []*ecs.Entity {
	r := make([]*ecs.Entity, 0)
	for u := range s.units {
		if s.isUnitVisibleForPlayer(u, pl) {
			r = append(r, u)
		}
	}
	return r
}

// Create and place unit on the field.
func (s *UnitSys) placeUnit(cx, cy int, utype string, player *Player) (bool, *ecs.Entity) {
	en := makeUnit(utype, player)
	//c_Unit(en).setPlayer(player)
	//c_Building(en).setCPos(h.cx, h.cy)
	pos := Vec3{0, 0, float64(ZLevel_Ground0)}
	p(pos)
	pos[0] = (float64(cx) * cell_size) + half_cell_size
	pos[1] = (float64(cy) * cell_size) + half_cell_size
	c_Unit(en).SetPos(pos)
	spawnEntity(en)

	// Add to the system
	//s.addElem(en)

	return true, en
}

// Get units for particular player.
func (s *UnitSys) getUnitsForPlayer(player *Player) []*ecs.Entity {
	//pp(player.name)
	r := make([]*ecs.Entity, 0)
	for u := range s.units {
		p("-->", c_Unit(u).getPlayer(), c_PlayerRecord(u).player,
			c_Unit(u).utype, player, c_Unit(u).getPlayer() == player, player.name)
		if c_PlayerRecord(u).player == player {
			//if c_Unit(u).getPlayer() == player {
			r = append(r, u)
		}
	}
	return r
}

func (s *UnitSys) update(dt float64) {
	// XXX don't use range() since
	// Us.units removes may occur in UpdateUnit
	/*
	  for i := 0; i < len(us.units); i++ {
	    u := us.units[i]
	*/

	// Seems no problem with maps
	for u := range s.units {

		// Entity was deleted?
		//if u == nil {
		//	continue
		//}

		updateEntity(u, dt)

		// Move?
		//uc := u.Get(UnitT).(*Unit)
		//v := uc.getView()
		//v.update(dt)
		//v.Render()
	}
}
