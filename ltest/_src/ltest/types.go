package main

//import . "rx/math"

type Renderable interface {
	Render()
}

type StartableI interface {
	start()
}

type PauseableI interface {
	pause()
	unpause()
}

/*
type NonRenderable interface {
	render() {}
}
*/

/*
type OptFloat {
    _v float64
    _set bool
}

func (of *OptFloat) set(*v float64) {
    if v != nil {
        of._v = v
        of._set = true
    } else {
        of._v = 0
        of._set = false
    }
}

func (of OptFloat) val() float64 {

}
*/
