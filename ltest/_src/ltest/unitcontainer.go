package main

/*
type Container interface {
  AddItem(interface{})
  ListItems()
}
*/
type UnitI interface {
	spawn()
	//Render()
	update(dt float64)
	getName() string
	show()
	getView() ViewI
	/*
	  // Obj //
	  Pos() Pos
	  SetPos(p Pos)
	  Rot() float64
	  SetRot(r float64)
	*/
}

type UnitContainer struct {
	items []UnitI
}

func newUnitContainer() *UnitContainer {
	return &UnitContainer{}
}

func (c *UnitContainer) addItem(u UnitI) {
	c.items = append(c.items, u)
}

func (c *UnitContainer) removeItem(it UnitI) {
	println("UnitContainer.RemoveItem not implemented")
}

func (c *UnitContainer) listItems() {
	println("items:")
	for i, v := range c.items {
		println("-> ", i, v.getName())
		v.show()
	}
}

func (c *UnitContainer) hasItems() bool {
	if len(c.items) > 0 {
		return true
	} else {
		return false
	}
}
