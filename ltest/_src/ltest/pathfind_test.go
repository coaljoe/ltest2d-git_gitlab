package main

import (
	//. "ltest"
	. "fmt"
	"testing"
	//"rx"
)

/*
func InitTest() {
  //m := NewMap(128, 128)
  win := rx.NewWindow()
  //win.Init(640, 480)
  win.Init(1024, 576)
  app := rx.NewApp(win)
  app.Init()
  g := NewGame()
  _ = g
}
*/

func TestPathfind(t *testing.T) {

	//InitGame()
	//InitAll()
	initTest()

	p := pathfind(CPos{0, 0}, CPos{1, 0})
	Println("Path:", p)

	p2 := pathfind(CPos{0, 0}, CPos{5, 0})
	Println("Path:", p2)

	p3 := pathfind(CPos{1, 1}, CPos{3, 3})
	Println("Path:", p3)

}
