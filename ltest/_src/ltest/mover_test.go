// +build !static

package main

import (
	"fmt"
	"testing"
)

func TestGroundMover(t *testing.T) {
	//var _ = `
	initTest()

	u1 := makeUnit("lighttank", getPlayer()).Get(UnitT).(*Unit)

	u1.moveTo(CPos{1, 1})
	//Println(u1.path)
	fmt.Println(".")

	for i := 0; i < 2; i++ {
		u1.update(0)
	}
	//`
}
