package main

import (
	"encoding/json"
	"lib/ecs"
)

// Health component [s](embeded type)[/s]
type Health struct {
	*ecs.Component
	health_, maxHealth int
}

func newHealth(en *ecs.Entity, maxHealth int) *Health {
	h := &Health{
		Component: ecs.NewComponent(),
		maxHealth: maxHealth,
		health_:   maxHealth,
	}
	en.AddComponent(h)
	return h
}

func (h Health) isAlive() bool {
	return h.health_ > 0
}

func (h *Health) setHealth(amt int) {
	if amt < 0 {
		amt = 0
	}
	h.health_ = amt
}

func (h *Health) health() int {
	return h.health_
}

//func (h *Health) Delete() {}
//func (h *Health) Update(dt float64) {}

//////////////////
// Serialization

func (h *Health) store() string {
	m := make(map[string]interface{}, 0)
	m["health"] = h.health_
	bytes, err := json.Marshal(m)
	if err != nil {
		panic(err)
	}
	return string(bytes)
}

func (h *Health) restore(s string) {
	var m map[string]interface{}
	json.Unmarshal([]byte(s), &m)
	h.health_ = int(m["health"].(float64))
}
