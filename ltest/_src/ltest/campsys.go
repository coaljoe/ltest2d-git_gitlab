package main

type CampSys struct {
	//camps map[CampId]*Camp
	camps map[int]*Camp
}

func newCampSys() *CampSys {
	s := &CampSys{
		//camps: make(map[CampId]*Camp),
		camps: make(map[int]*Camp),
	}
	//t.makecamps()
	return s
}

// Create camp for the player.
func (cs *CampSys) makeCamp(ctype CampId, player *Player) *Camp {
	_log.Inf("[CampSys] makeCamp;", ctype, player)
	var c *Camp
	c = newCamp(ctype)
	// Set links
	c.player = player
	player.camp = c
	//cs.camps[ctype] = c
	cs.camps[c._id] = c
	return c
}

func (cs CampSys) getDefaultCamp() *Camp {
	//pdump(cs.camps)
	for _, c := range cs.camps {
		if c.player.isHuman() {
			return c
		}
	}
	panic("get default camp error")
}

func (cs CampSys) getCampsByCampId(cId CampId) []*Camp {
	r := make([]*Camp, 0)
	for _, c := range cs.camps {
		if c.id == cId {
			r = append(r, c)
		}
	}
	return r
}

// A short cut.
func defaultCamp() *Camp {
	return game.campsys.getDefaultCamp()
}
