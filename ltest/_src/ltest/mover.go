package main

import (
	"fmt"
	"rx"

	. "rx/math"

	"github.com/looplab/fsm"
)

type Mover interface {
	moveTo(dst CPos)
	hasPath() bool
	getPath() Path
	//DropPath()
	truncatePathTo(newdst CPos)
	cancelPath()
	update(dt float64)
}

//////////////
// BaseMover

type BaseMover struct {
	path Path
	u    *Unit
	fsm  *fsm.FSM
	// Debug
	d_showPath bool
	d_enabled  *bool
}

func (bm *BaseMover) getPath() Path { return bm.path }
func (bm *BaseMover) state() string { return bm.fsm.Current() }

func newBaseMover(u *Unit) *BaseMover {
	bm := &BaseMover{
		path: make(Path, 0),
		u:    u,
	}
	bm.fsm = fsm.NewFSM("idle",
		fsm.Events{
			{Name: "premoving", Src: []string{"idle"}, Dst: "premoving"}, // Idle -> premoving
			{Name: "moving", Src: []string{"premoving"}, Dst: "moving"},  // Premoving -> moving
			{Name: "idle", Src: []string{"moving"}, Dst: "idle"},         // Moving -> idle
			//{Name: "moving", Src: []string{"idle"}, Dst: "moving"}, // Moving -> idle?
		},
		fsm.Callbacks{
			"enter_state": func(e *fsm.Event) { bm._enterState(e) },
		},
	)
	//t.fsm.Event("idle")
	//t.fsm.Event("moving")
	//t.fsm.Event("idle")
	if bm.fsm.Cannot("moving") || bm.fsm.Cannot("idle") {
		//pp("bad fsm")
	}

	// Debug
	bm.d_enabled = &vars.debugPathfinding
	bm.d_showPath = true

	return bm
}

func (bm *BaseMover) hasPath() bool {
	return len(bm.path) > 0
}

func (bm *BaseMover) truncatePathTo(newdst CPos) {
	if !bm.hasPath() {
		panic("cant truncate path: no path")
	}
	// Truncate path
	//newpath := make(Path, 0)
	lastNode := bm.path.lastNode()
	//newpath = append(newpath, NewNode(newD.x, newD.y))
	newpath := pathfind(CPos{lastNode.x, lastNode.y}, newdst)
	newpath = append(newpath, lastNode)
	bm.path = newpath
	//bm.u.dPos = nil
}

func (bm *BaseMover) cancelPath() {
	if bm.hasPath() {
		lastNode := bm.path.lastNode()
		bm.truncatePathTo(CPos{lastNode.x, lastNode.y})
	}
}

// Fsm switch
func (bm *BaseMover) _enterState(e *fsm.Event) {
	fmt.Printf("::FSM:: BaseMover from %s to %s [cur: %s]\n", e.Src, e.Dst, e.FSM.Current())

	u := bm.u

	switch e.Dst {
	case "idle": // Start
		u.speed = 0
		/*
			if e.Src == "moving" {
				pub(ev_unit_move, u)
				pp(2)
			}
		*/

	case "premoving": // Start
		// Rotating
		lastNode := bm.path.lastNode()
		nextPos := CPos{lastNode.x, lastNode.y}
		deg := degBetweenI(u.cpos().x, u.cpos().y, nextPos.x, nextPos.y)
		u.dir.rotTo(deg, u.maxRotSpeed)

	case "moving": // Start
		u.speed = u.maxSpeed

	}
}

func (bm *BaseMover) update(dt float64) {
	// Debug rendering
	if *bm.d_enabled && bm.hasPath() {
		// Render debug path
		//pp("derp")
		//rx.DrawBegin()
		//gl.Disable(gl.DEPTH_TEST)
		for _, n := range bm.path {
			//rx.DrawCube()
			px := (float64(n.x) * cell_size) + cell_size/2
			py := (float64(n.y) * cell_size) + cell_size/2
			rx.DebugDrawCube(Vec3{px, py, 0}, 1.0, Vec3{.4, .8, .2})
			if n.parent != nil {
				parentx := (float64(n.parent.x) * cell_size) + cell_size/2
				parenty := (float64(n.parent.y) * cell_size) + cell_size/2
				rx.DebugDrawLine(Vec3{px, py, .5}, Vec3{parentx, parenty, .5},
					3.0, Vec3{0.6, 1, .4})
			}
		}
		//gl.Enable(gl.DEPTH_TEST)
		//rx.DrawEnd()
	}
}

/////////////////
// Ground mover

type MovingType int

const (
	MovingType_Tracked MovingType = iota
	MovingType_Wheeled
)

type GroundMover struct {
	*BaseMover
	movingtype MovingType
}

func newGroundMover(u *Unit) *GroundMover {
	gm := &GroundMover{
		BaseMover:  newBaseMover(u),
		movingtype: MovingType_Tracked,
	}
	//t.fsm.
	return gm
}

/*
// Fsm switch
func (m *GroundMover) enterState(e *fsm.Event) {
  Printf("::FSM:: GroundMover from %s to %s [cur: %s]\n", e.Src, e.Dst, e.FSM.Current())
  uc := mb.Get(UnitT).(*Unit)

  switch e.dst {
  case "engage":
  // Start
    trg := mb.target
    tpos := trg.Pos2()
      uc.MoveTo(CPos{int(tpos.x) / cell_size, int(tpos.y) / cell_size})

  case "disengage":
  // Start
    tpos := mb.savedIdlePos
    p(tpos, uc.Pos2())
    //uc.DropPath()
    //uc.MoveTo(CPos{10, 10})
    //uc.TruncatePathTo(CPos{10, 10})
    //uc.TruncatePathTo(mb.savedIdlePos)
    //uc.TruncatePathTo(CPos{int(tpos.x) / cell_size, int(tpos.y) / cell_size})
    //uc.MoveTo(CPos{int(tpos.x) / cell_size, int(tpos.y) / cell_size})
    uc.MoveTo(mb.savedIdlePos)
  }
}
*/

func (m *GroundMover) moveTo(dst CPos) {
	println("ground mover moveto()")
	if dst.x < 0 || dst.y < 0 {
		println("move to negative value, ignored.", dst.x, dst.y)
		return
	}

	if m.state() == "idle" {
		m.path = pathfind(m.u.cpos(), dst)
	} else {
		// In transition, move from next node
		lastNode := m.path.lastNode()
		nextd := CPos{lastNode.x, lastNode.y}
		m.path = pathfind(nextd, dst)
	}
}

func (m *GroundMover) update(dt float64) {
	m.BaseMover.update(dt)
	//println("groundmover.update")
	if !m.hasPath() {
		return
	}

	u := m.u

	moveTo := func(x, y int) {
		println("::step to", x, y)
		pub(ev_unit_step, EvUnitStep{GetEntity(u), x, y})
		//p(u.cpos())
		//pp(3)
		// Don't rotate here
		/*
		   deg := DegBetweenI(u.CPos().x, u.CPos().y, x, y)
		   //u.SetDir(deg)
		   u.rot.Y = -(Mod(deg, 360))
		   //println("DEG", deg)
		   //u.rot = 0
		*/
		adj := cell_size / 2.0 // Center of cell
		//adj := (cell_size / 2.0) + rand.Float64()
		u.setDPos2(Vec2{float64(x)*cell_size + adj, float64(y)*cell_size + adj})
		/*
			max_range := cell_size / 2.0 / 2.0
			min_range := -max_range
			adjv := Vec2{(cell_size / 2.0) + random(min_range, max_range),
				(cell_size / 2.0) + random(min_range, max_range)}
			dPos := Vec2{cellsToUnits(x), cellsToUnits(y)}.Add(adjv)
			u.setDPos2(dPos)
		*/
		//u.stepTime = 0
	}

	// Handle path
	///fmt.Println(m.path)
	if len(m.path) > 0 {
		n := m.path[len(m.path)-1]
		switch m.state() {
		case "idle":
			fmt.Println("handled", n, len(m.path))
			moveTo(n.x, n.y)
			m.fsm.Event("premoving")
		case "moving":
			//p(n.x, u.CPos().x, n.y, u.CPos().y)
			if n.x == u.cpos().x && n.y == u.cpos().y { // Node reached
				//if !u.isMoving() {
				//if !u.hasDPos() {
				// Path.pop()
				a := m.path
				_, m.path = a[len(a)-1], a[:len(a)-1]
				if len(m.path) == 0 {
					// It was last node
					println("path handled.")
				}
				m.fsm.Event("idle")
			}
		}
	}

	// Move

	// Update states
	switch m.state() {
	case "premoving": // Update
		// Check
		if u.dir.locked() {
			//pp(u.rot.Y, -u.rot.Y, u.dRot)
			m.fsm.Event("moving")
			break
		}

	case "moving": // Update
		// Total time of travel A -> B [constant]
		//tt := (cell_size / u.speed) * 10
		//u.travelTime = tt
		//u.speed = u.maxSpeed
		_ = `
		// Check
		if m.stepTime == 0 {
			m.stepTime = game.timer.time
		}

		// Total time of travel A -> B [constant]
		tt := (cell_size / u.speed) * 10
		//sPos := Pos{u.x * cell_size, u.y * cell_size}
		sPos := u.Pos2()
		dPos := u.dPos

		step := (game.timer.time - m.stepTime) / tt
		nextPos := Pos{geomLerp(sPos.x, dPos.x, step),
			geomLerp(sPos.y, dPos.y, step)}
		//fmt.Println("step", fmt.Sprintf("%f", step))
		//fmt.Println(fmt.Sprintf("%f %f", game.timer.time, s.stepTime))
		if RoundPrec(step, 2) < 1.0 {
			u.SetPos2(nextPos)
		} else {
			u.SetPos2(u.dPos)
			/*
			   u.x = u.pos.x // Cell_size
			   u.y = u.pos.z // Cell_size
			*/
			//u.cpos = CPos{u.pos.x / cell_size, u.pos.y / cell_size}
			//return "idle"
			//m.IdleState()
		}
		`
	}
}

type NavyMover struct {
}

func NewNavyMover() *NavyMover {
	return &NavyMover{}
}

func (m *NavyMover) moveTo(d CPos) {
	println("navy mover moveto()")
}

func (m *NavyMover) update(dt float64) {
}

type AirMover struct {
}

func NewAirMover() *AirMover {
	return &AirMover{}
}

func (m *AirMover) moveTo(d CPos) {
	println("air mover moveto()")
}

func (m *AirMover) update(dt float64) {
}
