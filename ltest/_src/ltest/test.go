package main

import "fmt"

var _test_initialized = false

func initTest() {
	fmt.Println("-- init test begin --")
	if !_test_initialized {
		//gamedir := "/home/j/dev/misc/ltest/go"
		//os.Chdir(gamedir)
		//initAll()
		initApp()
		initBaseGame()

		// Init test game
		game.createNewTestGame()
		game.start()

		_test_initialized = true
	}
	fmt.Println("-- init test done --")
}
