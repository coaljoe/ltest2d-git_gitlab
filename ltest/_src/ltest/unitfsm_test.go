// +build !static

package main

var _ = `

import (
  "testing"
  . "fmt"
)

func TestUnitFSM(t *testing.T) {

  u1 := MakeUnit("lighttank", CRedCamp).(*Unit)
  u2 := MakeUnit("lighttank", CRedCamp).(*Unit)

  u1.SetTarget(u2)

  u1.fsm.IdleState()
  Println(u1.speed)

  u1.fsm.StepState()
  Println(u1.speed)

  u1.fsm.IdleState()
  Println(u1.speed)

  for i := 0; i < 2; i++ {
    u1.Update(0)
    u2.Update(0)
  }

}
`
