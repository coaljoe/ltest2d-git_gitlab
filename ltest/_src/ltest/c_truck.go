package main

//. "math"
//. "rx/math"
import "lib/ecs"

// Component
type Truck struct {
	*ecs.Component
	container *Resource
}

func newTruck(en *ecs.Entity, rType ResourceType) *Truck {
	t := &Truck{
		Component: ecs.NewComponent(),
		container: newResource(rType),
	}
	en.AddComponent(t)
	return t
}

func (t *Truck) put(n int) {
	// Show transfer animation
	t.container.put(n)
}

func (t *Truck) take(n int) {
	// Show transfer animation
	t.container.take(n)
}

func (t *Truck) delete() {
	p("Truck.Delete")
}

func (t *Truck) update(dt float64) {
	//p("Truck.Update")
	//mt := tc.GetComponentTag(&Turret{}, "main").(*Turret)
}
