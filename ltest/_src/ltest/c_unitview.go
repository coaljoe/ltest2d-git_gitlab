package main

import (
	"lib/ecs"
	"rx"
	//. "rx/math"
)

// Component
type UnitView struct {
	*ecs.Component
	*View
	m *Unit
	rootNodeName,
	snd_moving,
	snd_idle string
}

func newUnitView(en *ecs.Entity, u *Unit) *UnitView {
	v := &UnitView{
		Component: ecs.NewComponent(),
		View:         newView(en),
		m:            u,
		rootNodeName: "body",
	}
	en.AddComponent(v)

	v.nodeName = "body"
	return v
}

func (v *UnitView) load() {
	if v.loaded {
		return
	}
	println("UnitView.load()")
	ast := GetEntity(v).Get(&ViewAsset{}).(*ViewAsset).getAsset()
	v.node = ast.getNode(v.nodeName)

	// Tweak texture filters for entire model
	for _, node := range ast.nodes {
		tex := node.Mesh.Material().Texture()
		if tex != nil {
			tex.SetOpts(
				// Set nearest+mip filter
				rx.TextureOptions{
					MinFilter: rx.TextureFilterNearest,
					MagFilter: rx.TextureFilterNearest,
					//MinFilter: rx.TextureFilterNearest,
					//MinFilter: rx.TextureFilterLinear,
					//MagFilter: rx.TextureFilterLinear,
					//Nomipmaps: true})
					Nomipmaps: false})
			/*
				// Check
				if tex.MinFilter() != rx.TextureFilterNearest ||
					tex.MagFilter() != rx.TextureFilterNearest {
					pp("bad filter; node name:", node.Name(),
						"minFilter:", int(tex.MinFilter()), "magFilter:", int(tex.MagFilter()))
				}
			*/
			//if node.Name() != "body" {
			//	pp("set for:", node.Name())
			//}
		}
	}
	v.loaded = true
}

func (v *UnitView) spawn() {
	println("UnitView.spawn()")
	if !v.loaded {
		v.load()
	}
	//v.node.spawn()
	rx.Rxi().Scene().Add(v.node)
	v.spawned = true
}

func (v *UnitView) delete() {
	println("UnitView.delete()")
	rx.Rxi().Scene().RemoveMeshNode(v.node)
	v.spawned = false
}

func (v *UnitView) render() {
	//println("UnitView.render()")
}

func (v *UnitView) update(dt float64) {
	//println("UnitView.update()")

	// Update units's position
	newPos := v.m.Pos()
	//curPos := v.node.Pos()
	//v.node.SetPos(Vec3{newPos.X(), newPos.Y(), curPos.Z()})
	v.node.SetPos(newPos)
	//v.node.SetPos(pos)

	// Update units's rotation
	//v.node.SetRot(Vec3{0, v.m.rot, 0})
	v.node.SetRot(v.m.Rot())
}
