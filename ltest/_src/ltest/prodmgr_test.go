package main

var _ = `

import (
  "testing"
)

func TestProdMgr(t *testing.T) {

  camp := NewCamp(CRedCamp)
  camp.money.Set(1000)

  q := NewProdQueue(camp)
  q.CanProduce("dumptruck")
  q.AddProd("dumptruck")
  q.AddProd("supplytruck")
  q.List()

  pm := NewProdMgr()
  pm.AddProdQueue(q)

  f := MakeBuilding("factory", camp.id).(*Building)
  f.Show()
  f.Produce(q.ProduceLast().typename, q.camp.id)
  f.Produce(q.ProduceLast().typename, q.camp.id)
  q.List()
  camp.money.Show()

  if !q.IsEmpty() {
    t.Errorf("queue is not empty")
  }
}
`
