package main

import (
	"lib/ecs"
	"rx"
)

// Component
type BuildingView struct {
	*ecs.Component
	*View
	m          *Building
	snd_moving string
	snd_idle   string
}

func newBuildingView(en *ecs.Entity, m *Building) *BuildingView {
	v := &BuildingView{
		Component: ecs.NewComponent(),
		View: newView(en),
		m:    m,
	}
	en.AddComponent(v)

	v.nodeName = "body"
	return v
}

func (v *BuildingView) load() {
	if v.loaded {
		return
	}
	println("BuildingView.load()")
	ast := GetEntity(v).Get(&ViewAsset{}).(*ViewAsset).getAsset()
	v.node = ast.getNode(v.nodeName)
	v.loaded = true
}

func (v *BuildingView) spawn() {
	//pp(2)
	println("buildingview.spawn()")
	if !v.loaded {
		v.load()
	}
	//v.node.spawn()
	rx.Rxi().Scene().Add(v.node)
	v.spawned = true
}

func (v *BuildingView) delete() {
	p("BuildingView.delete()")
	rx.Rxi().Scene().RemoveMeshNode(v.node)
	v.spawned = false
}

func (v *BuildingView) render() {
	//println("buildingview.render()")
	//rx.Rxi().Scene().RemoveMeshNode(v.node)
	//v.spawned = false
}

func (v *BuildingView) update(dt float64) {
	//println("buildingview.update()")
	//pp(2)

	// Update buildings's position
	newPos := v.m.Pos()
	v.node.SetPos(newPos)
	//v.node.SetRot(v.m.Rot())
}
