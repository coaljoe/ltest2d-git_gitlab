package main

// Building system
type BuildingMgr struct {
	buildings []*Building
}

func newBuildingMgr() *BuildingMgr {
	return &BuildingMgr{}
}

func (bm *BuildingMgr) addBuilding(b *Building) bool {
	if bm._findBuildingIdx(b) > -1 {
		println("err: the building was already added")
		return false
	}
	bm.buildings = append(bm.buildings, b)
	return true
}

func (bm *BuildingMgr) removeBuilding(b *Building) bool {
	if i := bm._findBuildingIdx(b); i > -1 {
		bm.buildings = append(bm.buildings[:i], bm.buildings[i+1:]...)
		return true
	} else {
		println("err: building not found")
		return false
	}
}

func (bm *BuildingMgr) listBuildings() {
	println("BuildingMgr.ListBuildings")
	for i, b := range bm.buildings {
		println("-> ", i, "name:", b.name)
	}
}

func (bm *BuildingMgr) update(dt float64) {

}

func (bm *BuildingMgr) _findBuildingIdx(b *Building) int {
	for i, b1 := range bm.buildings {
		if b1 == b {
			return i
		}
	}
	return -1
}
