package main

import (
	"lib/ecs"
	"math"
	. "rx/math"
)

// For all units.
// Component
type Unit struct {
	*ecs.Component
	*Obj
	Mover         // MoverI
	*Combat       // Link
	*PlayerRecord // Link
	*Position     // Link
	//fsm *UnitFSM
	// AI
	//*UnitAI
	// State
	*Health // Link?
	// Props
	id           int
	name         string
	utype        string
	weight       float64
	maxSpeed     float64
	accSpeed     float64
	maxRotSpeed  float64
	realm        RealmType
	unitCont     *Container
	itemCont     *Container
	fuel         *Resource
	explotionBhv func()
	static       bool
	spawned      bool
	View         ViewI // Fixme
}

func (u *Unit) getView() ViewI      { return u.View }
func (u *Unit) getName() string     { return u.name }
func (u *Unit) getRealm() RealmType { return u.realm }

func newUnit(en *ecs.Entity) *Unit {
	u := &Unit{
		Component: ecs.NewComponent(),
		Obj: newObj(en, "unit obj"),
		id:  _ider.GetNextId("unit"),
		//speed:        0.0,
		realm:        RealmType_Ground,
		explotionBhv: standardExplotion, // Default
		fuel:         newResource(ResourceType_Fuel),
		//Health:       newHealth(100),
		//dir:          NewDir(),
		maxRotSpeed: 100.0,
	}
	en.AddComponent(u)
	u.Health = newHealth(en, 100)
	u.Combat = newCombat(en)
	u.PlayerRecord = newPlayerRecord(en)
	u.Position = newPosition(en)
	u.Mover = newGroundMover(u) // Default
	//u.UnitAI = NewUnitAI(en)
	// No need for reference of unitai in unit
	//_ = NewUnitAI(en)
	//u.fsm = NewUnitFSM(u)

	// Tune
	//u.Position.mtype = PositionMovingType_Lerp
	return u
}

func (u *Unit) convertToStaticUnit() {
	//u.Position = nil // Static units still have direction
	u.Mover = nil
	u.fuel = nil
	u.static = true
}

func (u *Unit) isStaticUnit() bool {
	return u.static
}

// For military score calculation.
func (u *Unit) getUnitMilitaryScore() float64 {
	/*
		_, maxAr := c_Combat(u).getAttackRatingRange()
		//unitPower := maxAr
		unitPower := math.Pow(float64(maxAr), 1.5)
	*/

	// Sum of all units' weapons power (attackRating^1.5)
	// So the unit with few poverful weapons will get bigger score
	// Because it can attack from both weapons(?)
	unitPower := 0.0
	for _, w := range u.Combat.getWeapons() {
		unitPower += math.Pow(float64(w.attackRating()), 1.5)
	}

	// Navy units are twice less powerful in military score
	// (like in original method from a civ game)
	// XXX: what about air units?
	if u.realm == RealmType_Navy {
		unitPower /= 2.0
	}

	// XXX: exclude static units?

	return unitPower
}

// Get view range in cells.
func (u *Unit) getViewRangeCells() int {
	minViewRange := 2
	locateRange := int(u.Combat.getMaxLocateRadius() / cell_size)
	//p("locateRange:", locateRange)
	return maxi(minViewRange, locateRange)
}

/*
func (u *Unit) Destroy() {
  println("unit.destroy")
  // Explode
  u.explotionBhv()
}*/

/* View */
/*
func (u *Unit) Render() {
  u.View.Render()
}
*/

// Fixme: rename to OnSpawn?
// XXX cannot be called directly, use _entity.spawn?
func (u *Unit) spawn() {
	if u.spawned {
		return
	}
	u.spawned = true // Must be set before SpawnEntity

	println("Unit.Spawn; id = ", u.id, u.spawned)
	// XXX only sending an event from here
	pub(ev_unit_spawn, GetEntity(u))
	//SpawnEntityBut(GetEntity(u), u.Component)
	spawnEntity(GetEntity(u))

	//u.View.Spawn()
}

/*
func (u *Unit) destroy() {
	println("Unit.Destroy")
	pub(ev_unit_destroy, GetEntity(u))
	//u.Unit.Destroy()
	u.View.delete()
}
*/

func (u *Unit) delete() {
	println("Unit.Delete")
	//u.Combat = nil
	//u.PlayerRecord = nil
	//uc.Health = nil // Fixme,  use proper delete
	//uc.UnitAI = nil

	// Currently doing the same in the system (ev_unit_destroy)
	//game.unitsys.removeUnit(GetEntity(u))
}

func (u *Unit) cpos() CPos {
	adj := cell_size / 2.0
	return CPos{
		int((u.PosX() - adj) / cell_size),
		int((u.PosY() - adj) / cell_size)}
}

// Place unit at center of pos x, y.
func (u *Unit) setCPos(x, y int) {
	adj := cell_size / 2
	u.setPos2(Vec2{float64(x*cell_size + adj), float64(y*cell_size + adj)})
}

func (u *Unit) show() {
	println("Unit.Show")
	println(" Name:", u.name)
}

func (u *Unit) update(dt float64) {
	//println("unit.update")
	//u.fsm.Update(dt)
	if u.Mover != nil {
		u.Mover.update(dt)
	}
	//u.MovingBhv.Update(dt)
}

/* strategy pattern test */

//type ExplotionBhv interface {
//  Explode()
//}

func standardExplotion() {
	println("ExplotionBhv: standard boom")
}

func toxicExplotion() {
	println("ExplotionBhv: toxic boom")
}
