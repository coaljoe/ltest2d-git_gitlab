package main

import "testing"

func TestHealth(t *testing.T) {
	ent := newEntity()
	h := newHealth(ent, 100)
	if h.health() != 100 {
		println("fail")
	}

	h.setHealth(50)
	if h.health() != 50 {
		println("fail")
	}

	v := h.health()
	if v != 50 {
		println("fail")
	}

	// Test serialization
	s := h.store()
	p(s)

	ent2 := newEntity()
	h2 := newHealth(ent2, 100)
	h2.restore(s)

	if h2.health() != 50 {
		t.Fail()
	}

	if GetEntity(h).Id() != GetEntity(h2).Id() {
		t.Log("Entity ids don't match")
		t.Fail()
	}

}

var _ = `
func TestHealth(t *testing.T) {
	ent := ecs.NewEntity()
	h := NewHealth(ent, 100)
	if h.health != 100 {
		println("fail")
	}

	h.SetHealth(50)
	if h.health != 50 {
		println("fail")
	}

	v := h.GetHealth()
	if v != 50 {
		println("fail")
	}
}

type IntCmp struct {
	*Component
	v int
}

// Define a new method to Health
func (h *Health) TestIntCmp() {
	println("test intcmp")

	// Get 'external' component
	c := h.Entity.Get(&IntCmp{}).(*IntCmp)

	// Test component's value
	if c.v != 1 {
		println("error")
	} else {
		println("test ok")
	}
}

func TestHealthECS(t *testing.T) {
	ent := ecs.NewEntity()
	h := NewHealth(ent, 100)

	intcmp := &IntCmp{v: 1}
	ent.AddComponent(intcmp)
	h.TestIntCmp()
}
`
