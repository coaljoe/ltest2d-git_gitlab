package main

import (
	"fmt"
	. "rx/math"

	"sort"
)

type ObjSys struct {
	*GameSystem
	//objs map[int]*Obj
}

func newObjSys() *ObjSys {
	s := &ObjSys{}
	s.GameSystem = newGameSystem("ObjSys", "Object system", s)
	return s
}

// XXX not used
/*
func (s *ObjSys) addObj(o *Obj) {
	if s.hasElem(o) {
		panic("already have obj; id: " + Itoa(o.id))
	}
	s.addElem(o)
}

func (s *ObjSys) removeObj(o *Obj) {
	s.removeElem(o)
}

func (s *ObjSys) hasObj(o *Obj) bool {
	return s.hasElem(o)
}
*/

func (s *ObjSys) getObjsInRadius(c Vec2, r float64) Objs {
	ret := make(Objs, 0)
	for _, oi := range s.getElems() {
		o := oi.(*Obj)
		p := o.pos2()
		if inRadius(p.X(), p.Y(), r, c.X(), c.Y()) {
			ret = append(ret, o)
		}
	}
	return ret
}

func (s *ObjSys) getObjsInRadiusSortByDist(c Vec2, r float64, reverse bool) Objs {
	objs := s.getObjsInRadius(c, r)
	// Sort objs by distance
	sort.Slice(objs, func(i, j int) bool {
		d1 := distancePos2(objs[i].pos2(), c)
		d2 := distancePos2(objs[j].pos2(), c)
		if reverse {
			return d1 > d2 // Reverse
		} else {
			return d1 < d2
		}
	})
	return objs
}

func (s *ObjSys) getObjById(id int) *Obj {
	for _, el := range s.getElems() {
		obj := el.(*Obj)
		if obj.id == id {
			return obj
		}
	}
	//return nil
	panic(fmt.Sprintf("object not found; id=%d", id))
}

func (s *ObjSys) update(dt float64) {
	// Process all objects here
}
