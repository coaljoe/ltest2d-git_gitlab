package main

import (
	"fmt"
	. "math"
	"strings"
)

type ResourceType int

const (
	ResourceType_Ammo ResourceType = iota
	ResourceType_Fuel
	ResourceType_Pop //?
	ResourceType_Money
)

type Resource struct {
	typ   ResourceType
	amt   int
	limit int
}

func newResource(typ ResourceType) *Resource {
	return &Resource{
		typ:   typ,
		limit: MaxInt32,
	}
}

func (r *Resource) set(n int) {
	r.amt = mini(n, r.limit)
	/*
		r.limit &&>0..limit	)
	*/
}

func (r *Resource) take(n int) int {
	r.amt -= n
	neg := 0
	if r.amt < 0 {
		neg = r.amt
		r.amt = 0
	}
	return n + neg
}

func (r *Resource) takeFrom(lhs *Resource) {
	v := lhs.take(lhs.amt)
	r.put(v)
}

func (r *Resource) put(n int) {
	r.amt = mini(r.amt+n, r.limit)
}

func (r *Resource) takeByUnits(n int, unitWeight float64) int {
	amt := Ceil(float64(n) * unitWeight)
	return r.take(int(amt))
	//return int(r.amt)
}

func (r *Resource) isEmpty() bool {
	if r.amt > 0 {
		return false
	} else {
		return true
	}
}

func resourceTypeFromString(s string) ResourceType {
	s_lc := strings.ToLower(s)
	switch s_lc {
	case "ammo":
		return ResourceType_Ammo
	case "fuel":
		return ResourceType_Fuel
	case "money":
		return ResourceType_Money
	case "population":
		return ResourceType_Pop
	default:
		panic("unknown resourceType; s: " + s_lc)
	}
}

func (r *Resource) name() string {
	switch r.typ {
	case ResourceType_Ammo:
		return "Ammo"
	case ResourceType_Fuel:
		return "Fuel"
	case ResourceType_Money:
		return "Money"
	case ResourceType_Pop:
		return "Population"
	default:
		panic(fmt.Sprintf("unknown resource type=%v", r.typ))
	}
}

func (r *Resource) show() {
	println("Resource.Show\n name:", r.name(), "\n amt:", r.amt)
}
