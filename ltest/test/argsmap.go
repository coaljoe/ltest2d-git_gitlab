package main

import "log"

type ArgsMap map[string]interface{}

func bar(am ArgsMap) {
	if v, ok := am["foo"].(string); ok { // type assertion
		log.Println("bar", v)
	} else {
		log.Println("bar no foo")
	}
}

// Or

type Args struct {
	foo     string
	boo     int
	a, b, c float32
}

func bar2(a Args) {
	if a.foo != "" {
		log.Println("bar2", a.foo)
	} else {
		log.Println("bar2 no foo")
	}
}

func main() {
	bar(ArgsMap{"foo": "map", "blah": 10})
	bar(ArgsMap{})

	bar2(Args{foo: "struct"})
	bar2(Args{})
}
