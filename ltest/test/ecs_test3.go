package main

import (
	"reflect"
)

// components
type Chimney struct {
	*Component
	smokeColor string
}

func (c *Chimney) Render() {
	println("chimney smoking")
}

type Windows struct {
	*Component
	color string
}

func (c *Windows) Render() {
	println("windows blinking")
}

type Health struct {
	*Component
	health int
}

func NewHealth(en *Entity) *Health {
	return &Health{
		Component: NewComponent(en),
		health:    100,
	}
}

type Building struct {
	*Component
	name string
}

func (b *Building) Render() {
	println(b.name, "render")
	// need access to other components

	//wc := b.Entity.components[1].(*Windows)
	wc := b.Entity.GetComponent(&Windows{}).(*Windows)
	if wc != nil {
		println("has windows of", wc.color, "color")
	}

	cc := b.Entity.GetComponent(&Chimney{})
	if cc != nil {
		cc_ := cc.(*Chimney) // fixme
		println("has chimney with", cc_.smokeColor, "smoke")
	}

	hc := b.Entity.GetComponent(&Health{}).(*Health)
	if hc != nil {
		println("and", hc.health, "health.")
	}
}

func (b *Building) Destroy() {
	println("destroy", b.name)
}

// entities
type Housing struct {
	*Entity
	*Building
	*Windows
	*Health
}

type Factory struct {
	*Entity
	*Building
	*Windows
	*Chimney
	*Health
}

func main() {
	ent1 := NewEntity()
	b1 := &Housing{
		Entity:   ent1,
		Building: &Building{Component: NewComponent(ent1), name: "Housing"},
		Windows:  &Windows{color: "blue"},
		Health:   NewHealth(ent1),
	}

	// add components
	b1.AddComponent(b1.Building)
	b1.AddComponent(b1.Windows)
	b1.AddComponent(b1.Health)

	b1.Building.Render()

	ent2 := NewEntity()
	b2 := &Factory{
		Entity:   ent2,
		Building: &Building{Component: NewComponent(ent2), name: "Factory"},
		Windows:  &Windows{color: "green"},
		Chimney:  &Chimney{smokeColor: "black"},
		Health:   NewHealth(ent2),
	}

	// add components
	/*b2.components = append(b2.Components, b2.Building)
	  b2.components = append(b2.Components, b2.Windows)
	  b2.components = append(b2.Components, b2.Chimney)
	  b2.components = append(b2.Components, b2.Health)
	*/
	b2.AddComponent(b2.Building)
	b2.AddComponent(b2.Windows)
	b2.AddComponent(b2.Chimney)
	b2.AddComponent(b2.Health)

	b2.Building.Render()

	b1.Destroy()
	b2.Destroy()

	b2.Chimney.Render()
	b2.Windows.Render()

	// b1.Chimney.Render() // no chimney, fine error
	b1.Windows.Render()
}

// ecs
type Component struct {
	*Entity // backlink to entity
}

func NewComponent(en *Entity) *Component {
	return &Component{
		Entity: en,
	}
}

type Entity struct {
	components []interface{}
}

func NewEntity() *Entity {
	return &Entity{}
}

func (en *Entity) AddComponent(cmp interface{}) {
	en.components = append(en.components, cmp)
}

func (en *Entity) GetComponent(cmp interface{}) interface{} {
	for _, cmp1 := range en.components {
		if reflect.TypeOf(cmp1) == reflect.TypeOf(cmp) {
			return cmp1
		}
	}
	println("*component not found")
	return nil
}
