package main

import "fmt"

type Z struct {
	x uint
}

func NewZ() Z {
	return Z{x: 2}
}

func changeZ(z *Z) {
	z.x -= 1
}

func main() {
	z := NewZ()
	changeZ(&z)
	fmt.Println("Z:", z)
	changeZ(&z)
	fmt.Println("Z:", z)
	/*
		                                                                                                   changeZ(z);
		fmt.Println("Z:", z);
	*/
}
