package gui_base

import (
	//"fmt"

	xg "kristallos.ga/xgui"

	//"kristallos.ga/ltest2d/gui"
)

// Base gui interface

// Like GuiI

type GuiI interface {
	//Init()

	//?
	// GuiGameSheetI ?
	GameSheet() GuiGameSheetI
	// GuiLsSheetI ?
	LsSheet() GuiLsSheetI

	Xgi() *xg.XGui
	CellX() int
	SetCellX(v int)
	CellY() int
	SetCellY(v int)

	HasMouseFocus() bool
	Enabled() bool
	SetEnabled(v bool)

	// ?
	//Render()
	Draw()
	Update(dt float64)
}

// XXX ?

/*
func NewGui() *Gui {
	fmt.Println("NewGui()")

	gui := gui.NewGui()
	
	return gui
}
*/
