package gui

import (
	"fmt"

	ps "kristallos.ga/lib/pubsub"
	xg "kristallos.ga/xgui"
	//"github.com/veandco/go-sdl2/gfx"
	"github.com/veandco/go-sdl2/sdl" //. "math"
	//. "kristallos.ga/rx/math"
	"strings"

	"kristallos.ga/ltest2d/gui/gui_base"

	"kristallos.ga/ltest2d/main_module"
)

type GuiGameSheet struct {
	*xg.Sheet
	topTextFontSize int
	topText          *xg.Label
	debugText        *xg.Label
	debugInfo        *xg.Label
	sidePanel        *xg.Panel
	minimapP         *xg.Panel
	minimapCreated   bool
	minimapMouseHold bool
	// Main buttons block
	//controlButtons [3][3]*xg.Button
	controlMenu *GuiControlMenu
	// Resource labels
	// font size ?
	rlTextFontSize    int
	rlNormalColor     xg.Color
	rlRedColor        xg.Color
	rlControlMenuMode bool // Display mode: control button
	rlMoneyIcon       *xg.ImageBox
	rlMoney           *xg.Label
	rlMoneyRed        bool
	rlPopIcon         *xg.ImageBox
	rlPop             *xg.Label
	rlPopRed          bool
	rlFuelIcon        *xg.ImageBox
	rlFuel            *xg.Label
	rlFuelRed         bool
	rlMetalIcon       *xg.ImageBox
	rlMetal           *xg.Label
	rlMetalRed        bool
}
//func (s *GuiGameSheet) ControlMenu() *GuiControlMenu
func (s *GuiGameSheet) ControlMenu() gui_base.GuiControlMenuI {
	return s.controlMenu
}

func TestCallback2(pl interface{}) {
	fmt.Println("test callback2")
	panic(2)
}

func TestCallback3(pl interface{}) {
	fmt.Println("test callback3")
	//pp(2)
}

func newGuiGameSheet(g *Gui) *GuiGameSheet {
	s := &GuiGameSheet{
		Sheet: xg.NewSheet("GameSheet"),
		topTextFontSize: 12,
		rlTextFontSize: 14,
		//rlNormalColor: xg.ColorBlack,
		//rlRedColor:    xg.ColorRed,
		rlNormalColor: xg.Color{190, 195, 195},
		rlRedColor:    xg.Color{230, 50, 50},
	}
	g.xgi.SheetSys.AddSheet(s)
	//g.xgi.SetEnabled(g.enabled)

	s.topText = xg.NewLabel(xg.Pos{2, 0}, "topText")
	//g.sheet.AddWidget(g.topText)
	/*
		st := s.topText.Style()
		//st.TextColor = xg.ColorGreen
		st.TextColor = xg.ColorWhite
		st.TextFont = xg.DebugFont
		s.topText.SetStyle(st)
	*/
	s.topText.Style.TextColor = xg.ColorWhite
	s.topText.Style.TextFontName = "debug_font"
	s.topText.Style.TextFontSize = s.topTextFontSize
	s.Root().AddChild(s.topText)

	//s.debugText = xg.NewLabel(xg.Pos{0, 0.97}, "debugText")
	s.debugText = xg.NewLabel(xg.Pos{0, 20}, "debugText")
	s.debugText.Style.TextFontSize = s.topTextFontSize
	s.debugText.SetVisible(false)
	s.debugText.SetText("")
	//s.debugText
	s.Root().AddChild(s.debugText)

	// XXX
	// ?
	fontSize := s.topText.Style.TextFontSize

	//ysh := s.topText.Style.TextFont().LineHeight() * 2
	ysh := s.topText.Style.TextFont().LineHeight(fontSize) * 2
	s.debugInfo = xg.NewLabel(xg.Pos{10, ysh}, "debugInfo")
	s.debugInfo.Style.TextColor = xg.ColorWhite
	s.debugInfo.Style.TextFontName = "debug_font"
	s.debugInfo.Style.TextFontSize = s.topTextFontSize
	s.Root().AddChild(s.debugInfo)

	if !main_module.GetVarsDev() {
		s.debugInfo.SetText("")
		s.topText.SetText("")
	}

	/*
		t1 := xg.NewLabel(xg.Pos{.5, .25}, "TestText with callback")
		s.Root().AddChild(t1)
		t1.SetCallback(TestCallback2)
	*/

	/*
		// Panel test
		//p := rg.NewPanel(rg.Rect{0, 0, 100, 100})
		//p := rg.NewPanel(rg.Rect{0, 0, 10, 10})
		//p := rg.NewPanel(rg.Rect{0, 0, .2, .2})
		//p := xg.NewPanel(xg.Rect{.5, .5, .5, .5})
		// Native res
		nx, ny := 1920, 1080
		_ = ny
		v := 1600
		k := float64(v) / float64(nx)
		//pp(k)
		x := int(float64(vars.resX) * k)
		//pp(x)
		//pp(float64(x) * getPw())
		//p := xg.NewPanel(xg.Rect{1600 * getPw(), 1.0, 320 * getPw(), 1.0})
		v2 := 320
		k2 := float64(v2) / float64(nx)
		w := int(float64(vars.resX) * k2)
		_ = w
		_ = x
		//p := xg.NewPanel(xg.Rect{float64(x) * getPw(), 0.0, 320 * getPw(), 1.0})
		//p := xg.NewPanel(xg.Rect{float64(x) * getPw(), 0.0, float64(w) * getPw(), 1.0})
		//p := xg.NewPanel(xg.Rect{.0, .0, .1, .1})
		//p := xg.NewPanel(xg.Rect{.0, .0, .5, .5})
		//p := xg.NewPanel(xg.Rect{.25, .25, .25, .25})
		//p := xg.NewPanel(xg.Rect{.5, .5, .5, .5})
		kx := float64(vars.resX) / float64(nx)
		ky := float64(vars.resY) / float64(ny)
		//pw := getPw() * 320
			pw := getPw() * 320 * kx
			ph := getPh() * 1080 * ky
	*/
	//p := xg.NewPanel(xg.Rect{1.0 - pw + (1 * getPw()), 0.0, pw, ph})
	//xpw, xph := 240, 1080
	xpw, xph := 220, 1080
	p := xg.NewPanel(xg.Rect{main_module.Xvars.ResX() - xpw, 0, xpw, xph})
	p.SetName("right panel")
	p.Label.SetText("")
	//p.SetColor(&xg.ColorBlue)
	//mySheet.AddWidget(p)
	//g.mainSheet.Root().AddChild(p)
	p.BgImage.Load("res/gui/images/right_panel_bg.png")
	p.ScaleBgImage = true
	s.Root().AddChild(p)

	s.sidePanel = p

	s.controlMenu = newGuiControlMenu(s)

	// Resource labels
	{

		/*
		font := xg.NewFont()
		font.Load("res/gui/fonts/Arimo-Bold.ttf", 14)
		//xg.XGui.Style.Fonts["rl_font_bold"] = font
		g.xgi.Style.Fonts["rl_font_bold"] = font
		*/
		//font := xg.AddFont("rl_font_bold", "res/gui/fonts/Arimo-Bold.ttf", 14)
		font := xg.AddFont("rl_font_bold", "res/gui/fonts/Arimo-Bold.ttf")
		textFontSize := s.rlTextFontSize
		textColor := xg.Color{10, 10, 10}
		textColor = xg.Color{215, 215, 215}
		iconYsh := -3
		labelXsh := 25

		// Money

		s.rlMoneyIcon = xg.NewImageBox(xg.Rect{20, 300 + iconYsh, 25, 25})
		s.rlMoneyIcon.SetName("rlMoneyIcon")
		s.rlMoneyIcon.SetImage("res/gui/images/icons/money.png")
		//s.rlMoneyIcon.ScaleImage = true
		s.sidePanel.AddChild(s.rlMoneyIcon)

		//s.rlMoney = xg.NewLabel(xg.Pos{80, 300}, "5000")
		//s.rlMoney = xg.NewLabel(xg.Pos{45, 300}, "5000")
		//s.rlMoney = xg.NewLabel(xg.Pos{45, 300}, "9999")
		s.rlMoney = xg.NewLabel(xg.Pos{20 + labelXsh, 300}, "9999")
		s.rlMoney.SetName("rlMoney")
		s.rlMoney.Style.SetTextFont(font)
		s.rlMoney.Style.TextFontSize = textFontSize
		s.rlMoney.Style.TextColor = textColor
		s.sidePanel.AddChild(s.rlMoney)

		/*
		   s.rlPopBar = xg.NewLabel(xg.Pos{130, 300}, "웃웃웃웃")
		   s.rlPopBar.SetName("rlPopBar")
		   s.rlPopBar.Style.TextFont = font
		   s.rlPopBar.Style.TextColor = xg.Color{10, 10, 10}
		   s.sidePanel.AddChild(s.rlPopBar)
		*/

		// Pop

		s.rlPopIcon = xg.NewImageBox(xg.Rect{130, 300 + iconYsh, 25, 25})
		s.rlPopIcon.SetName("rlPopIcon")
		s.rlPopIcon.SetImage("res/gui/images/icons/pop.png")
		//s.rlPopBar.value = 50
		s.sidePanel.AddChild(s.rlPopIcon)

		//s.rlPop = xg.NewLabel(xg.Pos{190, 300}, "9999")
		s.rlPop = xg.NewLabel(xg.Pos{130 + labelXsh, 300}, "9999")
		s.rlPop.SetName("rlPop")
		s.rlPop.Style.SetTextFont(font)
		s.rlPop.Style.TextFontSize = textFontSize
		s.rlPop.Style.TextColor = textColor
		s.sidePanel.AddChild(s.rlPop)

		// Fuel

		s.rlFuelIcon = xg.NewImageBox(xg.Rect{20, 320 + iconYsh, 25, 25})
		s.rlFuelIcon.SetName("rlFuelIcon")
		s.rlFuelIcon.SetImage("res/gui/images/icons/fuel.png")
		//s.rlFuelIcon.ScaleImage = true
		s.sidePanel.AddChild(s.rlFuelIcon)

		s.rlFuel = xg.NewLabel(xg.Pos{20 + labelXsh, 320}, "9999")
		s.rlFuel.SetName("rlFuel")
		s.rlFuel.Style.SetTextFont(font)
		s.rlFuel.Style.TextFontSize = textFontSize
		s.rlFuel.Style.TextColor = textColor
		s.sidePanel.AddChild(s.rlFuel)

		// Metal

		s.rlMetalIcon = xg.NewImageBox(xg.Rect{130, 320 + iconYsh, 25, 25})
		s.rlMetalIcon.SetName("rlMetalIcon")
		//s.rlMetalIcon.SetImage("res/gui/images/icons/brick.png")
		s.rlMetalIcon.SetImage("res/gui/images/icons/metal.png")
		//s.rlMetalBar.value = 50
		s.sidePanel.AddChild(s.rlMetalIcon)

		s.rlMetal = xg.NewLabel(xg.Pos{130 + labelXsh, 320}, "9999")
		s.rlMetal.SetName("rlMetal")
		s.rlMetal.Style.SetTextFont(font)
		s.rlMetal.Style.TextFontSize = textFontSize
		s.rlMetal.Style.TextColor = textColor
		s.sidePanel.AddChild(s.rlMetal)
	}

	main_module.SubS("ev_building_place", s.onBuildingPlace)
	main_module.SubS("ev_unit_step", s.onUnitStep)

	return s
}

func (s *GuiGameSheet) onMouseButton(ev *ps.Event) {
	//game := main_module.GetGame()

	evData := ev.Data.(*xg.EvMouseButtonData)
	if evData.Pressed {
		if evData.ActiveWidget == s.minimapP {
			//pp(2)
			//s.minimapMouseHold = true
			/*
				sx := float64(game.gui.xgi.Context.InputSys.MX) / float64(vars.resX)
				sy := float64(game.gui.xgi.Context.InputSys.MY) / float64(vars.resY)
				s.minimapFollowMouse(sx, sy)
			*/
			mx := gui.xgi.Context.InputSys.MX
			my := gui.xgi.Context.InputSys.MY
			s.minimapFollowMouse(mx, my)
		}
	}
}

func (s *GuiGameSheet) onMouseMove(ev *ps.Event) {
	fmt.Println("ev:", ev)
	evData := ev.Data.(*xg.XEvMouseMoveData)
	if evData.ActiveWidget == s.minimapP {
		var _ = `
		//pp(2)
		kx := s.minimapP.BgImageScaleKx
		ky := s.minimapP.BgImageScaleKy
		_ = kx
		_ = ky

		absRect := s.minimapP.GetAbsoluteRect()
		//rectMx := evData.M.SX - absRect[0]
		rectMx := evData.M.X - absRect[0]
		//rectMxW := rectMx / absRect[2] // 0..1
		rectMxW := float64(rectMx) / float64(absRect[2]) // 0..1
		//rectMy := evData.M.SY - absRect[1]
		rectMy := evData.M.Y - absRect[1]
		//rectMxH := rectMy / absRect[3] // 0..1
		rectMxH := float64(rectMy) / float64(absRect[3]) // 0..1
		vpX := float64(game.field.w) * main_module.Xcell_size * rectMxW
		vpY := float64(game.field.h) * main_module.Xcell_size * rectMxH
		vpWidthPx := game.vp.W()
		vpHeightPx := game.vp.H()
		vpX -= float64(vpWidthPx / 2)
		vpY -= float64(vpHeightPx / 2)

		sdl.PumpEvents()

		//state := sdl.GetKeyboardState()
		_, _, state := sdl.GetMouseState()
		if (state & sdl.BUTTON_LEFT) > 0 {
			//pp(2)
			game.vp.setPos(vpX, vpY)
		}

		p("absRect:", absRect, evData.M, rectMx, rectMxW)
		`
		//s.minimapFollowMouse(evData.M.SX, evData.M.SY)
		s.minimapFollowMouse(evData.M.X, evData.M.Y)
	}
}

func (s *GuiGameSheet) onBuildingPlace(ev *main_module.EventS) {
	// Rsedraw minimap
	//s.updateMinimapData()
	s.updateMinimapDataFow()
}

func (s *GuiGameSheet) onUnitStep(ev *main_module.EventS) {
	// Redraw minimap
	//pp(9)
	//d := ev.Data.([]interface{})
	d := ev.Data
	x := d[1].(int)
	y := d[2].(int)
	oldX := d[3].(int)
	oldY := d[4].(int)
	//pp(x, y, oldX, oldY)
	s.updateMinimapDataRect(x, y, 1, 1)
	s.updateMinimapDataRect(oldX, oldY, 1, 1)
	//s.updateMinimapData()

	s.updateMinimapDataFow()
}

//func (s *GuiGameSheet) minimapFollowMouse(sx, sy float64) {
func (s *GuiGameSheet) minimapFollowMouse(mx, my int) {
	//game := main_module.GetGame()
	vp := main_module.GetVP()

	field := main_module.GetField() 

	// XXX1
	//var _ = `
	//pp(2)
	kx := s.minimapP.BgImageScaleKx
	ky := s.minimapP.BgImageScaleKy
	_ = kx
	_ = ky

	absRect := s.minimapP.GetAbsoluteRect()
	rectMx := mx - absRect[0]
	//rectMxW := rectMx / absRect[2] // 0..1
	rectMxW := float64(rectMx) / float64(absRect[2]) // 0..1
	rectMy := my - absRect[1]
	//rectMxH := rectMy / absRect[3] // 0..1
	rectMxH := float64(rectMy) / float64(absRect[3]) // 0..1
	vpX := float64(field.W()) * main_module.Xcell_size * rectMxW
	vpY := float64(field.H()) * main_module.Xcell_size * rectMxH
	//vpWidthPx := game.vp.w
	//vpWidthPx := int(float64(game.vp.w - xg.UnitsToPxW(s.sidePanel.Rect()[2])))
	vpWidthPx := int(float64(vp.W() - s.sidePanel.Rect()[2]))
	vpHeightPx := vp.H()
	vpX -= float64(vpWidthPx / 2)
	vpY -= float64(vpHeightPx / 2)

	sdl.PumpEvents()

	//state := sdl.GetKeyboardState()
	_, _, state := sdl.GetMouseState()
	if (state & sdl.BUTTON_LEFT) > 0 {
		//pp(2)
		vp.SetPos(vpX, vpY)
	}

	fmt.Println("absRect:", absRect, mx, my, rectMx, rectMxW)
	//`
}

func (s *GuiGameSheet) createMinimap() {
	//game := main_module.GetGame()
	field := main_module.GetField()


	// XXX1
	//var _ = `
	//minimapP := xg.NewPanel(xg.Rect{0.0, 0.0, 0.5, 0.5})
	//minimapP := xg.NewPanel(xg.Rect{0.0, 0.0, 1.0, 0.5})
	//padding := 0.05
	padding := 16
	//aspect := float64(main_module.Xvars.ResX()) / float64(vars.resY)
	aspect := main_module.Xvars.ResAspect()
	_ = padding
	_ = aspect
	var minimapP *xg.Panel
	//minimapP = xg.NewPanel(xg.Rect{padding, padding / aspect, 1.0 - padding*2, 0.3 - (padding * 2 / aspect)})
	//minimapP = xg.NewPanel(xg.Rect{100, 100, 200, 200})
	//minimapP = xg.NewPanel(xg.Rect{padding, padding, 210, 210})
	var w, h int
	if field.Aspect() == 1.0 {
		w = 190 - 1
		h = 190 - 1
	} else {
		w = 190 - 1
		h = int(190.0/field.Aspect()) - 1
	}
	//pp("Z", w, h, game.field.aspect)

	minimapP = xg.NewPanel(xg.Rect{padding, padding, w, h})
	minimapP.SetName("minimapP")
	//minimapP.Label().SetText("1")  // FIXME?
	minimapP.Label.SetText("")
	//minimapP.BgImage.Create(100, 100)
	//minimapP.BgImage.PutPixel(50, 50, 0, 255, 0, true)
	minimapP.BgImage.Create(field.W(), field.H())
	minimapP.ScaleBgImage = true

	if field.Aspect() != 1.0 {
		// Center minimap
		rect := minimapP.Rect()
		rect[1] += (w / 2) - (h / 2)
		minimapP.SetRect(rect)
	}

	minimapP.SetCallback(TestCallback3, nil)
	xg.Sub(xg.Ev_mouse_button, s.onMouseButton)
	xg.Sub(xg.Ev_mouse_move, s.onMouseMove)
	s.sidePanel.AddChild(minimapP)
	//s.Root().AddChild(minimapP)
	s.minimapP = minimapP

	s.minimapCreated = true
	//`
}

func (s *GuiGameSheet) UpdateMinimapData() {
	f := main_module.GetField()
	s.updateMinimapDataRect(0, 0, f.W(), f.H())
}

func (s *GuiGameSheet) updateMinimapDataRect(x_, y_, w, h int) {
	game := main_module.GetGame()
	fowsys := game.GetSystem("FowSys").(*main_module.FowSys)

	if !s.minimapCreated {
		s.createMinimap()
	}
	f := main_module.GetField()
	for y := y_; y < y_+h; y++ {
		for x := x_; x < x_+w; x++ {
			//cell := f.cells[x][y]
			cell := f.GetCell(x, y)
			if cell.Z().IsSea() {
				//s.minimapP.BgImage.PutPixel(x, y, 0, 0, 255-(int(-cell.z)*32), false)
				//s.minimapP.BgImage.PutPixel(x, y, 0, 0, 255-(int(-cell.z)*48), false)
				//v := maxi(255-(int(-cell.z)*64), 0)
				if true {
					v := max(255-(int(-cell.Z())*48), 0)
					s.minimapP.BgImage.PutPixel(x, y, v/5, v/4, int(float64(v)*0.8), false)
				}
				if false {
					//v := maxi(255-(int(-cell.z)*12), 0)
					//v := maxi(255-(int(-cell.z)*16), 0)
					v := max(255-(int(-cell.Z())*64), 0)
					_ = v
					p("v:", v)
					s.minimapP.BgImage.PutPixel(x, y, 84/2-(v/2), 90/2-(v/2), 167/2-(v/2), false)
					//s.minimapP.BgImage.PutPixel(x, y, 65+v, 105+v, 225+v, false)
					//s.minimapP.BgImage.PutPixel(x, y, v, v, int(float64(v)*0.8), false)
				}
			} else {
				// Green
				//s.minimapP.BgImage.PutPixel(x, y, 0, 128+int(cell.z)*32, 0, false)
				// Snow
				//v := 128 + int(cell.z)*32
				//v := (16*4 + 128) + int(cell.z)*16
				v := 210 - int(cell.Z())*28
				s.minimapP.BgImage.PutPixel(x, y, v, v, v, false)
			}

			if true {
				if cell.HasBuilding() {
					b := cell.GetBuilding()
					if b.GetPlayer() != nil {
						campColor := b.GetPlayer().Camp().Id.Color()
						s.minimapP.BgImage.PutPixel(x, y,
							campColor.R(), campColor.G(), campColor.B(), false)
					}
				}

				if cell.HasUnit() {
					u := cell.GetUnit()
					campColor := u.GetPlayer().Camp().Id.Color()
					s.minimapP.BgImage.PutPixel(x, y,
						campColor.R(), campColor.G(), campColor.B(), false)
				}
			}

			if true {
				if main_module.HasPlayer() {
					fow := main_module.GetPlayer().GetFow()

					if !fow.IsCellVisible(cell.Cx(), cell.Cy()) {
						if !fowsys.DisableFowDraw() {
							s.minimapP.BgImage.PutPixel(x, y, 0, 0, 0, false)
						}
					}
				}
			}
		}
	}
	s.minimapP.BgImage.Build()
}

func (s *GuiGameSheet) updateMinimapDataFow() {
	// XXX
	if !main_module.HasPlayer() {
		return
	}

	fow := main_module.GetPlayer().GetFow()
	if !fow.Dirty() {
		return
	}

	for _, pos := range fow.DirtyTilePoss() {
		s.updateMinimapDataRect(pos.X(), pos.Y(), 1, 1) // XXX slow?
	}

	fow.MarkAsClean()
}

// Update and render minimap
func (s *GuiGameSheet) updateMinimap(dt float64) {
	//game := main_module.GetGame()
	field := main_module.GetField()
	renderer := main_module.GetRenderer()

	// XXX1
	//var _ = `
	// Draw viewport frame
	//vp := game.vp
	vp := main_module.GetVP()
	mmX := s.minimapP.GetAbsoluteRect()[0]
	//mmXPx := xg.UnitsToPxW(mmX)
	mmXPx := mmX
	mmY := s.minimapP.GetAbsoluteRect()[1]
	//mmYPx := xg.UnitsToPxH(mmY)
	mmYPx := mmY
	_ = mmXPx
	_ = mmYPx
	kx := s.minimapP.BgImageScaleKx
	ky := s.minimapP.BgImageScaleKy
	//kx = 1
	//ky = 1
	//pp(kx, ky)
	vpCellPosX := int(vp.X() * kx / main_module.Xcell_size)
	vpCellPosY := int(vp.Y() * ky / main_module.Xcell_size)
	//vpWidthPx := int(float64(vp.w) * kx / cell_size)
	vpWidthPxFull := int(float64(vp.W()) * kx / main_module.Xcell_size)
	//vpWidthPx := int(float64(vp.w-xg.UnitsToPxW(s.sidePanel.Rect()[2])) * kx / cell_size)
	vpWidthPx := int(float64(vp.W()-s.sidePanel.Rect()[2]) * kx / main_module.Xcell_size)
	vpHeightPx := int(float64(vp.H()) * ky / main_module.Xcell_size)
	vpWidthNoScale := int(float64(vp.W()) / main_module.Xcell_size)
	_ = vpWidthNoScale
	vpWidthFullScaleK := float64(vpWidthPx) / float64(vpWidthPxFull)
	_ = vpWidthFullScaleK
	/*
		vpTopLeftPointX := mmXPx + vpCellPosX
		vpTopLeftPointY := mmYPx + vpCellPosY
		vpTopRightPointX := vpTopLeftPointX + vpWidthPx
		vpTopRightPointY := vpTopLeftPointY
		vpBottomLeftPointX := vpTopLeftPointX
		vpBottomLeftPointY := vpTopLeftPointY + vpHeightPx
		vpBottomRightPointX := vpTopRightPointX
		vpBottomRightPointY := vpTopRightPointY + vpHeightPx
	*/
	//vpMaxCellPosX := int(float64(game.field.w) * kx)
	//vpMaxCellPosX := 10

	vpTopLeftPointX := vpCellPosX
	vpTopLeftPointY := vpCellPosY
	/*
		vpTopRightPointX := pmod(vpTopLeftPointX+vpWidthPx, vpMaxCellPosX)
		vpTopRightPointY := vpTopLeftPointY

			vpBottomLeftPointX := vpTopLeftPointX
			vpBottomLeftPointY := vpTopLeftPointY + vpHeightPx
			vpBottomRightPointX := pmod(vpTopRightPointX, vpMaxCellPosX)
			vpBottomRightPointY := vpTopRightPointY + vpHeightPx
	*/

	/*
		if vp.overflowX > 0 {
			pp(2)
		}

		if vp.underflowX > 0 {
			pp(3)
		}
	*/

	/*
		//sdl.Do(func() {
		//renderer.Clear()
		//renderer.DrawLine(int32(mmXPx), int32(mmYPx), int32(mmXPx)+20, int32(mmYPx)+20)
		err := renderer.SetDrawColor(0, 255, 0, sdl.ALPHA_OPAQUE)
		if err != nil {
			panic(err)
		}
			//err = renderer.DrawLine(int32(mmXPx), int32(mmYPx), int32(mmXPx)+20, int32(mmYPx)+20)
			err = renderer.DrawLine(int32(0), int32(0), int32(1000), int32(1000))
			if err != nil {
				panic(err)
			}
			//pp(2)
	*/

	/*
		// Draw top line
		err = renderer.DrawLine(int32(vpTopLeftPointX), int32(vpTopLeftPointY),
			int32(vpTopRightPointX), int32(vpTopRightPointY))
		if err != nil {
			panic(err)
		}
		// Draw bottom line
		err = renderer.DrawLine(int32(vpBottomLeftPointX), int32(vpBottomLeftPointY),
			int32(vpBottomRightPointX), int32(vpBottomRightPointY))
		if err != nil {
			panic(err)
		}
		// Draw left line
		err = renderer.DrawLine(int32(vpTopLeftPointX), int32(vpTopLeftPointY),
			int32(vpBottomLeftPointX), int32(vpBottomLeftPointY))
		if err != nil {
			panic(err)
		}
		// Draw right line
		err = renderer.DrawLine(int32(vpTopRightPointX), int32(vpTopRightPointY),
			int32(vpBottomRightPointX), int32(vpBottomRightPointY))
		if err != nil {
			panic(err)
		}
	*/

	/*
		//lineWidth := int32(2)
		lineWidth := int32(1)
		//lineColor := sdl.Color{0, 255, 0, sdl.ALPHA_OPAQUE}
		//lineColor := sdl.Color{255, 255, 255, 64}
		lineColor := sdl.Color{255, 255, 255, 255}

		// Draw top line
		//if !gfx.ThickLineColor(renderer, int32(vpTopLeftPointX),
			int32(vpTopLeftPointY), int32(vpTopRightPointX), int32(vpTopRightPointY),
		if !gfx.ThickLineColor(renderer,
			int32(vpTopLeftPointX), int32(vpTopLeftPointY),
			int32(vpTopRightPointX), int32(vpTopRightPointY),
			lineWidth, lineColor) {
			panic("error")
		}
		// Draw bottom line
		if !gfx.ThickLineColor(renderer, int32(vpBottomLeftPointX),
			int32(vpBottomLeftPointY), int32(vpBottomRightPointX), int32(vpBottomRightPointY),
			lineWidth, lineColor) {
			panic("error")
		}
		// Draw left line
		if !gfx.ThickLineColor(renderer, int32(vpTopLeftPointX),
			int32(vpTopLeftPointY), int32(vpBottomLeftPointX), int32(vpBottomLeftPointY),
			lineWidth, lineColor) {
			panic("error")
		}
		// Draw right line
		if !gfx.ThickLineColor(renderer, int32(vpTopRightPointX),
			int32(vpTopRightPointY), int32(vpBottomRightPointX), int32(vpBottomRightPointY),
			lineWidth, lineColor) {
			panic("error")
		}
	*/

	lineWidth := 1
	//lineColor := sdl.Color{0, 255, 0, sdl.ALPHA_OPAQUE}
	//lineColor := sdl.Color{255, 255, 255, 64}
	lineColor := sdl.Color{255, 255, 255, 255}

	clip := false
	if vp.OverflowX() > 0 || vp.OverflowY() > 0 {
		//absRect := s.minimapP.GetAbsoluteRect()
		//clipRect := sdl.Rect{int32(mmXPx), int32(mmYPx),
		//	int32(xg.UnitsToPxW(absRect[2])), int32(xg.UnitsToPxH(absRect[3]))}
		clipRect := sdl.Rect{int32(mmXPx), int32(mmYPx),
			int32(float64(field.W()) * kx), int32(float64(field.H()) * ky)}
		renderer.SetClipRect(&clipRect)
		clip = true
	}

	// A | B
	// --+--
	// C | D

	main_module.DrawRectangleColorWidth(mmXPx+vpTopLeftPointX, mmYPx+vpTopLeftPointY, vpWidthPx, vpHeightPx,
		lineColor, lineWidth)

	if main_module.GetVarsDev() {
		// Draw secondary rects with different color
		lineColor = sdl.Color{0, 255, 0, sdl.ALPHA_OPAQUE}
	}

	if vp.OverflowX() > 0 {
		//vpWScaleK := float64(vpWidthPx) / float64(vpWidthPxFull)
		//pp(vpWScaleK, vpWidthPxFull, vpWidthPx)
		//overflowAmt := vp.overflowX / cell_size
		overflowAmt := int(float64(vp.OverflowX())*kx) / main_module.Xcell_size
		//overflowAmt := int((float64(vp.overflowX)*vpWScaleK)*kx) / cell_size
		//overflowAmt := int((float64(vp.overflowX)/vpWidthFullScaleK)*kx) / cell_size
		//drawRectangleColorWidth(mmXPx-vpWidthPx+overflowAmt, mmYPx+vpTopLeftPointY,
		//	vpWidthPx, vpHeightPx,lineColor, lineWidth)
		//drawRectangleColorWidth(mmXPx-vpWidthPx+overflowAmt, mmYPx+vpTopLeftPointY,
		//	int(float64(vpWidthPx)*vpWidthFullScaleK), vpHeightPx, lineColor, lineWidth)
		//drawRectangleColorWidth(mmXPx-vpWidthPx+overflowAmt, mmYPx+vpTopLeftPointY,
		// vpWidthPx, vpHeightPx, lineColor, lineWidth)
		main_module.DrawRectangleColorWidth(mmXPx-vpWidthPxFull+overflowAmt, mmYPx+vpTopLeftPointY,
			vpWidthPx, vpHeightPx, lineColor, lineWidth)
		//drawRectangleColorWidth(mmXPx-vpWidthPx+int(float64(overflowAmt)*kx), mmYPx+vpTopLeftPointY,
		//	vpWidthPx, vpHeightPx, lineColor, lineWidth)
	}
	if vp.OverflowY() > 0 {
		//overflowAmt := vp.overflowY / cell_size
		overflowAmt := int(float64(vp.OverflowY())*ky) / main_module.Xcell_size
		if overflowAmt > 0 && overflowAmt < field.H() {
			//drawRectangleColorWidth(mmXPx, mmYPx-vpTopLeftPointY-(vpHeightPx+overFlowAmt),
			//	vpWidthPx, vpHeightPx, lineColor, lineWidth)
			main_module.DrawRectangleColorWidth(mmXPx+vpTopLeftPointX, mmYPx-(vpHeightPx-overflowAmt),
				vpWidthPx, vpHeightPx, lineColor, lineWidth)
			//pp(2)
		}
	}
	// Draw fouth segment, top left (D)
	if vp.OverflowY() > 0 && vp.OverflowX() > 0 {
		//overflowAmtX := vp.overflowX / cell_size
		//overflowAmtY := vp.overflowY / cell_size
		overflowAmtX := int(float64(vp.OverflowX())*kx) / main_module.Xcell_size
		//overflowAmtX := int(float64(vp.overflowX)*vpWidthFullScaleK*kx) / cell_size
		overflowAmtY := int(float64(vp.OverflowY())*ky) / main_module.Xcell_size
		//drawRectangleColorWidth(mmXPx-vpWidthPx+overflowAmtX, mmYPx-(vpHeightPx-overflowAmtY),
		//	vpWidthPx, vpHeightPx, lineColor, lineWidth)
		main_module.DrawRectangleColorWidth(mmXPx-vpWidthPxFull+overflowAmtX, mmYPx-(vpHeightPx-overflowAmtY),
			vpWidthPx, vpHeightPx, lineColor, lineWidth)
	}

	if clip {
		renderer.SetClipRect(nil)
	}

	//})
	//renderer.Present()
	//p("ZZZ", game.frame)
	//pp(mmX, mmXPx)
	//pp(2)
	//`
}

func (s *GuiGameSheet) Render() {
	//_log.Dbg("%F", game.frame)
	// Call base
	s.Sheet.Render()

	s.updateMinimap(0)

	/*
		//println("MYSHEET RENDER")
		//r := rxi.Renderer()

		r.RenderQuad(0.5, 0.0, 0.2, 0.1, 0)
		r.Set2DMode()
		rx.DrawLine(0, 0, 0, .5, .5, 0)
		//rx.DrawLine(0, 0, 0, 1, 1, 0)
		//rx.DrawLine(0, 0, 0, 100, 100, 0)
		r.Unset2DMode()
	*/
}

func (s *GuiGameSheet) updateRLs(dt float64) {
	// Control is in Control Menu
	if s.rlControlMenuMode {
		return
	}

	p := main_module.GetPlayer()

	money := p.Camp().Money.Amt
	moneyLowValue := 5000
	pop := p.Camp().Pop.Amt
	popLowValue := 20
	fuel := p.Camp().Fuel.Amt
	fuelLowValue := 1000
	metal := p.Camp().Metal.Amt
	metalLowValue := 1000

	color := s.rlNormalColor
	if s.rlMoneyRed || money < moneyLowValue {
		color = s.rlRedColor
	}
	s.rlMoney.Style.TextColor = color
	s.rlMoney.SetText(fmt.Sprintf("%d", money))

	color = s.rlNormalColor
	if s.rlPopRed || pop < popLowValue {
		color = s.rlRedColor
	}

	s.rlPop.Style.TextColor = color
	s.rlPop.SetText(fmt.Sprintf("%d", pop))

	color = s.rlNormalColor
	if s.rlFuelRed || fuel < fuelLowValue {
		color = s.rlRedColor
	}

	s.rlFuel.Style.TextColor = color
	s.rlFuel.SetText(fmt.Sprintf("%d", fuel))

	color = s.rlNormalColor
	if s.rlMetalRed || metal < metalLowValue {
		color = s.rlRedColor
	}

	s.rlMetal.Style.TextColor = color
	s.rlMetal.SetText(fmt.Sprintf("%d", metal))
}

func (s *GuiGameSheet) Update(dt float64) {
	//game := main_module.GetGame()
	field := main_module.GetField()
	vp := main_module.GetVP()

	//_log.Dbg("%F")
	//pp(2)
	// Call base
	s.Sheet.Update(dt)

	//s.updateMinimap(dt)

	s.controlMenu.update(dt)

	s.updateRLs(dt)

	var extraDebugInfo string

	var cellInfo string
	p(gui.cellX, gui.cellY)
	if gui.cellX >= 0 && gui.cellY >= 0 &&
		gui.cellX < field.W() && gui.cellY < field.H() {
		cell := field.GetCell(gui.cellX, gui.cellY)
		if cell != nil {
			cellInfo = fmt.Sprintf("z: %s, tileId: %d, hmOrig: %d, open: %v, walkable: %v", cell.Z(), cell.TileId(),
				field.GetHm().DataOrig()[gui.cellX][gui.cellY],
				//cell.open,
				cell.Open(),
				cell.Walkable())

			if cell.IsShore() {
				extraDebugInfo += "shore: yes\n"
			}
		}
	}
	//g.topText.SetText(fmt.Sprintf("cellX: %d cellY: %d", g.cellX, g.cellY))
	/*
		s.topText.SetText(fmt.Sprintf("cellX: %d cellY: %d fps: %d cellInfo: (%s)",
			game.gui.cellX, game.gui.cellY, vars.fps, cellInfo))
	*/

	if main_module.GetVarsDev() {

		vars := main_module.GetVars()

		dStr := fmt.Sprintf("cellX: %d cellY: %d fps: %d cellInfo: (%s) vpCellPos: %d,%d",
			gui.cellX, gui.cellY, vars.Fps(), cellInfo, int(vp.X()/main_module.Xcell_size), int(vp.Y()/main_module.Xcell_size))
		if len(extraDebugInfo) > 0 {
			dStr += "\n" + strings.TrimSpace(extraDebugInfo)
		}
		
		s.topText.SetText(dStr)

		if len(main_module.GetDebug().DebugText()) > 0 {
			ds := "Debug text:\n"
			ds += "> " + main_module.GetDebug().DebugText() + "\n"
			s.debugInfo.SetText(ds)
		}

		cell := field.GetCell(gui.cellX, gui.cellY)
		_ = cell
		//s.debugText.SetText(fmt.Sprintf("overflowX: %d \n overflowY: %d", game.vp.overflowX, game.vp.overflowY))
		//s.debugText.SetText(fmt.Sprintf("buildingId: %d | buildingMask: %b", cell.buildingId, cell.buildingMask))
		//s.debugText.SetText(fmt.Sprintf("shore: %#v", cell.shore))
		//s.debugText.SetText(fmt.Sprintf("fowTileId: %d", cell.fowTileId))
		fow := main_module.GetPlayer().GetFow()
		fowTileId := fow.TileIds()[gui.cellX][gui.cellY]
		s.debugText.SetText(fmt.Sprintf("fowTileId: %d", fowTileId))
	} else {
		//s.topText.SetText(fmt.Sprintf("fps: %d", vars.fps))
	}
}
