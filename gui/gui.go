package gui

import (
        "fmt"

	xg "kristallos.ga/xgui"
	sb "kristallos.ga/xgui/backend/sdl"

	"kristallos.ga/ltest2d/gui/gui_base"

	"kristallos.ga/ltest2d/main_module"
	//"kristallos.ga/ltest2d"
)

// XXX global link ?
var (
	gui *Gui
)

type Gui struct {
	xgi *xg.XGui
	//sheet *rg.Sheet
	cellX int
	cellY int
	//topText   *rg.Label
	//debugText *rg.Label
	gameSheet *GuiGameSheet
	nrgSheet  *GuiNewRandomGameSheet
	lsSheet   *GuiLoadingScreenSheet
	enabled   bool
}
func  (g *Gui) Xgi() *xg.XGui { return g.xgi }
func  (g *Gui) CellX() int { return g.cellX }
func  (g *Gui) SetCellX(v int) { g.cellX = v }
func  (g *Gui) CellY() int { return g.cellY }
func  (g *Gui) SetCellY(v int) { g.cellY = v }
//func  (g *Gui) GameSheet() *GuiGameSheet { return g.gameSheet }
func  (g *Gui) GameSheet() gui_base.GuiGameSheetI { return g.gameSheet }
func  (g *Gui) LsSheet() gui_base.GuiLsSheetI { return g.lsSheet }
func  (g *Gui) Enabled() bool { return g.enabled }
func  (g *Gui) SetEnabled(v bool) { g.enabled = v }

func NewGui() *Gui {
        fmt.Println("NewGui")

	sb := sb.NewSdlBackend()
	fmt.Println("sb:", sb)
	renderer := main_module.GetRenderer()
	fmt.Println("renderer:", renderer)
	//renderer := ltest2d.GetRenderer()
	sb.Init(renderer)

	g := &Gui{
		xgi: xg.NewXGui(sb, nil),
		//sheet:   rg.NewSheet("GUI"),
		//gameSheet: newGuigameSheet(),
		//enabled: false,
		enabled: true,
	}
	g.xgi.SetEnabled(g.enabled)

	g.lsSheet = newLoadingScreenSheet(g)
	//g.lsSheet.SetVisible(false)

	if true {
		g.gameSheet = newGuiGameSheet(g)
		/*
			g.rgi.SheetSys.AddSheet(g.sheet)
			g.rgi.SetEnabled(g.enabled)

			g.topText = rg.NewLabel(rg.Pos{0, 0}, "topText")
			//g.sheet.AddWidget(g.topText)
			g.sheet.Root().AddChild(g.topText)

			g.debugText = rg.NewLabel(rg.Pos{0, 0.97}, "debugText")
			g.debugText.SetVisible(false)
			g.sheet.Root().AddChild(g.debugText)
		*/
		/*
			x := rg.NewRxGui()
			x.SetEnabled(true)
			g := &Gui{rgi: x}
		*/

		var _ = `
		if false {
			// Panel test
			//p := rg.NewPanel(rg.Rect{0, 0, 100, 100})
			//p := rg.NewPanel(rg.Rect{0, 0, 10, 10})
			//p := rg.NewPanel(rg.Rect{0, 0, .2, .2})
			//p := xg.NewPanel(xg.Rect{.5, .5, .5, .5})
			p := xg.NewPanel(xg.Rect{.5, .5, .33, .33})
			p.SetName("blue panel")
			p.SetColor(&xg.ColorBlue)
			//mySheet.AddWidget(p)
			g.gameSheet.Root().AddChild(p)

			// Button test
			//b := xg.NewButton(xg.Rect{.5, .5, .1, .1})
			b := xg.NewButton(xg.Rect{.5, .5, .25, .25})
			b.Label().SetText("Test button")
			//res\units\ground\gastruck\reds\model
			//pp(renderer)
			b.Panel.BgImage.Load("res/units/ground/gastruck/reds/model/uvturret.png")
			g.gameSheet.Root().AddChild(b)
		}
		`
	}

	if true {
		g.nrgSheet = newGuiNewRandomGameSheet(g)
		g.xgi.SheetSys.SetActiveSheet(g.nrgSheet)
		//g.nrgSheet.Show()
	}

	// XXX
	// Set global ?
	gui = g

	return g
}

func (g *Gui) HasMouseFocus() bool {
	//return true
	return g.xgi.HasMouseFocus()
}

func (g *Gui) showDebugText(s string) {
	/*
		g.debugText.SetText(s)
		g.debugText.SetVisible(true)
	*/
	g.gameSheet.debugText.SetText(s)
	g.gameSheet.debugText.SetVisible(true)
}

func (g *Gui) hideDebugText() {
	/*
		g.debugText.SetText("")
		g.debugText.SetVisible(false)
	*/
	g.gameSheet.debugText.SetText("")
	g.gameSheet.debugText.SetVisible(false)
}

func (g *Gui) Draw() {
	if !g.enabled {
		return
	}
	g.xgi.Render()
	//g.sheet.Render(r)
	//g.gameSheet.Render(r)
}

func (g *Gui) Update(dt float64) {
	if !g.enabled {
		return
	}

	//g.render(rx.Rxi().Renderer())
	//g.render(_rxi.Renderer())

	/*
		//g.topText.SetText(fmt.Sprintf("cellX: %d cellY: %d", g.cellX, g.cellY))
		g.topText.SetText(fmt.Sprintf("cellX: %d cellY: %d fps: %d",
			g.cellX, g.cellY, _rxi.App.Fps()))
	*/
	g.xgi.Update(dt)
	//g.gameSheet.Update(dt)
}
