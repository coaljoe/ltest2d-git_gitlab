package gui

import (
	xg "kristallos.ga/xgui"
	//"github.com/veandco/go-sdl2/sdl"
)

type IconBar struct {
	*xg.Widget
	minValue  int
	maxValue  int
	valueStep int
	value     int
	icon      xg.ImageI
}

func newIconBar(rect xg.Rect) *IconBar {
	ib := &IconBar{
		Widget:    xg.NewWidget("IconBar"),
		minValue:  0,
		maxValue:  100,
		valueStep: 10,
		value:     0,
	}
	ib.SetRect(rect)
	ib.Style.OuterBorderWidth = 2
	return ib
}

func (ib *IconBar) setIcon(path string) {
	ib.icon = xg.NewImage()
	ib.icon.SetScaleFilter(true)
	ib.icon.Create(50, 50)
	ib.icon.Load(path)
}

func (ib *IconBar) Render() {
	//pp(2)
	ib.Widget.Render()

	if ib.icon == nil {
		return
	}

	xw := ib.Rect().W()
	n := ib.value
	nStep := n / ib.valueStep
	iconStep := int(float64(ib.icon.W()) * 0.8)
	maxN := xw / iconStep
	pack := false
	//if n > maxN {
	if nStep > maxN {
		pack = true
	}

	p("nStep:", nStep, "maxN:", maxN)

	step := float64(maxN) / float64(n)
	_ = step

	absRect := ib.GetAbsoluteRect()

	//x := ((absRect.X() + ib.Rect().W()/2) - (ib.icon.W() / 2))
	x := absRect.X()
	y := ((absRect.Y() + ib.Rect().H()/2) - (ib.icon.H() / 2))
	w := ib.icon.W()
	h := ib.icon.H()

	if !pack {
		//numIcons := ib.value / ib.valueStep
		//iconsShiftStep := int(float64(ib.icon.W()) / 1.5)
		numIcons := n
		xsh := 0
		for i := 0; i < numIcons; i++ {
			ib.icon.Render(x+xsh, y, w, h)
			//xsh += iconsShiftStep
			xsh += iconStep
		}
	} else {
		panic(2)
	}
}
