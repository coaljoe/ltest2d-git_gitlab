package gui

import (
	xg "kristallos.ga/xgui"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"kristallos.ga/ltest2d/main_module"
)

var ndRand *rand.Rand

func init() {
	ndRand = rand.New(rand.NewSource(time.Now().Unix()))
}

type CampButton struct {
	id int
	on bool
	//alliance *string
	alliance int
	campId   main_module.CampId
	player   bool
	// Link to sheet
	s *GuiNewRandomGameSheet
	// View
	onButton   *xg.Button
	aButton    *xg.Button
	logoPanel  *xg.Panel
	titleLabel *xg.Label
}

func (cb *CampButton) setOn(v bool) {
	if v == true {
		cb.on = true
	} else {
		cb.on = false
	}
}

func (cb *CampButton) toggleAlliance() {
	/*
		s := "ABCDEFGH"
		if cb.alliance != nil {
			idx := strings.Index(s, cb.alliance)
			if
		} else {
			cb.alliance = "A"
		}
	*/
	if cb.alliance >= 0 {
		cb.alliance++
		if cb.alliance >= 4 {
			cb.alliance = -1
		}
	} else {
		cb.alliance = 0
	}

	//cb.aButton.Label().SetText(fmt.Sprintf("%d", cb.alliance))
	letterVal := ""
	if cb.alliance > -1 {
		//s := "ABCDEFGH"
		s := "ABCD"
		letterVal = string(s[cb.alliance])
	}
	cb.aButton.Label.SetText(fmt.Sprintf("%s", letterVal))
}

func (cb *CampButton) setCamp(cId main_module.CampId) {
	if !cId.Playable() {
		panic(fmt.Sprintf("CampId isn't playable;", cId))
	}

	cb.campId = cId
	cb.titleLabel.SetText(cb.campId.Name())
	cb.titleLabel.Style.TextColor = cb.campId.Color()

	if !cb.logoPanel.BgImage.HasImage() {
		cb.logoPanel.BgImage.SetScaleFilter(true)
		//cb.logoPanel.BgImage.ScaleFilter = false
		//cb.logoPanel.BgImage.Create(60-3*2, 60-3*2)
		//cb.logoPanel.BgImage.Create(20, 20)
		cb.logoPanel.BgImage.Create(5, 5)
		cb.logoPanel.ScaleBgImage = true
		//cb.logoPanel.BgImagePadding = xg.Rect{3, 3, 3, 3}
	}
	cb.logoPanel.BgImage.Load(fmt.Sprintf("res/gui/images/camp_logos/%s_logo.png",
		strings.ToLower(cb.campId.Name())))
}

func (cb *CampButton) setRandomCamp(deterministic bool) {
	var n int
	if deterministic {
		n = main_module.CampId_Min + rand.Intn(main_module.CampId_Max-1)
	} else {
		//r := rand.New(rand.NewSource(time.Now().Unix()))
		//n = r.Intn(CampId_Len)
		n = main_module.CampId_Min + ndRand.Intn(main_module.CampId_Max-1)
	}
	cId := main_module.CampId(n)
	cb.setCamp(cId)
}

func (cb *CampButton) toggleCamp() {
	//cb.setRandomCamp()
	nextCampId := int(cb.campId) + 1
	if nextCampId > int(main_module.CampId_Max) {
		nextCampId = main_module.CampId_Min
	}
	//pp(int(cb.campId), nextCampId)

	//cb.campId = CampId(nextCampId)
	campId := main_module.CampId(nextCampId)
	cb.setCamp(campId)
}

type GuiNewRandomGameSheet struct {
	*xg.Sheet
	// Link to gui
	g               *Gui
	topText         *xg.Label
	debugText       *xg.Label
	xsbl            *xg.Label
	backPanel       *xg.Panel
	bgPanel         *xg.Panel
	campButtons     []*CampButton
	newGameCallback func(pl interface{})
}

func TestCallback4(pl interface{}) {
	p("test callback4")
	panic(2)
}

func newGuiNewRandomGameSheet(g *Gui) *GuiNewRandomGameSheet {
	//game := main_module.GetGame()
	vars := main_module.GetVars()
	field := main_module.GetField()

	s := &GuiNewRandomGameSheet{
		Sheet:       xg.NewSheet("NewRandomGameSheet"),
		g:           g,
		campButtons: make([]*CampButton, 0),
	}
	fmt.Printf("-> %T\n", s)
	fmt.Printf("-> %v\n", s)
	fmt.Printf("-> %#v\n", s)
	g.xgi.SheetSys.AddSheet(s)
	//g.xgi.SetEnabled(g.enabled)

	s.backPanel = xg.NewPanel(xg.Rect{0, 0, vars.ResX(), vars.ResY()})
	bgImage := xg.NewImage()
	//bgImage.Load("res/gui/images/bg_noise2.png")
	//bgImage.Load("res/gui/images/bg_noise1.png")
	bgImage.Load("res/gui/images/bg_noise.png")
	s.backPanel.BgImage = bgImage
	s.backPanel.ScaleBgImage = true
	//s.backPanel.SetPassEvents(true)
	s.backPanel.Label.SetText("")
	s.Root().AddChild(s.backPanel)

	s.topText = xg.NewLabel(xg.Pos{0, 0}, "New Random Game Menu")
	s.topText.SetName("topText")
	//g.sheet.AddWidget(g.topText)
	s.topText.SetCallback(TestCallback4, nil)
	s.Root().AddChild(s.topText)

	s.debugText = xg.NewLabel(xg.Pos{0, 20}, "debugText")
	s.debugText.SetVisible(false)
	s.debugText.SetText("")
	s.Root().AddChild(s.debugText)

	// TODO: add base parent panel

	newGameCallback := func(pl interface{}) {
		ngt := main_module.GetNewGameTool()
		ngt.Reset()
		s.setupNewGameTool()
		ngt.InitNewGame()
		ngt.StartGame()
	}
	s.newGameCallback = newGameCallback

	_w, _h := vars.NativeResX(), vars.NativeResY()
	//bgPanel := xg.NewPanel(xg.Rect{(vars.resX - _w) / 2, (vars.resY - _h) / 2, _w, _h})
	
	// Padding
	//_padding := 10
	//_padding := 8
	_padding := 6
	//_padding_top := _padding * 2
	_padding_top := 18
	bgPanel := xg.NewPanel(xg.Rect{_padding + (vars.ResX() - _w) / 2, _padding_top + (vars.ResY() - _h) / 2,
		_w - _padding * 2, _h - (_padding_top + _padding )})
	
	bgPanel.Label.SetText("")
	s.Root().AddChild(bgPanel)
	s.bgPanel = bgPanel

	// Panel test
	//p := rg.NewPanel(rg.Rect{0, 0, 100, 100})
	//p := rg.NewPanel(rg.Rect{0, 0, 10, 10})
	//p := rg.NewPanel(rg.Rect{0, 0, .2, .2})
	//p := xg.NewPanel(xg.Rect{.5, .5, .5, .5})
	//pw, ph := 0.3, 0.1
	//margin := 0.01
	pw, ph := 300, 50
	margin := 20
	shy := ph + margin
	//p := xg.NewPanel(xg.Rect{.5 - pw, .5 - ph, pw, ph})
	//p := xg.NewPanel(xg.Rect{.5 - (pw / 2), (.5 - (ph / 2)) + shy, pw, ph})
	//p := xg.NewPanel(xg.Rect{vars.resX/2 - (pw / 2), (vars.resY/2 - (ph / 2)) + shy, pw, ph})
	p := xg.NewPanel(xg.Rect{360, 490, 280, 60})
	p.Label.SetText("Play game")
	p.Label.TextAlign = xg.TextAlignType_Center
	p.SetCallback(newGameCallback, nil)
	//s.Root().AddChild(p)
	bgPanel.AddChild(p)

	// Map preview panel
	//w, h := 0.2, 0.2*vars.resAspect
	//w, h := 200, 150
	//w, h := 200, 200
	//w, h := 280, 280
	//p2 := xg.NewPanel(xg.Rect{.5 - (w / 2), (.5 - (h / 2) - h) + shy, w, h})
	//p2 := xg.NewPanel(xg.Rect{vars.resX/2 - (w / 2), (vars.resY/2 - (h / 2) - h) + shy, w, h})
	p2 := xg.NewPanel(xg.Rect{360, 20, 280, 280})
	p2.Label.SetText("map preview")
	p2.Label.TextAlign = xg.TextAlignType_Center
	//p2.BgImagePadding = xg.Rect{3, 3, 3, 3}
	p2.BgImagePadding = xg.Rect{5, 5, 5, 5}
	//s.Root().AddChild(p2)
	bgPanel.AddChild(p2)
	//pp(p2.Id())
	//pdump(p2)
	//p2.Update(1)

	generateCallback := func(pl interface{}) {
		ngt := main_module.GetNewGameTool()

		// Read fw from the gui
		fw := s.GetWidgetByName("mapWidthSpinButton").(*xg.SpinButton).IntValue()
		fh := s.GetWidgetByName("mapHeightSpinButton").(*xg.SpinButton).IntValue()

		ngt.SetFw(fw)
		ngt.SetFh(fh)
		ngt.GeneratePreviewField()

		hmW := fw
		hmH := fh
		hmOpt := field.GetHm().Opt()

		pm := p2
		pm.Label.SetText("")
		//pm.BgImage.ScaleFilter = true
		pm.BgImage.Create(hmW, hmH)
		pm.ScaleBgImage = true
		pm.BgImage.Load(vars.TmpDir() + "/gui_hm_color.png")

		// Save variables
		//vars.S_fw = fw
		//vars.s_fh = fh
		//vars.s_hmOpt = hmOpt
		//vars.s_generateField = true
		vars.Set_s_fw(fw)
		vars.Set_s_fh(fh)
		vars.Set_s_hmOpt(hmOpt)
		vars.Set_s_generateField(true)

		//p_(pw)
		//pp(5)
	}

	// Generate button
	//b := xg.NewButton(xg.Rect{.5 - (pw / 2), (.5 - (ph / 2) - ph - margin) + shy, pw, ph})
	//b := xg.NewButton(xg.Rect{vars.resX/2 - (pw / 2), (vars.resY/2 - (ph / 2) - ph - margin) + shy, pw, ph})
	b := xg.NewButton(xg.Rect{360, 340, 280, 60})
	b.Label.SetText("Generate")
	//b.SetCallback(TestCallback4)
	b.SetCallback(generateCallback, nil)
	//s.Root().AddChild(b)
	//b.Style.OuterBorderWidth = 2
	//b.Style.OuterBorderColor = xg.Color{10, 20, 100}
	bgPanel.AddChild(b)

	// Generate button
	//b2 := xg.NewButton(xg.Rect{.5 - (pw / 2), (.5 - (ph / 2) + ph + margin) + shy, pw, ph})
	b2 := xg.NewButton(xg.Rect{vars.ResX()/2 - (pw / 2), (vars.ResY()/2 - (ph / 2) + ph + margin) + shy, pw, ph})
	b2.Label.SetText("Generate 2")
	//b.SetCallback(TestCallback4)
	b2.SetCallback(generateCallback, nil)
	//s.Root().AddChild(b2)

	// Spin button
	//w, h = 0.05, 0.05*vars.resAspect
	//w, h = 0.04, 0.10*vars.resAspect
	//sb := xg.NewSpinButton(xg.Rect{.5 - (pw / 2), (.5 - (ph / 2) - -(ph * 2) - -(margin * 2)) + shy, w, h})
	//sb := xg.NewSpinButton(xg.Rect{.5 - (w / 2), (.5 - (h / 2) - -(ph * 2.5) - -(margin * 2)) + shy, w, h})
	// Horiz
	//w, h = 0.12, 0.03*vars.resAspect
	//w, h = 120, 30
	//sb := xg.NewSpinButton(xg.Rect{.5 - (w / 2), (.5 - (h / 2) - -(ph * 2.5) - -(margin * 2)) + shy, w, h})
	//sb := xg.NewSpinButton(xg.Rect{vars.resX/2 - (w / 2), (vars.resY/2 - (h / 2) - -int(float64(ph)*2.5) - -(margin * 2)) + shy, w, h})
	sb := xg.NewSpinButton(xg.Rect{140, 60, 150, 40})
	sb.SetName("mapWidthSpinButton")
	sb.Value = 128
	sb.ValueStep = 128
	sb.ValueRangeMin = 128
	sb.ValueRangeMax = 1024
	if main_module.GetVarsDev() {
		sb.ValueRangeMin = 64
	}
	//sb.SetStyleVertical(
	//pp(sb.Rect(), sb.GetAbsoluteRect())
	//s.Root().AddChild(sb)
	bgPanel.AddChild(sb)

	// Spin button label
	//lshx := -0.1
	//lshy := 0.01
	//lshx := -100
	//lshy := 10
	//sbl := xg.NewLabel(xg.Pos{.5 - (w / 2), .5 - (h / 2)}, "Map width:")
	//sbl := xg.NewLabel(xg.Pos{.5 - (w / 2) + lshx, (.5 - (h / 2) - -(ph * 2.5) - -(margin * 2)) + shy + lshy},
	//sbl := xg.NewLabel(xg.Pos{vars.resX/2 - (w / 2) + lshx, (vars.resY/2 - (h / 2) - -int(float64(ph)*2.5) - -(margin * 2)) + shy + lshy},
	//	"Map width:")
	sbl := xg.NewLabel(xg.Pos{20, 70}, "Map width:")
	//s.xsbl = sbl
	sbl.SetName("mapWidthSpinButtonLabel")
	//sbl.TextAlign = xg.TextAlignType_Center
	//sbl.SetPassEvents(true)
	//sbl.SetCallback(TestCallback4)
	//s.Root().AddChild(sbl)
	bgPanel.AddChild(sbl)

	// Map height
	{
		sb := xg.NewSpinButton(xg.Rect{140, 120, 150, 40})
		sb.SetName("mapHeightSpinButton")
		sb.Value = 128
		sb.ValueStep = 128
		sb.ValueRangeMin = 128
		sb.ValueRangeMax = 1024
		if main_module.GetVarsDev() {
			sb.ValueRangeMin = 64
		}
		//sb.SetStyleVertical(
		//pp(sb.Rect(), sb.GetAbsoluteRect())
		//s.Root().AddChild(sb)
		bgPanel.AddChild(sb)

		sbl := xg.NewLabel(xg.Pos{20, 130}, "Map height:")
		//s.xsbl = sbl
		sbl.SetName("mapHeightSpinButtonLabel")
		//sbl.TextAlign = xg.TextAlignType_Center
		//sbl.SetPassEvents(true)
		//sbl.SetCallback(TestCallback4)
		//s.Root().AddChild(sbl)
		bgPanel.AddChild(sbl)
	}

	// Sea level
	{
		/*
			b := xg.NewButton(xg.Rect{140, 180, 150, 40})
			b.SetName("seaLevelSlider")
			b.Label().SetText("-----0----")
			bgPanel.AddChild(b)
		*/

		s := xg.NewSlider(xg.Rect{140, 180, 150, 40})
		s.SetSliderPosition(0.5)
		bgPanel.AddChild(s)

		l := xg.NewLabel(xg.Pos{20, 190}, "Sea level:")
		l.SetName("seaLevelSliderLabel")
		bgPanel.AddChild(l)
	}

	// Resources
	{
		/*
			b := xg.NewButton(xg.Rect{140, 240, 150, 40})
			b.SetName("resourcesSlider")
			b.Label().SetText("-----0----")
			bgPanel.AddChild(b)
		*/

		s := xg.NewSlider(xg.Rect{140, 240, 150, 40})
		s.WidgetStyle().ScoreBarColor = xg.ColorBlack
		s.ValueRangeMin = 1000
		s.ValueRangeMax = 5000
		s.SetSliderValue(4000)
		//s.SetSliderPosition(0.5)
		bgPanel.AddChild(s)

		l := xg.NewLabel(xg.Pos{20, 250}, "Resources:")
		l.SetName("resourcesSliderLabel")
		bgPanel.AddChild(l)
	}

	// Money
	{
		/*
			b := xg.NewButton(xg.Rect{140, 300, 150, 40})
			b.SetName("moneySlider")
			b.Label().SetText("-----0----")
			bgPanel.AddChild(b)
		*/
		s := xg.NewSlider(xg.Rect{140, 300, 150, 40})
		//s.Style.SliderScoreBarColor = xg.ColorBlack
		s.ValueRangeMin = 5000
		s.ValueRangeMax = 15000
		s.SetSliderValue(10000)
		bgPanel.AddChild(s)

		l := xg.NewLabel(xg.Pos{20, 310}, "Money:")
		l.SetName("moneySliderLabel")
		bgPanel.AddChild(l)
	}

	// Season
	{
		b := xg.NewButton(xg.Rect{140, 360, 150, 40})
		b.SetName("seasonButton")
		b.Label.SetText("Winter")
		bgPanel.AddChild(b)

		l := xg.NewLabel(xg.Pos{20, 370}, "Season:")
		l.SetName("seasonButtonLabel")
		bgPanel.AddChild(l)
	}

	//// Add camps buttons

	makeCampButton := func(x, y, id int) *xg.Panel {

		//var titleLabelCB func(pl interface{})

		titleLabelCB := func(idi interface{}) {
			rId := idi.(int)
			//pp(rId)

			cb := s.campButtons[rId]
			cb.toggleCamp()
		}

		// Bg
		_pName := fmt.Sprintf("campButton_%d", id)
		_p := xg.NewPanel(xg.Rect{x, y, 120, 120})
		_p.SetName(_pName)
		_p.Label.SetText(fmt.Sprintf("%d", id))
		_p.Label.TextAlign = xg.TextAlignType_Center
		_p.SetCallback(titleLabelCB, id)
		bgPanel.AddChild(_p)

		//// Callbacks
		onButtonCB := func(idi interface{}) {
			rId := idi.(int)
			//pp(rId)
			//s.GetWidgetByName(fmt.Sprintf("campButton_%d", id)).(*
			//onButton := s.GetWidgetById(rId).(*xg.Button)

			//dump(s.campButtons)
			//pp(rId, len(s.campButtons))
			cb := s.campButtons[rId]
			cb.setOn(!cb.on)

			if cb.on {
				//onButton.
				p := cb.onButton.Panel
				p.BgImageHide = false
				if !p.BgImage.HasImage() {
					//p.BgImage.Create(hmW, hmH)
					//p.BgImage.Create(16, 16)
					p.BgImage.Create(14, 14)
					//p.ScaleBgImage = true
					p.ScaleBgImage = false
					//p.BgImage.Load(vars.tmpDir + "/gui_hm_color.png")
					p.BgImage.Load("res/gui/images/camp_button_on_bg.png")
				}
			} else {
				p := cb.onButton.Panel
				p.BgImageHide = true
			}
		}

		aButtonCB := func(idi interface{}) {
			rId := idi.(int)
			//pp(rId)

			cb := s.campButtons[rId]
			cb.toggleAlliance()
		}

		// Top-Left on button
		onButton := xg.NewButton(xg.Rect{0, 0, 20, 20})
		onButton.Label.SetText("")
		//onButton.Panel.BgImagePadding = xg.Rect{5, 5, 5, 5}
		onButton.Panel.BgImagePadding = xg.Rect{3, 3, 3, 3}
		//onButton.SetCallback(onButtonCB, id)
		//onButton.SetCallback(onButtonCB, onButton.Id())
		onButton.WidgetStyle().NoPressed = true
		onButton.SetCallback(onButtonCB, id)
		_p.AddChild(onButton)

		// Top-right alliance button
		aButton := xg.NewButton(xg.Rect{100, 0, 20, 20})
		aButton.Label.SetText("")
		aButton.Label.Style.TextFontName = "rl_font_bold"
		aButton.Label.Style.TextColor = xg.Color{10, 10, 10}
		aButton.WidgetStyle().NoPressed = true
		aButton.SetCallback(aButtonCB, id)
		_p.AddChild(aButton)

		// Logo panel
		logoPanel := xg.NewPanel(xg.Rect{20, 0, 80, 80})
		logoPanel.Label.SetText("")
		//logoPanel.WidgetStyle().DrawAsRect = true
		//logoPanel.WidgetStyle().DrawRectBorderWidth = 2
		logoPanel.WidgetStyle().SetDrawTypeRect()
		logoPanel.WidgetStyle().RectStyle.BorderWidth = 2
		//logoPanel.DrawRectBorderColor = xg.ColorBlack
		//logoPanel.DrawRectBorderColor = xg.ColorBlue
		//logoPanel.WidgetStyle().DrawRectBorderColor = xg.Color{70, 70, 70}
		logoPanel.WidgetStyle().RectStyle.BorderColor = xg.Color{70, 70, 70}
		logoPanel.SetCallback(titleLabelCB, id)
		_p.AddChild(logoPanel)

		// Title label
		titleLabel := xg.NewLabel(xg.Pos{60, 100}, "Reds")
		titleLabel.TextAlign = xg.TextAlignType_Center
		//pp(s.g)
		//pp(game.gui.xgi.Style.Fonts)
		titleLabel.Style.TextFontName = "text_font_bold2"
		titleLabel.SetCallback(titleLabelCB, id)
		_p.AddChild(titleLabel)

		// Create record/class
		cb := &CampButton{}
		cb.id = id
		cb.s = s
		cb.alliance = -1
		cb.onButton = onButton
		cb.aButton = aButton
		cb.logoPanel = logoPanel
		cb.titleLabel = titleLabel

		//cb.setRandomCamp(true)
		cb.setRandomCamp(false)
		s.campButtons = append(s.campButtons, cb)

		if id == 0 {
			cb.player = true
		}

		if main_module.GetVarsDev() {
		//if vars.testGame {
			if id == 0 {
				cb.setCamp(main_module.CampId_Reds)
			}

			if id == 1 {
				cb.setCamp(main_module.CampId_Arctic)
			}
		}

		if id == 0 || id == 1 {
			//cb.setOn(true) // XXX doesn't work
			onButtonCB(id)
		}

		return _p
	}
	_ = makeCampButton

	x := 710
	//y := 200
	//y := 60
	y := 30
	pw = 2
	ph = 4
	/*
		for i := 0; i < pw; i++ {
			for j := 0; j < ph; j++ {
				px := x + i*110
				py := y + j*110
				id := i + j*pw
				_ = px
				_ = py
				_ = id
				makeCampButton(px, py, id)
			}
		}
	*/
	for i := 0; i < ph; i++ {
		for j := 0; j < pw; j++ {
			px := x + j*130
			py := y + i*130
			id := i*pw + j
			_ = px
			_ = py
			_ = id
			makeCampButton(px, py, id)
		}
	}

	if false {
		panic(s.topText.GetAbsoluteRect())
		panic(b.GetAbsoluteRect())
		panic(sbl.GetAbsoluteRect())
		panic(sbl.Rect())
	}

	/*
		println()
		println()
		println()
		//p_(s.Root().GetAllChildren())
		//p_(s.GetWidgets())
		allWs := s.Root().GetAllChildren()
		publicWs := s.Root().GetAllPublicChildren()

		println()
		println()
		println()
		p_("publicWs:", publicWs)
		p_("len:", len(allWs), len(publicWs))
		pp(2)
	*/

	return s
}

func (s *GuiNewRandomGameSheet) setupNewGameTool() {
	//game := main_module.GetGame()

	ngt := main_module.GetNewGameTool()
	ngt.Reset()

	for _, b := range s.campButtons {
		if !b.on {
			continue
		}
		// Transfer button data to ngt
		var ptype main_module.PlayerType
		name := ""
		if b.player {
			ptype = main_module.PlayerType_Human
			name = "Player1"
		} else {
			ptype = main_module.PlayerType_AI
			name = fmt.Sprintf("AIPlayer%d", b.id)
		}
		p := main_module.NewPlayer(ptype, b.campId)
		p.SetName(name)
		ngt.AddPlayer(p)
		//pdump(ngt.players)
		//pp(p)
	}
	//pdump(ngt.players)
	//pp(2)
}

func (s *GuiNewRandomGameSheet) Render() {
	//_log.Dbg("%F", game.frame)
	// Call base
	s.Sheet.Render()
}

func (s *GuiNewRandomGameSheet) Update(dt float64) {
	//_log.Dbg("%F")
	// Call base
	s.Sheet.Update(dt)

	//pp(2)
}
