local base = import 'res/buildings/bunker/base_bunker.libsonnet';

{
    // Building
    building: base.building {
        camp: "suur",
    },
  
    // Combat
    combat: base.combat {
    },

    // View
    view: base.view {
    }
}
