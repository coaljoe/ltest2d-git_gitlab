local base = import 'res/buildings/steel_mill/base_steel_mill.libsonnet';

{
    // Building
    building: base.building {
        camp: "reds",
    },
  
    // Combat
    combat: base.combat {
    },

    // View
    view: base.view {
    }
}
