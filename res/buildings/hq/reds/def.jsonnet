local base = import 'res/buildings/hq/base_hq.libsonnet';

{
    // Building
    building: base.building {
        camp: "reds",
    },
  
    // Combat
    combat: base.combat {
    },

    // View
    view: base.view {
    }
}
