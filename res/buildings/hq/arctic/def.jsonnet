local base = import 'res/buildings/hq/base_hq.libsonnet';

{
    // Building
    building: base.building {
        camp: "arctic",
    },
  
    // Combat
    combat: base.combat {
    },

    // View
    view: base.view {
    }
}
