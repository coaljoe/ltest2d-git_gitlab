local base = import 'res/buildings/factory/base_factory.libsonnet';

{
    // Building
    building: base.building {
        camp: "arctic",
    },
  
    // Combat
    combat: base.combat {
    },

    // View
    view: base.view {
    }
}
