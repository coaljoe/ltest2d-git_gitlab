local ammo_types = import 'ammo_types/lib.libsonnet';
//local ammo_types = import 'res/weapons/ammo_types/lib.libsonnet';

{
    type: "gun",
    name: "generic_76mm_gun",
    //tag: "main_gun",
    //"caliber": 76,
    fire_rate: 8,
    max_rounds: 10,
    maintance_cost: 100,
    ammo: 10,
    ammo_charge: 1,
    //"ammotype": "76mm_gun_ammo_reds",
    ammotype: ammo_types.generic_76mm_gun_shell,
    attack_delay: 0.5,
    realm: "ground",
}