//ammotype
{
    type: "shell", // Projectile ?
    name: "generic_76mm_gun_shell",
    caliber: 76,
    cost: 0,
    move_speed: 100,
    reliability: 0.9,
    accuracy: 0.8,
    weight: 9,
    affect_radius: 1,
    radius: 10.0,
    locate_radius: 20.0, 
    attack_rating: 3,
}