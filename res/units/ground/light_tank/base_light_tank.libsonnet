local uc_lib = import 'res/unit_components/lib.libsonnet';

{
    unit: {
        gui_name: "Light Tank",
        name: "light_tank",
        //static: false,
    },
    
    combat: {},
    
    unitAi: {
        moving_behavior: {
            type: "standard",
            target_locate_radius: 8,
            target_pursuit_time: 10,
        }
    },
    
    tank: {},
    
    //turret0: {},
    turretSlots: {
        /*
        turrets: [
            {
                
                return_enabled: true,
                hosted_weapon_tag: "main_gun",
            },
        ],
        */
        //turrets.main: uc.generic_turret,
        //turrets.side: uc.generic_machinegun_turret,
        /*
        turrets: {},
        turrets.main: uc.generic_turret { slot_name: "main" },
        turrets.side: uc.generic_machinegun_turret { slot_name: "side" }
        */
        //turrets:
        //{
        //main: uc_lib.generic_turret,
        //main: uc_lib.generic_turret { slot_name: "main" },
        //side: uc_lib.generic_machinegun_turret { slot_name: "side" },
        main: uc_lib.generic_tank_turret,
        //},
    },
    
    view: {},
}