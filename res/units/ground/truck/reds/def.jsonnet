local base = import 'res/units/ground/truck/base_truck.libsonnet';

{
    //Unit
    unit: base.unit {
        camp: "reds",
        weight: 2000.0,
        moveSpeed: 30.0,
        accSpeed: 10.0,
        armor: 50,
        maxFuel: 50.0,
    },
  
    // Combat
    combat: base.combat {
        defenceRating: 1,
    },
   
    // UnitAi
    unitAi: base.unitAi,

    // View
    view: base.view {
    }
}
