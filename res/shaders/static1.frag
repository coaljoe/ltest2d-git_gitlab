#include "uniforms.frag"
#include "lighting.frag"


void main() {
    vec4 color;
	//vec3 c;

    //vec3 c = ambient + calcLambertLight(L, N, mat.diffuse, light.intensity);
	//color = vec4(c, 1.0);
	color = vec4(ambient + calcLambertLight(L, N, mat.diffuse, light.intensity), 1.0);

#ifdef _TEXTURE_MAP
    color *= texture2D(texMap, gl_TexCoord[0].st);
#endif

    color *= calcShadow();

    gl_FragColor = color;
}

/* vim:set ft=glsl: */
