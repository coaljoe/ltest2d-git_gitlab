package main_module

import (
	. "kristallos.ga/rx/math"
	"math"
)

// entity
type Unit struct {
	*Obj
	*PlayerRecord
	id      int
	name    string
	guiName string
	//player  *Player
	campId  CampId
	cx, cy  int
	prevCx  int
	prevCy  int
	//dir         Dir // In position
	moveSpeed   float64
	maxRotSpeed float64
	realm       RealmType
	static      bool
	mover       MoverI
	// components
	health      *Health
	combat      *Combat
	position    *Position
	unitAi      *UnitAi
	tank        *Tank
	turretSlots  *TurretSlots
	//bunker   *Bunker
}

func newUnit() *Unit {
	u := &Unit{
		Obj: newObj("unit"),
		PlayerRecord: newPlayerRecord(nil),
		id:  _ider.GetNextId("Unit"),
		//name:   "",
		//player: p,
		//campId: p.camp.id,
		//dir:         newDir(),
		moveSpeed:   10.0,
		maxRotSpeed: 100.0,
		realm:       RealmType_Ground,
		static:      false,
		health:      newHealth(),
	}
	//e.colobj = newColObj(e.Obj, 5)
	//u.combat = newCombat(u, u.health)
	u.combat = newCombat(u)
	u.position = newPosition(u.Obj)
	// XXX not a component? fixme/check
	u.mover = newGroundMover(u)
	u.unitAi = newUnitAi(u)
	//e.movingBhv = newStandardMovingBhv(e)
	//e.movingBhv = newStrafingMovingBhv(e)
	//e.view = newUnitView(e)

	// Tune
	//u.combat.weapon1.fireRate = 35
	//e.combat.weapon1.shotSound = "res/audio/shot1.wav"
	//e.combat.weapon1.shotName = "shot3"

	game.unitsys.addElem(u)
	return u
}

func (u *Unit) getRealm() RealmType { return u.realm }
func (u *Unit) getHealth() *Health { return u.health }
func (u *Unit) getCombat() *Combat { return u.combat }
func (u *Unit) getComponents() []ComponentI {
	return []ComponentI{u.health, u.combat, u.position, u.unitAi, u.tank, u.turretSlots }
}

func (u *Unit) start() {
	//u.unitAi.start()
	StartEntity(u)
}

func (u *Unit) at(cx, cy int) bool {
	return u.cx == cx && u.cy == cy
}

func (u *Unit) placeAt(cx, cy int) {
	u.cx = cx
	u.cy = cy
	u.SetPos(Vec3{float64(u.cx)*cell_size + cell_size/2,
		float64(u.cy)*cell_size + cell_size/2, 0})

	u.step(cx, cy)
}

// Change unit position to `cx`, `cy`
func (u *Unit) step(cx, cy int) {
	_log.Dbg("step cx, cy:", cx, cy)
	cOld := &game.field.cells[u.cx][u.cy]
	cOld.open = true
	cOld.unitId = -1
	u.prevCx = u.cx
	u.prevCy = u.cy

	c := &game.field.cells[cx][cy]
	c.open = false
	c.unitId = u.id
	u.cx = cx
	u.cy = cy

	if HasPlayer() {
		GetPlayer().GetFow().fillCircle(u.cx, u.cy, 10, false)
	}

	PubS("ev_unit_step", u, u.cx, u.cy, u.prevCx, u.prevCy)
}

func (u *Unit) convertToStaticUnit() {
	u.mover = nil
	//u.fuel = nil
	u.static = true
}

func (u *Unit) isStaticUnit() bool {
	return u.static
}

// nil means no player was assigned / no owner (?)
func (u *Unit) assignPlayer(p *Player) {
	_log.Inf("%F p:", p)

	// XXX check camp compatibility?

	u.player = p
	// XXX fixme?
	u.campId = p.camp.Id
}

// For military score calculation.
func (u *Unit) getUnitMilitaryScore() float64 {
	/*
		_, maxAr := c_Combat(u).getAttackRatingRange()
		//unitPower := maxAr
		unitPower := math.Pow(float64(maxAr), 1.5)
	*/

	// Sum of all units' weapons power (attackRating^1.5)
	// So the unit with few poverful weapons will get bigger score
	// Because it can attack from both weapons(?)
	unitPower := 0.0
	for _, w := range u.combat.getWeapons() {
		unitPower += math.Pow(float64(w.attackRating()), 1.5)
	}

	// Navy units are twice less powerful in military score
	// (like in original method from a civ game)
	// XXX: what about air units?
	if u.realm == RealmType_Navy {
		unitPower /= 2.0
	}

	// XXX: exclude static units?

	return unitPower
}

func (u *Unit) explode() {
	p("unit explode")
}

func (u *Unit) debugDraw() {
	if u.mover != nil {
		u.mover.debugDraw()
	}
	u.position.debugDraw()
}

func (u *Unit) update(dt float64) {
	//u.cx = int(u.PosX() / cell_size)
	//u.cy = int(u.PosY() / cell_size)

	{
		step := false
		ncx, ncy := u.cx, u.cy
		// XXX fixme: only sets new cx, cy when the position is at cell's center
		// not very reliable and fast
		if u.cellPosX() != u.cx {
			newCx := int(u.PosX()-cell_size/2) % cell_size
			if newCx == 0 {
				//u.cx = u.CellPosX()
				ncx = u.cellPosX()
				step = true
			}
		}
		if u.cellPosY() != u.cy {
			newCy := int(u.PosY()-cell_size/2) % cell_size
			if newCy == 0 {
				//u.cy = u.CellPosY()
				ncy = u.cellPosY()
				step = true
			}
		}

		if step {
			u.step(ncx, ncy)
		}
	}

	//newCx := u.CellPosX() * cell_size
	//u.cy = int((u.PosY() + cell_size/2) / cell_size)

	if u.mover != nil {
		u.mover.update(dt)
	}
	if u.turretSlots != nil {
		u.turretSlots.update(dt)
	}
	u.unitAi.update(dt)
	u.position.update(dt)
}
