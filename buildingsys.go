package main_module

import (
	//. "kristallos.ga/rx/math"
	"fmt"
	"math/rand"
	"sort"
)

type BuildingSys struct {
	*GameSystem
}

func newBuildingSys() *BuildingSys {
	s := &BuildingSys{}
	s.GameSystem = newGameSystem("BuildingSys", "Building system", s)
	return s
}

func (s *BuildingSys) getBuildingById(id int) *Building {
	for _, el := range s.getElems() {
		b := el.(*Building)
		if b.id == id {
			return b
		}
	}
	//return nil
	panic(fmt.Sprintf("object not found; id=%d", id))
}
func (s *BuildingSys) getBuildingByPosCheck(cx, cy int) *Building {
	for _, el := range s.getElems() {
		b := el.(*Building)
		if b.cx == cx && b.cy == cy {
			return b
		}
	}
	return nil
}
func (s *BuildingSys) getBuildingByPos(cx, cy int) *Building {
	b := s.getBuildingByPosCheck(cx, cy)
	if b == nil {
		pp("error: building with pos not found; cx, cy:", cx, cy)
	}
	return nil // Never returns
}

// Find all suitable positions for building area in a field's rect `fRect`
func (s *BuildingSys) findPlacesForBuilding(bAreaX, bAreaY int, fRect CRect) (
	suitable bool, poss []CPos) {
	_log.Dbg("%F: bAreaX:", bAreaX, "bAreaY:", bAreaY, "fRect:", fRect)
	f := game.field
	//pos = CPos{-1, -1}

	for y := fRect.y; y < (fRect.y+fRect.h)-bAreaY; y++ {
		for x := fRect.x; x < (fRect.x+fRect.w)-bAreaX; x++ {
			p("-> x,y", x, y)

			// Walk building area
			good := true
			for j := 0; j < bAreaY; j++ {
				for i := 0; i < bAreaX; i++ {
					px := x + i
					py := y + j
					//p("-> px,py", px, py)
					//c := &f.cells[px][py]
					c := f.GetCell(px, py)
					//if !c.z.isMovable() {
					if !c.Walkable() {
						good = false
					}
				}
			}

			if good {
				suitable = true
				pos := CPos{x, y}
				poss = append(poss, pos)
				p("-> added", pos)
				//break outer
			}
		}
	}

	dump := false
	if dump {
		if len(poss) > 0 {
			pos := poss[0]
			for y := 0; y < bAreaY; y++ {
				for x := 0; x < bAreaX; x++ {
					px := pos.x + x
					py := pos.y + y
					p("dump pos:", pos)
					p("dump x,y:", x, y)
					//c := &f.cells[px][py]
					c := f.GetCell(px, py)
					pv(c)
				}
			}
		}
	}

	_log.Dbg("done %F")
	return suitable, poss
}

// Find non-overlapping places for buildings in area `fRect`
func (s *BuildingSys) findPlacesForBuildings(bSizes []CPos, numSites []int, fRect CRect) (
	suitable bool, rects []CRect) {
	_log.Dbg("%F: bSizes:", bSizes, "numSites:", numSites, "fRect:", fRect)
	f := game.field
	_ = f

	retSuitable := false
	retRects := []CRect{}
	//outer:
	//for y := fRect.y; y < fRect.y+fRect.h; y++ {
	//	for x := fRect.x; x < fRect.x+fRect.w; x++ {
	//c := &f.cells[x][y]
	//if c.z.isMovable() {
	//	count++
	//}

	// Preserve values/order for use with sortedBSizes
	numSitesForBSizes := make(map[CPos]int)
	for i, bSize := range bSizes {
		numSitesForBSizes[bSize] = numSites[i]
	}

	sortedBSizes := make([]CPos, len(bSizes))
	copy(sortedBSizes, bSizes)
	sort.SliceStable(sortedBSizes, func(i, j int) bool {
		return (sortedBSizes[i].x < sortedBSizes[j].x) &&
			(sortedBSizes[i].y < sortedBSizes[j].y)
	})
	//p("bSizes:", bSizes)
	//p("sorted:", sortedBSizes)
	//pp(2)

	// Make bSizes the sorted ones
	// XXX BUG? numSites aren't sorted
	//bSizes = sortedBSizes

	// Suitable area rect candidates (building places)
	// non-conflicting, non-overlapping
	placeCandidates := make([]CRect, 0)

	// From larger site to smaller
	//outer:
	for k := len(sortedBSizes) - 1; k >= 0; k-- {
		p("k:", k)
		bSize := sortedBSizes[k]
		numSites_ := numSitesForBSizes[bSize]

		if numSites_ < 1 {
			continue
		}

		//_log.ToggleEnabled()
		//_log.SetEnabled(false)
		//__disable_p = true
		suitable, locs := game.buildingsys.findPlacesForBuilding(bSize.x, bSize.y,
			//CRect{x, y, fRect.w, fRect.h})
			CRect{fRect.x, fRect.y, fRect.w, fRect.h})
		//__disable_p = false
		//_log.ToggleEnabled()

		if !suitable || len(locs) < numSites_ {
			continue
		}

		//p("area x, y:", x, y)
		p("locs:", locs)

		// TODO: find non-overlapping areas for locs

		shuffledLocs := make([]CPos, len(locs))
		copy(shuffledLocs, locs)
		// Randomize
		a := shuffledLocs
		//rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(a), func(i, j int) { a[i], a[j] = a[j], a[i] })

		//p(locs)
		//pp(shuffledLocs)

		/*
			//randIdx := rand.Intn(len(locs) - 1)
			//loc := locs[randIdx]
			loc := shuffledLocs[0]
			//rects := fg.campSites[player.id]
			rect := CRect{loc.x, loc.y, siteSize.x, siteSize.y}
			areaRects = append(areaRects, rect)
		*/

		p("shuffledLocs:", shuffledLocs)

		for n := 0; n < numSites_; n++ {
			p("site num:", n)
			for _, loc := range shuffledLocs {
				locRect := CRect{loc.x, loc.y, bSize.x, bSize.y}
				p("processing locRect:", locRect)
				// Check if loc overlap with place candidates
				good := true
				for _, candidateRect := range placeCandidates {
					p("test candidateRect:", candidateRect, "agaist locRect:", locRect)
					if candidateRect == locRect {
						good = false
						p("SAME BAD")
						break
						//continue
					}
					if candidateRect.overlap(locRect) {
						good = false
						p("BAD")
						break
					}
				}

				if good {
					// Add rect to candidates
					p("adding candidate locRect:", locRect)
					placeCandidates = append(placeCandidates, locRect)
					p("-> current placeCandidates:", placeCandidates)
				}
			}
		}
	}

	// Process place candidates
	// Count results
	count := make(map[CPos]int)
	for _, candidate := range placeCandidates {
		k := CPos{candidate.w, candidate.h}
		count[k] += 1
	}

	p("count:", count)

	/*
		i := 0
		//p(count[bSizes[i]], numSites[i])
		if count[bSizes[i]] >= numSites[i] {
			retSuitable = true
			retRects = placeCandidates[0:]
			//pp(4)
			break outer
		}
	*/

	// Check results
	for _, bSize := range sortedBSizes {
		c := count[bSize]
		n := numSitesForBSizes[bSize]
		if c < n {
			p("count check fail, break")
			p("c:", c, "n:", n, "bSize:", bSize)
			return retSuitable, retRects
			//pp(2)
			//break outer // Fail
		}
	}

	//}
	//}

	// Finally
	retSuitable = true
	retRects = placeCandidates[0:]
	//pp(4)
	//break outer

	return retSuitable, retRects
}

func (s *BuildingSys) update(dt float64) {
}
