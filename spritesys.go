package main_module

type SpriteSys struct {
	*GameSystem
}

func newSpriteSys() *SpriteSys {
	s := &SpriteSys{}
	s.GameSystem = newGameSystem("SpriteSys", "Sprite system", s)
	return s
}

func (s *SpriteSys) draw() {
	p("Z", s.elems)
	for _, eli := range s.getElems() {
		el := eli.(SpriteI)
		el.draw()
	}
}

func (s *SpriteSys) update(dt float64) {
}
