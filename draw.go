package main_module

import (
	. "kristallos.ga/rx/math"
	//"github.com/veandco/go-sdl2/gfx"
	"github.com/veandco/go-sdl2/sdl"
)

var (
	ColorWhite sdl.Color = sdl.Color{255, 255, 255, 255}
	ColorBlack sdl.Color = sdl.Color{0, 0, 0, 255}
	ColorRed   sdl.Color = sdl.Color{255, 0, 0, 255}
	ColorGreen sdl.Color = sdl.Color{0, 255, 0, 255}
	ColorBlue  sdl.Color = sdl.Color{0, 0, 255, 255}
)

func getPw() float64 {
	return 1.0 / float64(vars.resX)
}

func getPh() float64 {
	return 1.0 / float64(vars.resY)
}

func DrawRectangleColorWidth(x, y, w, h int, color sdl.Color, lineWidth int) {
	//lineWidth := int32(2)
	//lineWidth := int32(1)
	//lineWidth = 10
	//lineColor := sdl.Color{0, 255, 0, sdl.ALPHA_OPAQUE}
	//lineColor := sdl.Color{255, 255, 255, 64}
	//lineColor := sdl.Color{255, 255, 255, 255}
	topLeftPointX := x
	topLeftPointY := y
	topRightPointX := x + w
	topRightPointY := y
	bottomLeftPointX := x
	bottomLeftPointY := y + h
	bottomRightPointX := x + w
	bottomRightPointY := y + h
	_ = bottomRightPointX
	_ = bottomRightPointY

	// Draw top line
	/*
		if !gfx.ThickLineColor(renderer, int32(topLeftPointX), int32(topLeftPointY),
			int32(topRightPointX), int32(topRightPointY),
			int32(lineWidth), color) {
			panic("error")
		}
	*/
	DrawHLineWidth(topLeftPointX, topLeftPointY, w, color, lineWidth)
	// Draw bottom line
	/*
		if !gfx.ThickLineColor(renderer, int32(bottomLeftPointX), int32(bottomLeftPointY),
			int32(bottomRightPointX), int32(bottomRightPointY),
			int32(lineWidth), color) {
			panic("error")
		}
	*/
	DrawHLineWidth(bottomLeftPointX, bottomLeftPointY, w, color, lineWidth)
	// Draw left line
	/*
		if !gfx.ThickLineColor(renderer, int32(topLeftPointX), int32(topLeftPointY),
			int32(bottomLeftPointX), int32(bottomLeftPointY),
			int32(lineWidth), color) {
			panic("error")
		}
	*/
	DrawVLineWidth(topLeftPointX, topLeftPointY, h, color, lineWidth)
	// Draw right line
	/*
		if !gfx.ThickLineColor(renderer, int32(topRightPointX), int32(topRightPointY),
			int32(bottomRightPointX), int32(bottomRightPointY),
			int32(lineWidth), color) {
			panic("error")
		}
	*/
	DrawVLineWidth(topRightPointX, topRightPointY, h, color, lineWidth)

	// Debug
	if false {
		DrawCross(topLeftPointX, topLeftPointY, 20, ColorRed)
		DrawCross(topRightPointX, topRightPointY, 20, ColorRed)
	}
}

/*
func drawLineWidth(x1, y1, x2, y2 int, c sdl.Color, width int) {
	if width < 1 {
		pp("bad width:", width)
	}

	if x1 == x2 || y1 == y2 {
		pp("bad params")
	}


}
*/

func DrawHLineWidth(x, y, l int, c sdl.Color, width int) {
	shY := 0
	if width > 1 {
		shY = -(width / 2)
	}
	py := y
	for i := 0; i < width; i++ {
		DrawHLine(x, py+shY, l, c)
		py++
	}
}

func DrawVLineWidth(x, y, l int, c sdl.Color, width int) {
	shX := 0
	if width > 1 {
		shX = -(width / 2)
	}
	px := x
	for i := 0; i < width; i++ {
		DrawVLine(px+shX, y, l, c)
		px++
	}
}

func DrawLine(x1, y1, x2, y2 int, c sdl.Color) {
	renderer.SetDrawColor(uint8(c.R), uint8(c.G), uint8(c.B), uint8(c.A))
	renderer.DrawLine(int32(x1), int32(y1), int32(x2), int32(y2))
}

func DrawHLine(x, y, l int, c sdl.Color) {
	DrawLine(x, y, x+l, y, c)
}

func DrawVLine(x, y, l int, c sdl.Color) {
	DrawLine(x, y, x, y+l, c)
}

// XXX for debug use
func DrawArrow(x1, y1, x2, y2 int, c sdl.Color) {
	DrawLine(x1, y1, x2, y2, c)

	p1 := Vec2{float64(x1), float64(y1)}
	p2 := Vec2{float64(x2), float64(y2)}
	_ = p1
	_ = p2

	width := 5.0

	cPt := p2
	cPt = cPt.MoveTowards(p1, width*4)

	lineVec := p2.Sub(p1)
	dirVec := lineVec.Norm()

	//pp(p1, p2, lineVec)
	//normalVec := Vec2{-lineVec.Y(), lineVec.X()}
	normalVec := Vec2{-dirVec.Y(), dirVec.X()}
	//lPt := cPt.Add(normalVec)
	lPt := cPt.Add(normalVec.MulScalar(-width))
	rPt := cPt.Add(normalVec.MulScalar(width))

	//drawLine(int(p2.X()), int(p2.Y()), int(cPt.X()), int(cPt.Y()), ColorRed)

	//drawLine(int(cPt.X()), int(cPt.Y()), int(lPt.X()), int(lPt.Y()), ColorGreen)
	//drawLine(int(cPt.X()), int(cPt.Y()), int(rPt.X()), int(rPt.Y()), ColorBlue)
	DrawLine(int(p2.X()), int(p2.Y()), int(lPt.X()), int(lPt.Y()), c)
	DrawLine(int(p2.X()), int(p2.Y()), int(rPt.X()), int(rPt.Y()), c)
}

func DrawCross(x, y, size int, c sdl.Color) {
	DrawHLine(x-size/2, y, size, c)
	DrawVLine(x, y-size/2, size, c)
}

func DrawFillRect(x, y, w, h int, c sdl.Color) {
	/*
		var q sdl.BlendMode
		p(renderer.GetDrawBlendMode(&q))
		p(q)
		pp(c)
	*/
	err := renderer.SetDrawBlendMode(sdl.BLENDMODE_BLEND)
	if err != nil {
		panic(err)
	}

	renderer.SetDrawColor(uint8(c.R), uint8(c.G), uint8(c.B), uint8(c.A))
	var rect sdl.Rect
	rect.X = int32(x)
	rect.Y = int32(y)
	rect.W = int32(w)
	rect.H = int32(h)
	renderer.FillRect(&rect)

	err = renderer.SetDrawBlendMode(sdl.BLENDMODE_NONE)
	if err != nil {
		panic(err)
	}
}
