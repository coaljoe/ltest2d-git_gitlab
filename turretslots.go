package main_module

// component
type TurretSlots struct {
	//turrets  []*Turret
	// Slots/Objects/Elements
	turrets map[string]*Turret // Read-only?
}

func newTurretSlots() *TurretSlots {
	ts := &TurretSlots{
		//turrets: make([]*Turret, 0),
		turrets: make(map[string]*Turret),
	}

	return ts
}

func (ts *TurretSlots) start() {}

func (ts *TurretSlots) hasSlot(slotName string) bool {
	return ts.turrets[slotName] != nil
}

func (ts *TurretSlots) addTurret(t *Turret, slotName string) {
	if ts.hasSlot(slotName) {
		pp("already have turret with such slotName:", slotName)
	}

	ts.turrets[slotName] = t
	// Set slotName record for turret
	t.slotName = slotName
}

func (ts *TurretSlots) getTurretById(id int) *Turret {
	panic("not implemented")
}

func (ts *TurretSlots) getTurretBySlotName(slotName string) *Turret {
	//panic("not implemented")
	if !ts.hasSlot(slotName) {
		pp("no such slotName:", slotName)
	}
	return ts.turrets[slotName]
}

func (ts *TurretSlots) update(dt float64) {
	//_log.Dbg("%F")

	for _, t := range ts.turrets {
		//pdump(t)
		//dump(t)
		t.update(dt)
	}
}
