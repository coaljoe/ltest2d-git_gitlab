package main_module

// component
type Tank struct {
	//mt *Turret
}

func newTank() *Tank {
	t := &Tank{}
	//t.mt = en.GetComponentTag(&Turret{}, "main").(*Turret)
	return t
}

func (t *Tank) start() {}

func (t *Tank) destroy() {
	println("Tank.Destroy")
	//t.mt = nil
}

func (t *Tank) update(dt float64) {
	//p("Tank.Update")
	//mt := tc.GetComponentTag(&Turret{}, "main").(*Turret)
}
