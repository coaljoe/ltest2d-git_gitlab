package main_module

type FieldLayerView struct {
	m *FieldLayer
}

func newFieldLayerView(m *FieldLayer) *FieldLayerView {
	v := &FieldLayerView{
		m: m,
	}
	return v
}
