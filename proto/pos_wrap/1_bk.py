from glm import *

"""
rst.x = pos.x-obj.x;
rst.y = pos.y-obj.y;

if (abs(rst.x) > sizeX / 2) {
if (rst.x > 0) {
rst.x = pos.x + sizeX;
} else {
rst.x = pos.x - sizeX;
}
} else {
rst.x = pos.x;
}
if (abs(rst.y) > sizeY / 2) {
if (rst.y > 0) {
rst.y = pos.y + sizeY;
} else {
rst.y = pos.y - sizeY;
}
} else {
rst.y = pos.y;
}
"""

#obj = vec2(4, 4)
#pos = vec2(24, 8)
#obj = vec2(24, 8)
#pos = vec2(4, 4)

obj = vec2(4, 4)
pos = vec2(24, 4)
want = vec2(-8, 4)

rst = pos - obj 
sizeX = 32
sizeY = 32

print("rst before:", rst)

if abs(rst.x) > sizeX / 2:
    if rst.x > 0:
        rst.x = pos.x + sizeX
    else:
        rst.x = pos.x - sizeX

if abs(rst.y) > sizeY / 2:
    if rst.y > 0:
        rst.y = pos.y + sizeY
    else:
        rst.y = pos.y - sizeY

print("obj:", obj)
print("pos:", pos)
print("rst:", rst)

res = obj+rst
print()
print("want:", want)
print("test:", res)
print("test:", res.x % sizeX, res.y % sizeY)
