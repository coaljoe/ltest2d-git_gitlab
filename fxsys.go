package main_module

type FxSys struct {
	*GameSystem
}

func newFxSys() *FxSys {
	s := &FxSys{}
	s.GameSystem = newGameSystem("FxSys", "Fx system", s)
	return s
}

func (s *FxSys) update(dt float64) {
	// Ship blinking
}
