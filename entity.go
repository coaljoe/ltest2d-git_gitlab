package main_module

type ComponentI interface {
	start()
}

type EntityI interface {
	//SpawnableI
	DestroyableI
	hasView() bool
	getView() ViewI
	getId() int
	isDead() bool
	getComponents() []ComponentI
	start()
	update(dt float64)
}

func SpawnEntity(e EntityI) {
	game.systems["EntitySys"].addElem(e)
	//e.spawn()
	e.start()
	if e.hasView() {
		e.getView().spawn()
	}
}

func StartEntity(e EntityI) {
	for _, c := range e.getComponents() {
		c.start()
	}
}

func DestroyEntity(e EntityI) {
	//p("YYY destroyEntity id:", e.getId())
	game.systems["EntitySys"].removeElem(e)
	e.destroy()
	if e.hasView() {
		e.getView().destroy()
	}
}

func tryDestroyEntity(e EntityI) {
	if game.systems["EntitySys"].hasElem(e) {
		game.systems["EntitySys"].removeElem(e)
		e.destroy()
		if e.hasView() {
			e.getView().destroy()
		}
	}
}

func UpdateEntity(e EntityI, dt float64) {
	//p("XXX updateEntity id:", e.getId())
	e.update(dt)
	if e.hasView() {
		e.getView().update(dt)
	}
}
