package main_module

type UnitSys struct {
	*GameSystem
}

func newUnitSys() *UnitSys {
	s := &UnitSys{}
	s.GameSystem = newGameSystem("UnitSys", "Unit system", s)
	return s
}

func (s *UnitSys) getUnitById(id int) *Unit {
	for _, el := range s.getElems() {
		u := el.(*Unit)
		if u.id == id {
			return u
		}
	}
	//return nil
	pp("error: unit not found; id:", id)
	return nil
}

func (s *UnitSys) getUnitByPosCheck(cx, cy int) *Unit {
	for _, el := range s.getElems() {
		u := el.(*Unit)
		if u.cx == cx && u.cy == cy {
			return u
		}
	}
	return nil
}

func (s *UnitSys) getUnitByPos(cx, cy int) *Unit {
	u := s.getUnitByPosCheck(cx, cy)
	if u == nil {
		pp("error: unit with pos not found; cx, cy:", cx, cy)
	}
	return nil // Never returns
}

func (s *UnitSys) isUnitVisibleForPlayer(u *Unit, p *Player) bool {
	return true
}

func (s *UnitSys) getUnits() []*Unit {
	ret := make([]*Unit, 0)
	for _, el := range s.getElems() {
		u := el.(*Unit)
		ret = append(ret, u)
	}
	return ret
}

func (s *UnitSys) getUnitsForPlayer(p *Player) []*Unit {
	ret := make([]*Unit, 0)
	for _, el := range s.getElems() {
		u := el.(*Unit)
		if u.player == p {
			ret = append(ret, u)
		}
	}
	return ret
}

func (s *UnitSys) update(dt float64) {
	for _, ui := range s.elems {
		u := ui.(*Unit)
		u.update(dt)
	}
}
