package main_module

import (
	"path/filepath"
)

type ProdSys struct {
	prodTypes      map[ProdTypeKey]*ProdType
	availProdTypes map[int][]*ProdType
}

func newProdSys() *ProdSys {
	s := &ProdSys{
		prodTypes:      make(map[ProdTypeKey]*ProdType),
		availProdTypes: make(map[int][]*ProdType),
	}

	// Init prodTypes
	s.loadProdTypes()

	//pdump(s.prodTypes)

	return s
}

func (s *ProdSys) loadProdTypes() {
	// XXX FIXME: glob doesn't support '**' pattern
	files, err := filepath.Glob("res/data/prod_types/*/*.hjson")
	if err != nil {
		pp(err)
	}

	if len(files) == 0 {
		pp("error: no prodType definition files")
	}

	for _, file := range files {
		pt := newProdType()
		pt.load(file)

		k := ProdTypeKey{pt.Name, pt.CampId}
		s.prodTypes[k] = pt
	}
}

func (s *ProdSys) GetAvailProdTypes(p *Player) []*ProdType {
	return s.availProdTypes[p.id]
}

func (s *ProdSys) addAvailProdType(p *Player, ptName string) {
	campId := p.camp.Id
	k := ProdTypeKey{ptName, campId}
	pt := s.prodTypes[k]
	if pt == nil {
		pp("error: ProdTypeKey not found; k:", k)
	}
	s.availProdTypes[p.id] = append(s.availProdTypes[p.id], pt)
}

func (s *ProdSys) haveAvailProdType(p *Player, ptName string) bool {
	campId := p.camp.Id
	k := ProdTypeKey{ptName, campId}
	pt := s.prodTypes[k]
	return pt != nil
}

func (s *ProdSys) GetAvailProdType(p *Player, ptName string) *ProdType {
	if !s.haveAvailProdType(p, ptName) {
		pp("don't have such prodtype; ptName:", ptName)
	}
	campId := p.camp.Id
	k := ProdTypeKey{ptName, campId}
	pt := s.prodTypes[k]
	return pt
}

func (s *ProdSys) CanProduce(p *Player, ptName string) bool {
	if s.haveAvailProdType(p, ptName) {
		return true
	}
	return false
}
