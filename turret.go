package main_module

type Turret struct {
	unit *Unit // link
	slotName string // Read-only?
	name string
	guiName string
	rotSpeed float64
	rotAcc float64
	reloadTime float64
	//fsm              *Fsm
	dir              *Dir
	targetRotAngle      float64
	// Return system
	returnEnabled    bool    // Auto-return enabled
	returnDelay      float64 // Delay before return turret back
	returnSpeedCoeff float64 // Speed coeff.
	returnLock       bool    // Lock
	/* state */
	curRotAcc float64
  
	// ?
	//view ViewI
}

func newTurret(u *Unit) *Turret {
  t := &Turret{
    unit: u,
		reloadTime:       10.0,
		rotSpeed:         10.0,
		rotAcc:           1.00,
		dir:              newDir(),
		targetRotAngle:      -1,
		returnEnabled:    true,
		returnDelay:      4.0,
		returnSpeedCoeff: 0.6,
		//fsm:              NewFsm(idle, transitions),
  }
  
  return t
}

func (t *Turret) rotAngle() float64 {
	return t.dir.angle()
}

// -1: angle not set (?) now nil = unset?
func (t *Turret) rotateTo(v float64) {
	t.rotateToSpeedCoeff(v, 1)
}

func (t *Turret) rotateToSpeedCoeff(v float64, speedCoeff float64) {
	t.dir.rotTo(v, t.rotSpeed*speedCoeff)
}

func (t *Turret) update(dt float64) {
	//_log.Dbg("%F")

	t.dir.update(dt)
}
