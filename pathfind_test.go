package main_module

import (
	"fmt"
	"testing"
)

func TestPathfind(t *testing.T) {

	//InitGame()
	//InitAll()
	//initTest()

	//initTest()

	pf := game.pathfinder

	if false {
		p("test_bar_block...")
		//game.field.loadImage("tmp/maps/test2/gray_fill.png")
		game.field.loadImage("tmp/maps/test2/test_bar_block.png")
	
		p := pf.pathfind(CPos{2, 0}, CPos{4, 4})
		fmt.Println("Path:", p)
	}

	if false {
		p("test_bar_block...")
		//game.field.loadImage("tmp/maps/test2/gray_fill.png")
		game.field.loadImage("tmp/maps/test2/test_bar_block2.png")
		
		p := pf.pathfind(CPos{8, 6}, CPos{8, 10})
		fmt.Println("Path:", p)
	}

	if true {
		//p := pf.pathfind(CPos{3, 3}, CPos{10, 10})
		//p := pf.pathfind_ex(CPos{3, 3}, CPos{10, 10}, true)

		game.field.loadImage("tmp/maps/test2/gray_fill.png")
		//p := pf.pathfind_ex(CPos{2, 0}, CPos{4, 0}, true)
		//p := pf.pathfind(CPos{0, 0}, CPos{6, 0})
		p := pf.pathfind(CPos{2, 0}, CPos{4, 0})
		fmt.Println("Path:", p)
	}

	if false {
		p := pf.pathfind(CPos{0, 0}, CPos{1, 0})
		fmt.Println("Path:", p)
	}

	if false {

		p2 := pf.pathfind(CPos{0, 0}, CPos{5, 0})
		fmt.Println("Path:", p2)

		p3 := pf.pathfind(CPos{1, 1}, CPos{3, 3})
		fmt.Println("Path:", p3)
	}

	if false {

		//pp(game.field.h)
		//p4 := pathfind(CPos{0, 0}, CPos{-5, 0})
		p4 := pf.pathfind(CPos{0, 0}, CPos{game.field.h - 5, 0})
		fmt.Println("Path:", p4)

	}
}
