//module kristallos.ga/ltest2d
module kristallos.ga/ltest2d/main_module

go 1.22.3

replace kristallos.ga/ltest2d/gui => ./gui

require kristallos.ga/ltest2d/gui/gui_base v0.0.0-replace

replace kristallos.ga/ltest2d/gui/gui_base => ./gui/gui_base

//replace kristallos.ga/xgui => ../xgui_upstream1_test1

require (
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/bradfitz/iter v0.0.0-20191230175014-e8f45d346db8
	github.com/davecgh/go-spew v1.1.1
	github.com/drbig/perlin v0.0.0-20141206142450-e4c467936143
	github.com/emirpasic/gods v1.12.0
	github.com/hjson/hjson-go v3.0.1+incompatible
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/looplab/fsm v0.1.0
	github.com/ojrac/opensimplex-go v1.0.1
	github.com/stretchr/testify v1.3.0
	github.com/veandco/go-sdl2 v0.4.39
	kristallos.ga/lib/debug v0.0.0-20231206203330-ec813b10aad7
	kristallos.ga/lib/ider v0.0.0-20231206203330-ec813b10aad7
	kristallos.ga/lib/pubsub v0.0.0-20231206203330-ec813b10aad7
	kristallos.ga/lib/sr v0.0.0-20231206203330-ec813b10aad7
	kristallos.ga/lib/xlog v0.0.0-20231206203330-ec813b10aad7
	kristallos.ga/rx/math v0.0.0-20240115160632-7763db890ad7
	kristallos.ga/rx/transform v0.0.0-20240115160632-7763db890ad7
	kristallos.ga/xgui v0.0.0-20240806082116-a6d830fde05b
)

require github.com/peterbourgon/mergemap v0.0.1

require (
	github.com/WastedCode/serializer v0.0.0-20150605061548-b76508a5f9d4 // indirect
	github.com/fossoreslp/go-uuid-v4 v1.0.0 // indirect
	github.com/iancoleman/orderedmap v0.3.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)

//replace kristallos.ga/rx => ../rx_orig
//replace kristallos.ga/rx => ../go/src/rx
//replace kristallos.ga/xgui => ../xgui_upstream
//replace kristallos.ga/xgui => ../xgui_style_ref

//replace kristallos.ga/lib/debug => ../lib/debug
//replace kristallos.ga/lib/xlog => ../lib/xlog

// bb_local
// replace bitbucket.org/coaljoe/lib => ../bb_local/lib
// replace bitbucket.org/coaljoe/lib/debug => ../bb_local/lib/debug
// replace bitbucket.org/coaljoe/lib/ider => ../bb_local/lib/ider
// replace bitbucket.org/coaljoe/lib/sr => ../bb_local/lib/sr
// replace bitbucket.org/coaljoe/rx/math => ../bb_local/rx/math
// replace bitbucket.org/coaljoe/rx/transform => ../bb_local/rx/transform
// replace bitbucket.org/coaljoe/xgui => ../bb_local/xgui
