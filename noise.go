// XXX not used
package main_module

import (
	"image"
	"image/color" //"image/draw"
	"image/png"
	"math" //"math/rand"
	"math/rand"
	"os"
)

func makeNoise(w, h int) []float64 {
	// Python code from here:
	// https://gamedev.stackexchange.com/questions/23625/how-do-you-generate-tileable-perlin-noise
	var _ = `
	perm = range(256)
	random.shuffle(perm)
	perm += perm
	dirs = [(math.cos(a * 2.0 * math.pi / 256),
    	     math.sin(a * 2.0 * math.pi / 256))
			 for a in range(256)]
	`
	// XXX should be initialized once to be deterministic?
	var perm []int
	perm = make([]int, 256)
	for i := 0; i < 256; i++ {
		perm[i] = i
	}

	// XXX non-deterministic?
	// XXX add random seed initializer like in python version
	rand.Shuffle(len(perm), func(i, j int) {
		perm[i], perm[j] = perm[j], perm[i]
	})

	perm = append(perm, perm...)
	//pp(perm)

	type DirPair struct {
		V0 float64
		V1 float64
	}

	var dirs [256]DirPair
	//dirs = make([256]DirPair, 256)
	for i := 0; i < 256; i++ {
		dp := DirPair{
			V0: math.Cos(float64(i) * 2.0 * math.Pi / 256.0),
			V1: math.Sin(float64(i) * 2.0 * math.Pi / 256.0),
		}
		dirs[i] = dp
	}

	//pp(dirs)

	var _ = `
	def noise(x, y, per):
    	def surflet(gridX, gridY):
        	distX, distY = abs(x-gridX), abs(y-gridY)
        	polyX = 1 - 6*distX**5 + 15*distX**4 - 10*distX**3
        	polyY = 1 - 6*distY**5 + 15*distY**4 - 10*distY**3
        	hashed = perm[perm[int(gridX)%per] + int(gridY)%per]
        	grad = (x-gridX)*dirs[hashed][0] + (y-gridY)*dirs[hashed][1]
        	return polyX * polyY * grad
    	intX, intY = int(x), int(y)
    	return (surflet(intX+0, intY+0) + surflet(intX+1, intY+0) +
        	    surflet(intX+0, intY+1) + surflet(intX+1, intY+1))
	`
	/*
		iabs := func(x int) int {
			if x < 0 {
				return -x
			}
			return x
		}
	*/

	noise := func(x, y float64, per int) float64 {
		surflet := func(gridX, gridY int) float64 {
			distX, distY := math.Abs(x-float64(gridX)), math.Abs(y-float64(gridY))
			polyX := 1 - 6*math.Pow(distX, 5) + 15*math.Pow(distX, 4) - 10*math.Pow(distX, 3)
			polyY := 1 - 6*math.Pow(distY, 5) + 15*math.Pow(distY, 4) - 10*math.Pow(distY, 3)
			//hashed := perm[perm[int(gridX)%per]+int(gridY)%per]
			hashed := perm[perm[pmod(int(gridX), per)]+pmod(int(gridY), per)]
			grad := (x-float64(gridX))*dirs[hashed].V0 + (y-float64(gridY))*dirs[hashed].V1
			/*
				pf("%v %T\n", polyX, polyX)
				pf("%v %T\n", polyY, polyY)
				pf("%v %T\n", hashed, hashed)
				pf("%v %T\n", grad, grad)
				pp(2)
			*/
			return polyX * polyY * grad
		}
		intX, intY := int(x), int(y)
		return (surflet(intX+0, intY+0) + surflet(intX+1, intY+0) +
			surflet(intX+0, intY+1) + surflet(intX+1, intY+1))
	}
	_ = noise
	var _ = `
	def fBm(x, y, per, octs):
    	val = 0
    	for o in range(octs):
        	val += 0.5**o * noise(x*2**o, y*2**o, per*2**o)
    	return val

	size, freq, octs, data = 128, 1/32.0, 5, []
	for y in range(size):
    	for x in range(size):
			data.append(fBm(x*freq, y*freq, int(size*freq), octs))
	`

	fBm := func(x, y float64, per int, octs int) float64 {
		val := 0.0
		for o := 0; o < octs; o++ {
			val += math.Pow(0.5, float64(o)) * noise(x*math.Pow(2, float64(o)), y*math.Pow(2, float64(o)),
				per*int(math.Pow(2, float64(o))))
			//p(val)
			//pp(val)
		}
		return val
	}
	_ = fBm

	//size, freq, octs := 128, 1/32.0, 5
	//size, freq, octs := 128, 1/64.0, 8
	freq, octs := 1/64.0, 8
	size := mini(w, h)
	//size := w * h
	_ = freq
	_ = octs
	var data []float64
	//data = make([]float64, size*size)
	data = make([]float64, 0)
	_ = data
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//v := fBm(float64(x)*freq, float64(y)*freq, int(float64(size)*freq), octs)
			//v := fBm(float64(x)*freq, float64(y)*freq, int(float64(h)*freq), octs)
			v := fBm(float64(x)*freq, float64(y)*freq, int(float64(size)*freq), octs)
			//v := 1.0
			data = append(data, v)
		}
	}

	//pp(data)
	//pp(8)
	//pp(data)

	return data
}

func saveNoise(path string, data []float64, w, h int) {
	//pp(data)
	f, err := os.Create(path)
	if err != nil {
		println("cant create file: " + path)
		panic(err)
	}
	defer f.Close()

	rect := image.Rect(0, 0, w, h)
	im := image.NewGray(rect)

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//p(hm.data[x][y], uint8(hm.data[x][y]))
			// Reconstructed value
			//rv := 128 + (data[x][y] * hm.levelStep)
			// Normalize to 0, 255 range
			//v := data[x][y]
			idx := w*y + x
			p(x, y, idx, len(data))
			v := data[w*y+x]
			//v = data[100]
			//v = data[len(data)-1]
			rv := int(((v - -1.0) / (1.0 - -1.0)) * 255)
			p(v, rv)
			c := color.Gray{Y: uint8(rv)}
			im.SetGray(x, y, c)

			/*
				if idx > 128 {
					pp(2)
				}
			*/
		}
	}

	png.Encode(f, im)
	p("noise saved to:", path)
}
