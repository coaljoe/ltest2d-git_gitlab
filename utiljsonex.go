// Json utilities
package main_module

import (
//	"bytes"
	"fmt"
	"os"
	"strings"
	"regexp"

	"github.com/peterbourgon/mergemap"
)

// XXX ?
func xremoveComments(s string) string {
	reg, err := regexp.Compile("//.*?\n")
	if err != nil {
		panic(err)
	}
	ret := reg.ReplaceAllString(s, "\n")
	//fmt.Println(ret)

	return ret
}

// TODO: support more than one include ?
func xloadJsonFromFile(path string) map[string]interface{} {
	fmt.Println("xloadJsonFromFile path:", path)
	
	data, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}

	dataS := string(data)
	dataS = xremoveComments(dataS)

	var ret map[string]interface{}

	includes := make([]string, 0)
	haveIncludes := false
	
	// Parse includes
	if true {
	
		lines := strings.Split(dataS, "\n")
		for _, line := range lines {
			fmt.Println("-> line:", line)
			
			if strings.HasPrefix(line, "#include") {
				v := strings.Split(line, " ")[1]
				v = strings.ReplaceAll(v, "\"", "")
				fmt.Println("v:", v)
				includes = append(includes, v)

				haveIncludes = true
				//pp(3)
			}
		}

		if haveIncludes {
			fmt.Println("includes:")
			fmt.Println(includes)
		}
	}

	// XXX return
	if !haveIncludes {
		ret = unmarshalHjsonFromData(data)
		return ret
	}

	path2 := includes[0]

	data2, err := os.ReadFile(path2)
	if err != nil {
		panic(err)
	}

	xdata := dataS
	xdata2 := string(data2)

	xdata = xremoveComments(xdata)
	xdata2 = xremoveComments(xdata2)

	removeIncludes := func(s string) string {
		ret := ""

		for _, x := range strings.Split(s, "\n") {
			if strings.HasPrefix(x, "#include") {
				continue
			}
			ret += x + "\n"
		}
		
		return ret
	}
	_ = removeIncludes

	xdata = removeIncludes(xdata)
	xdata2 = removeIncludes(xdata2)

	fmt.Println("xdata:")
	fmt.Println(xdata)
	fmt.Println("xdata2:")
	fmt.Println(xdata2)

	xdatajs := unmarshalHjsonFromData([]byte(xdata))
	xdata2js := unmarshalHjsonFromData([]byte(data2))
	

	

	// Result map ?
	//resMap := mergemap.Merge(m1, m2)
	resMap := mergemap.Merge(xdatajs, xdata2js)

	fmt.Println()
	fmt.Println("resMap:")
	fmt.Println(resMap)
	fmt.Println()
	dump(resMap)

	resultStr := ""

	resultStr = string(marshalHjson(resMap))

	fmt.Println()
	fmt.Println("resultStr:")
	fmt.Println(resultStr)

	//pp(2)
	
	//ret = unmarshalHjsonFromData(data)
	ret = resMap
	
	//pp(2)
	
	return ret
}
