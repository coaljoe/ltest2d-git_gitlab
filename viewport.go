package main_module

import (
	"math"

	"github.com/veandco/go-sdl2/sdl"
)

type Viewport struct {
	x, y       float64 // Internal scrolling value in floating point
	shx, shy   int     // Viewport's shift in pixels
	w, h       int     // Viewport width and height in pixels
	mcx, mcy   int     // Mouse cell x,y
	overflowX  int
	overflowY  int
	underflowX int
	underflowY int
}
func (v *Viewport) X() float64 { return v.x }
func (v *Viewport) Y() float64 { return v.y }
func (v *Viewport) W() int { return v.w }
func (v *Viewport) H() int { return v.h }
func (v *Viewport) OverflowX() int { return v.overflowX }
func (v *Viewport) OverflowY() int { return v.overflowY }

func newViewport() *Viewport {
	v := &Viewport{
		w: vars.resX,
		h: vars.resY,
	}
	return v
}

func (v *Viewport) SetPos(x, y float64) {
	v.x = x
	v.y = y
}

func (v *Viewport) update(dt float64) {
	sdl.PumpEvents()
	//mx, my, _ := sdl.GetMouseState()
	mx, my := vars.mx, vars.my
	win := sdl.GetMouseFocus()
	hasFocus := false
	if win != nil {
		hasFocus = true
	}
	hasFocus = true
	//p(">>>", mx, my, win)

	vx := 0.0
	vy := 0.0

	state := sdl.GetKeyboardState()
	scrollAmt := vars.scrollSpeed * dt

	if vars.dev {
		//pp(2)
		key1 := game.keymap.getKey1("scrollFastKey")
		key2 := game.keymap.getAltKey1("scrollFastKey")
		skey1 := sdl.GetScancodeFromKey(key1)
		skey2 := sdl.GetScancodeFromKey(key2)
		if state[skey1] > 0 || state[skey2] > 0 {
			scrollAmt = vars.scrollSpeedFast * dt
		}
	}

	key := game.keymap.getKey1("scrollUpKey")
	skey := sdl.GetScancodeFromKey(key)
	if state[skey] > 0 || hasFocus && my >= 0 && int(my) < vars.scrollPadding {
		//pp(2)
		//game.vp.shx += int(vars.scrollSpeed * dt)
		//v.y += scrollAmt
		vy = -scrollAmt
		//p(game.vp.shx)
	}

	key = game.keymap.getKey1("scrollDownKey")
	skey = sdl.GetScancodeFromKey(key)
	if state[skey] > 0 || hasFocus && my >= 0 && int(my) > vars.resY-vars.scrollPadding {
		//v.y -= scrollAmt
		vy = scrollAmt
	}

	key = game.keymap.getKey1("scrollLeftKey")
	skey = sdl.GetScancodeFromKey(key)
	if state[skey] > 0 || hasFocus && mx >= 0 && int(mx) < vars.scrollPadding {
		//v.x += scrollAmt
		vx = -scrollAmt
	}

	key = game.keymap.getKey1("scrollRightKey")
	skey = sdl.GetScancodeFromKey(key)
	if state[skey] > 0 || hasFocus && mx >= 0 && int(mx) > vars.resX-vars.scrollPadding {
		//v.x -= scrollAmt
		vx = scrollAmt
	}

	// Hack to make diagonal movement less speedy and more uniform
	// Not optimized
	if vx != 0.0 && vy != 0.0 {
		v := math.Sqrt(2) / 2.0 // 0.707... (half-diagonal)
		//p(vx, vy)
		vx *= v
		vy *= v
		//p(vx, vy)
		//pp(2)
	}

	// Update viewport vars
	v.x += vx
	v.y += vy

	if vars.wrapField {
		// Wrap X
		//p(">>>", v.shx, game.field.w*cell_size)
		if int(math.Round(v.x)) > game.field.w*cell_size {
			//v.shx = 0
			v.x = 0
		}
		/*
			if int(math.Round(v.x)) < -v.w {
				v.x = float64(game.field.w*cell_size - v.w)
			}
		*/
		if int(math.Round(v.x)) < 0 {
			v.x = float64(game.field.w*cell_size) + v.x
		}

		// Wrap Y
		if int(math.Round(v.y)) > game.field.h*cell_size {
			v.y = 0
		}

		if int(math.Round(v.y)) < 0 {
			v.y = float64(game.field.h*cell_size) + v.y
		}
	}

	// Update shift
	v.shx = int(math.Round(v.x))
	v.shy = int(math.Round(v.y))

	if vars.wrapField {
		v.overflowX = maxi((v.shx+v.w)-game.field.w*cell_size, 0)
		v.overflowY = maxi((v.shy+v.h)-game.field.h*cell_size, 0)

		v.underflowX = v.w - v.overflowX
		v.underflowY = v.h - v.overflowY
		/*
			if v.shx > 0 {
				v.overflowX = (v.shx + v.w) - game.field.w*cell_size
			} else {
				v.overflowX = 0
			}
		*/

		/*
			if v.shx >= 0 || v.shx < -v.w {
				v.underflowX = 0
			} else {
				v.underflowX = v.shx
			}
		*/
		/*
			if v.overflowX < 0 {
				v.underflowX = -v.overflowX
			} else {
				v.underflowX = 0
			}
		*/
		//p("ZZZ x", v.overflowX, v.underflowX, v.shx)
		//p("ZZZ y>>>", v.overflowY, v.shy)
		//pp(2)
	}

	//p(">>>", v.shx, v.shy)

	// Update mcx, mcy
	// XXX duplicate of hud.cx/cy?
	if game.field.generated {
		/*
		   v.mcx = wrapCoordX((v.shx + vars.mx) / cell_size)
		   v.mcy = wrapCoordY((v.shy + vars.my) / cell_size)
		*/
		v.mcx = game.hud.mcx
		v.mcy = game.hud.mcy
	}
}
