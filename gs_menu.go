package main_module

import ()

type MenuGameState struct {
	*GameState
}

func newMenuGameState() *MenuGameState {
	gs := &MenuGameState{
		GameState: newGameState("menu"),
	}
	return gs
}

func (gs *MenuGameState) start() {
	/*
		//game.createNewDefaultGame()
		// XXX fixme
		if !game.gameCreated {
			game.start()
		}
	*/

	//game.gui.enabled = false
	game.gui.SetEnabled(true)
	game.hud.enabled = false

}

func (gs *MenuGameState) stop() {
	//game.finishLevel()
	//game.stop()
}

func (gs *MenuGameState) draw() {
	//game.field.view.draw()
}

func (gs *MenuGameState) postDraw() {
}

func (gs *MenuGameState) update(dt float64) {
	//game.field.update(dt)
}
