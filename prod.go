package main_module

type ProdTypeKey struct {
	name   string
	campId CampId
}

// Production type info
type ProdType struct {
	Name      string
	CampId    CampId
	GuiName   string
	GuiIcon   string // XXX calculate automatically?
	CatName   string
	CostMoney int
	CostPop   int
	CostFuel  int
	CostMetal int
	BuildTime float64
	Available bool // Multiple players conflict (same camp type)? change to availProdTypes per player?
}

func newProdType() *ProdType {
	pt := &ProdType{}
	return pt
}

func (pt *ProdType) load(path string) {
	_log.Inf("%F")

	_log.Dbg("loading prodtype from", path)
	dat := unmarshalHjsonFromFile(path)
	pt.Name = dat["name"].(string)
	pt.CampId.setFromString(dat["campId"].(string))
	pt.GuiName = dat["guiName"].(string)
	pt.CatName = dat["catName"].(string)
	//pt.costMoney = dat["costMoney"]
	if v := dat["costMoney"]; v != nil {
		pt.CostMoney = int(v.(float64))
	}
	if v := dat["costPop"]; v != nil {
		pt.CostPop = int(v.(float64))
	}
	if v := dat["costFuel"]; v != nil {
		pt.CostFuel = int(v.(float64))
	}
	if v := dat["costMetal"]; v != nil {
		pt.CostMetal = int(v.(float64))
	}
	pt.BuildTime = dat["buildTime"].(float64)
}
