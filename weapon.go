package main_module

import (
	"github.com/emirpasic/gods/sets/linkedhashset"
	. "kristallos.ga/rx/math"
)

// TODO: test fire in bursts
// bursts size/length = burst time * fireRate
//
// ex:
// fireRate = 600 rpm
// burstTime = 0.5 sec
// burstSize = (600 / 60) * 0.5 = 5 rounds/shots per burst/fire

type WeaponTargetI interface {
	CombatHostI
}

// Компонент итема отвечающий за ф-ции оружия.
// component
type Weapon struct {
	// link to host cc
	combat *Combat
	target        WeaponTargetI
	// Props
	name          string
	rounds        int
	maxRounds     int
	fireRange     float64 // m
	fireRate      float64 // shots/min
	reloadTime    float64 // s
	shotName      string
	shotSpeed     float64
	shotSound     string
	maintanceCost int
	attackDelay   float64
	// TODO: add controlType: USER | AUTO
	// user: control over weapon and its targets
	// auto: no control / the weapon is automatic
	//controlType   WeapontControlType_Auto
	// The weapont is automatically controlled (no user control)
	autoControl   bool
	//ammo         *Resource
	ammoType     *AmmoType
	hostRealms   *linkedhashset.Set
	targetRealms *linkedhashset.Set
	//
	tag          string
	fireCounter    int
	//// State
	firing            bool
	// Time since last fire
	lastFireTimer *Timer
}

func newWeapon(combat *Combat) *Weapon {
	w := &Weapon{
		combat: combat,
		//item:              it,
		rounds:        0,
		maxRounds:     10,
		fireRange:     10,
		fireRate:      50,
		reloadTime:    1,
		shotName:      "shot",
		shotSpeed:     100,
		shotSound:     "res/audio/shot2a.wav",
		fireCounter:    0,
		maintanceCost: 0,
		attackDelay:   0,
		//ammo:      newResource(ResourceType_Ammo),
		ammoType: newAmmoType(),
		//hostRealms:        RealmType_Ground,
		//targetRealms:      RealmType_Ground,
		// XXX fixme: use bitmask values: hostRealmsMask, targetRealmsMask
		hostRealms:        linkedhashset.New(RealmType_Ground),
		targetRealms:      linkedhashset.New(RealmType_Ground),
		//fireIntervalTimer: newTimer(false),
		lastFireTimer: newTimer(false),
	}
	w.reload()
	
	return w
}

func (w *Weapon) hasTarget() bool {
	return w.target != nil
}

func (w *Weapon) setTarget(t WeaponTargetI) bool {

	//if !w.canAttack(t) {
	if !w.canAttackRealm(t.getRealm()) {
		println("err: target's weapon cannot attack this target")
		return false
	}
	w.target = t
	//wh.TtTargeter.setTarget(t)
	//wh.targetLocker.setTarget(t)
	p("target was set")
	
	return true
}

func (w *Weapon) unsetTarget() bool {
	//w.TtTargeter.unsetTarget()
	p("target was unset")
	
	return true
}


func (w *Weapon) isTargetLocked() bool {
	return true
}

func (w *Weapon) isTargetInRadius() bool {
	return true
}

func (w *Weapon) ready() bool {
	return true
}

func (w *Weapon) setTargetRealms(realms ...RealmType) {
	//_log.Dbg("%F", realms)
	
	w.targetRealms.Clear()
	for _, realm := range realms {
		w.targetRealms.Add(realm)
	}
}

// Check if tRealm is in weapon's target realms
func (w *Weapon) canAttackRealm(realm RealmType) bool {
	//_log.Dbg("%F", realm)
	return w.targetRealms.Contains(realm)
}

func (w *Weapon) fire() {
	_log.Dbg("weapon.fire")

	var _ = `
	// make bullet
	view := w.cc.host.getView()
	//pf("XXX: %v\n", view.(*EnemyView))
	if view == nil {
		panic("no view")
	}
	//fpNode := view.getNode("firepointNode")
	fpNode := view.getNode("body")
	//fpNode := view.getNode("fire_point")
	//pf("YYY: %v\n", fpNode)
	//pf("YYY: %#v\n", fpNode)
	//srcPos := fpNode.WorldPos()
	srcPos := fpNode.Pos()
	//p("XXX ", srcPos)
	//p := w.target.Pos2() // fixme
	p := srcPos
	//p.Y += 30
	//p.Y = 30
	//p.SetY(30)
	byEnemy := false
	if w.cc.host.getObjType() == "enemy" {
		byEnemy = true
	}
	var dstPos Vec3
	if !byEnemy {
		p.SetY(gameWindowH)
		dstPos = Vec3{p.X(), p.Y(), p.Z()}
		//srcPos = srcPos.Add(Vec3{0, 3.5, 0}) // Adjust fire point
		srcPos = srcPos.Add(Vec3{0, 5.0, -1.0}) // Adjust fire point
	} else {
		dstPos = Vec3{p.X(), -1, p.Z()}
		srcPos = srcPos.Add(Vec3{0, -3.5, 0}) // Adjust fire point

	}
	//_log.Dbg("shot from:", srcPos, "to:", dstPos, "speed:", w.shotSpeed)
	_log.Inf("shot from:", srcPos, "to:", dstPos, "speed:", w.shotSpeed)

	pr := newShot(w.shotName, srcPos, dstPos, w.shotSpeed, byEnemy)
	spawnEntity(pr)
	if byEnemy {
		// Rotate the shot
		pr.position.dir.setAngle(-90)
	}
	//game.entitysys.listEntities()
	//pp(2)

	// Play sound
	//playSound("res/audio/test.wav")
	playSound(w.shotSound, vol_sfx)
	/*
		if w.shotName == "shot" {
			//playSound("res/audio/slimeball.wav", vol_sfx)
			playSound("res/audio/shot1.wav", vol_sfx)
		} else if w.shotName == "shot2" {
			playSound("res/audio/iceball.wav", vol_sfx)
		} else {
			pp("unknown shotName sound")
		}
	*/
	`
	if w.rounds == 0 {
		if !w.reload() {
			_log.Dbg("no bullets")
			return
		}
	}

	// Emit shot
	//srcPos := w.host.pos2()
	//srcPos := w.combat.host.pos2()
	//dstPos := w.target.pos2()
	// XXX approximate?
	srcPos := Vec3{
		float64(cell_size/2 + (w.combat.host.cellPosX() * cell_size)),
		float64(cell_size/2 + (w.combat.host.cellPosY() * cell_size)), 0}
	dstPos := Vec3{
			float64(cell_size/2 + (w.target.cellPosX() * cell_size)),
			float64(cell_size/2 + (w.target.cellPosY() * cell_size)), 0}
	shot := newShot(w.shotName, srcPos, dstPos, w.shotSpeed)
	_ = shot
	SpawnEntity(shot)

	// Fire
	w.rounds -= 1
	w.fireCounter += 1

	// Set to 0
	w.lastFireTimer.restart()
}

func (w *Weapon) canFire() bool {
	if w.rounds < 0 {
		return false
	}

	//p(w.lastFireTimer.dt())
	//p(w.fireInterval())
	//pp(2)

	// Check readiness
	if (!w.lastFireTimer.zero()) && // XXX fixme?
	 	w.lastFireTimer.dt() < w.fireInterval() {
		return false
	}

	return true
}

func (w *Weapon) startFiring() {
	w.firing = true
}

func (w *Weapon) stopFiring() {
	w.firing = false
}

func (w *Weapon) reload() bool {
	_log.Dbg("weapon.reload")
	// TODO: use ammo
	w.rounds = w.maxRounds
	return true
}

// Fire interval in seconds
func (w *Weapon) fireInterval() float64 {
	return 1.0 / (w.fireRate / 60.0)
}

func (w *Weapon) hasAmmo() bool {
	return w.rounds > 0
}

func (w *Weapon) attackRating() int {
	return w.ammoType.attackRating
}

func (w *Weapon) destroy() {
	timersys().removeTimer(w.lastFireTimer)
}

func (w *Weapon) update(dt float64) {
	// Update firing
	if w.firing {
		if w.canFire() {
			w.fire()
		} else {
			if !w.hasAmmo() {
				w.reload()
			}
		}
	}
}
