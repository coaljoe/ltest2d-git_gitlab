package main_module

import (
	"fmt"
)

// ?
/*
type Encoder struct {
type DataEncoder struct {
type DataSaver struct {

}
*/

func encodeZLevelToString(zl ZLevel) string {
	switch zl {
	case ZLevel_Sea0:
		return "sea0"
	case ZLevel_Sea1:
		return "sea1"
	case ZLevel_Sea2:
		return "sea2"
	case ZLevel_Sea3:
		return "sea3"
	case ZLevel_Ground0:
		return "ground0"
	case ZLevel_Ground1:
		return "ground1"
	case ZLevel_Ground2:
		return "ground2"
	case ZLevel_Ground3:
		return "ground3"
	default:
		panic(fmt.Sprintf("error: unknown ZLevel zl=%d", zl))
	}
}
