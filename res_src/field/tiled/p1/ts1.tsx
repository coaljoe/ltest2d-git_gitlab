<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.09.12" name="ts1" tilewidth="28" tileheight="28" tilecount="92" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <terraintypes>
  <terrain name="g0" tile="0"/>
  <terrain name="s0" tile="0"/>
 </terraintypes>
 <tile id="0">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l1_01.png"/>
 </tile>
 <tile id="1">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_01.png"/>
 </tile>
 <tile id="2">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_e_01.png"/>
 </tile>
 <tile id="3">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_n_01.png"/>
 </tile>
 <tile id="4">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_ne_01.png"/>
 </tile>
 <tile id="5">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_nw_01.png"/>
 </tile>
 <tile id="6">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_s_01.png"/>
 </tile>
 <tile id="7">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_se_01.png"/>
 </tile>
 <tile id="8">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_sw_01.png"/>
 </tile>
 <tile id="9">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_tip_ne_01.png"/>
 </tile>
 <tile id="10">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_tip_nw_01.png"/>
 </tile>
 <tile id="11">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_tip_se_01.png"/>
 </tile>
 <tile id="12">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_tip_sw_01.png"/>
 </tile>
 <tile id="13">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l2_w_01.png"/>
 </tile>
 <tile id="14">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_01.png"/>
 </tile>
 <tile id="15">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_e_01.png"/>
 </tile>
 <tile id="16">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_n_01.png"/>
 </tile>
 <tile id="17">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_ne_01.png"/>
 </tile>
 <tile id="18">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_nw_01.png"/>
 </tile>
 <tile id="19">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_s_01.png"/>
 </tile>
 <tile id="20">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_se_01.png"/>
 </tile>
 <tile id="21">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_sw_01.png"/>
 </tile>
 <tile id="22">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_tip_ne_01.png"/>
 </tile>
 <tile id="23">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_tip_nw_01.png"/>
 </tile>
 <tile id="24">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_tip_se_01.png"/>
 </tile>
 <tile id="25">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_tip_sw_01.png"/>
 </tile>
 <tile id="26">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l3_w_01.png"/>
 </tile>
 <tile id="27" terrain="1,1,1,1">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_01.png"/>
 </tile>
 <tile id="28" terrain="1,,1,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_e_01.png"/>
 </tile>
 <tile id="29" terrain=",,1,1">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_n_01.png"/>
 </tile>
 <tile id="30" terrain="1,,1,1">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_ne_01.png"/>
 </tile>
 <tile id="31" terrain=",1,1,1">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_nw_01.png"/>
 </tile>
 <tile id="32" terrain="1,1,,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_s_01.png"/>
 </tile>
 <tile id="33" terrain="1,1,1,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_se_01.png"/>
 </tile>
 <tile id="34" terrain="1,1,,1">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_sw_01.png"/>
 </tile>
 <tile id="35" terrain=",,1,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_tip_ne_01.png"/>
 </tile>
 <tile id="36" terrain=",,,1">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_tip_nw_01.png"/>
 </tile>
 <tile id="37" terrain="1,,,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_tip_se_01.png"/>
 </tile>
 <tile id="38" terrain=",1,,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_tip_sw_01.png"/>
 </tile>
 <tile id="39" terrain=",1,,1">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l4_w_01.png"/>
 </tile>
 <tile id="40" terrain="0,0,0,0">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_01.png"/>
 </tile>
 <tile id="41" terrain="0,,0,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_e_01.png"/>
 </tile>
 <tile id="42" terrain=",,0,0">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_n_01.png"/>
 </tile>
 <tile id="43" terrain="0,,0,0">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_ne_01.png"/>
 </tile>
 <tile id="44" terrain=",0,0,0">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_nw_01.png"/>
 </tile>
 <tile id="45" terrain="0,0,,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_s_01.png"/>
 </tile>
 <tile id="46" terrain="0,0,0,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_se_01.png"/>
 </tile>
 <tile id="47" terrain="0,0,,0">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_sw_01.png"/>
 </tile>
 <tile id="48" terrain=",,0,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_tip_ne_01.png"/>
 </tile>
 <tile id="49" terrain=",,,0">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_tip_nw_01.png"/>
 </tile>
 <tile id="50" terrain="0,,,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_tip_se_01.png"/>
 </tile>
 <tile id="51" terrain=",0,,">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_tip_sw_01.png"/>
 </tile>
 <tile id="52" terrain=",0,,0">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l5_w_01.png"/>
 </tile>
 <tile id="53">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_01.png"/>
 </tile>
 <tile id="54">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_e_01.png"/>
 </tile>
 <tile id="55">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_n_01.png"/>
 </tile>
 <tile id="56">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_ne_01.png"/>
 </tile>
 <tile id="57">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_nw_01.png"/>
 </tile>
 <tile id="58">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_s_01.png"/>
 </tile>
 <tile id="59">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_se_01.png"/>
 </tile>
 <tile id="60">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_sw_01.png"/>
 </tile>
 <tile id="61">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_tip_ne_01.png"/>
 </tile>
 <tile id="62">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_tip_nw_01.png"/>
 </tile>
 <tile id="63">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_tip_se_01.png"/>
 </tile>
 <tile id="64">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_tip_sw_01.png"/>
 </tile>
 <tile id="65">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l6_w_01.png"/>
 </tile>
 <tile id="66">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_01.png"/>
 </tile>
 <tile id="67">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_e_01.png"/>
 </tile>
 <tile id="68">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_n_01.png"/>
 </tile>
 <tile id="69">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_ne_01.png"/>
 </tile>
 <tile id="70">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_nw_01.png"/>
 </tile>
 <tile id="71">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_s_01.png"/>
 </tile>
 <tile id="72">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_se_01.png"/>
 </tile>
 <tile id="73">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_sw_01.png"/>
 </tile>
 <tile id="74">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_tip_ne_01.png"/>
 </tile>
 <tile id="75">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_tip_nw_01.png"/>
 </tile>
 <tile id="76">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_tip_se_01.png"/>
 </tile>
 <tile id="77">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_tip_sw_01.png"/>
 </tile>
 <tile id="78">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l7_w_01.png"/>
 </tile>
 <tile id="79">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_01.png"/>
 </tile>
 <tile id="80">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_e_01.png"/>
 </tile>
 <tile id="81">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_n_01.png"/>
 </tile>
 <tile id="82">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_ne_01.png"/>
 </tile>
 <tile id="83">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_nw_01.png"/>
 </tile>
 <tile id="84">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_s_01.png"/>
 </tile>
 <tile id="85">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_se_01.png"/>
 </tile>
 <tile id="86">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_sw_01.png"/>
 </tile>
 <tile id="87">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_tip_ne_01.png"/>
 </tile>
 <tile id="88">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_tip_nw_01.png"/>
 </tile>
 <tile id="89">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_tip_se_01.png"/>
 </tile>
 <tile id="90">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_tip_sw_01.png"/>
 </tile>
 <tile id="91">
  <image width="28" height="28" source="../../../../res/field/tiles/winter/l8_w_01.png"/>
 </tile>
</tileset>
