package main_module

import (
	ps "kristallos.ga/lib/pubsub"
	"kristallos.ga/lib/sr"
	"bytes"
	"encoding/gob"
	"fmt"
	//"github.com/veandco/go-sdl2/sdl"
	"io"
	//"io/ioutil"
	"math/rand"
	"os"
)

type Field struct {
	w, h      int
	aspect    float64
	cells     [][]Cell
	hm        HeightMap
	generated bool
	spawned   bool
	layers    []*FieldLayer
	fg        *FieldGen
	ft        *FieldTool
	view      *FieldView
}
func (f *Field) W() int { return f.w }
func (f *Field) H() int { return f.h }
func (f *Field) Aspect() float64 { return f.aspect }
func (f *Field) Hm() HeightMap { return f.hm }
func (f *Field) GetHm() *HeightMap { return &f.hm } // ref ?

func newField() *Field {
	f := &Field{
		hm:     newHeightMap(),
		layers: make([]*FieldLayer, 0),
	}
	f.ft = newFieldTool(f)
	sub(ev_mouse_button_event, f.onMouseButtonEvent)
	return f
}

//func (f *Field) onMouseButtonEvent(t sdl.MouseButtonEvent) {
func (f *Field) onMouseButtonEvent(ev *ps.Event) {
	if !f.generated {
		return
	}
	/*
			t := ev.Data.(*sdl.MouseButtonEvent)
			if t.Button == sdl.BUTTON_LEFT && t.State == sdl.PRESSED {
				//cx, cy := vars.mx/cell_size, vars.my/cell_size
		        mcx, mcy := game.vp.mcx, game.vp.mcy
		        p("mcx:", mcx, "mcy:", mcy)
				f.view.removeTile(mcx, mcy)
			}
	*/
}

func (f *Field) spawn() {
	_log.Inf("%F")

	if !f.generated {
		pp("field wasn't generated")
	}
	
	if f.spawned {
		p("warning: field was already spawned, do nothing.")
		return
	}
	
    // XXX move?
    // TODO: add loading of fows on game load?
    //_FowSys.generateFows(w, h)
    game.fowsys.generateFows(f.w, f.h)
    //pp(game.fowsys.fows)

	
	// TODO: add loading of fows on game load?
	f.view = newFieldView(f)
	f.view.spawn()
	
	f.spawned = true
}

func (f *Field) size() (int, int) {
	return f.w, f.h
}

// Set new size
func (f *Field) setSize(w, h int) {
	_log.Inf("%F w:", w, "h:", h)

	f.w = w
	f.h = h
	f.aspect = float64(f.w) / float64(f.h)

	// Generate cells
	f._generateCells()
}

// Generate only heightmap
func (f *Field) generateHM(hmW, hmH int, hmOpt HeightMapOptions) {
	f.hm.generate(hmW, hmH, hmOpt)
	f.hm.saveColor(vars.tmpDir + "/gui_hm_color.png")
}

// General field generate function.
func (f *Field) generate(w, h int, hmW, hmH int,
	hmOpt HeightMapOptions, fgOpt FieldGenOptions) {

	f.setSize(w, h)

	f.fg = newFieldGen(f, fgOpt)

	// Generate heightmap
	f.hm.generate(hmW, hmH, hmOpt)

	//f.hmScaled.generate(f.w, f.h, hmOpt)
	//f.hmScaled.clear()

	f.updateZValues()

	// Debug
	f.hm.save(vars.tmpDir+"/hm.png", false)
	f.hm.save(vars.tmpDir+"/hm_orig.png", true)
	f.hm.saveTxt(vars.tmpDir + "/hm.txt")
	f.hm.saveColor(vars.tmpDir + "/hm_color.png")
	//pp(2)

	//f.hmScaled.save(vars.tmpDir + "/hm_scaled.png")
	//f.hmScaled.saveTxt(vars.tmpDir + "/hm_scaled.txt")

	f.ft.fixMalformedZLevels()

	//f.generateRivers()

	f.updateTileIds()
	
	f.generateShores()

	f.generated = true

	// Generate extra objects and such
	//generateFieldObjects(f, fgOpt)
	//generateFieldTerrainFeatures()
	//generateFieldCampSites()
	//generateFieldZones(f, fgOpt)
	//f.fg.generateOreMines()
	err, _ := f.fg.generateFieldAreas()
	if err != nil {
		panic(err)
	}
	
	//game.fowsys.generateFows(w, h)
	//pp(game.fowsys.fows)

	// Attempt to update old hm.data with updated cell.z values
	//p(f.hm.data)

	for y := 0; y < f.hm.h; y++ {
		for x := 0; x < f.hm.w; x++ {
			z := f.cells[x][y].z
			//l := f.hm.ztol(int(z))
			//f.hm.data[x][y] = l
			f.hm.data[x][y] = int(z)
		}
	}

	//println()
	//pp(f.hm.data)

	// Debug
	f.hm.save(vars.tmpDir+"/hm_fixed.png", false)
	f.hm.saveTxt(vars.tmpDir + "/hm_fixed.txt")

	if fgOpt.checkZlevels {
		f.check()
	}

	// Add field layers
	//f.layers = append(

	//game.gui.gameSheet.updateMinimapData()
}

func (f *Field) generateRivers() {
	//for x := 0;
	startPoint := CPos{0, 5}
	endPoint := CPos{10, 5}
	//endPoint := CPos{10, 10}
	curPoint := startPoint
	path := game.pathfinder.pathfind(startPoint, endPoint)
	//path := pathfind_ex(startPoint, endPoint, true)
	p(path)
	//node := path.popLastNode()
	//p(path)
	//p(node)
	//pp(2)
	//pp(path)
	for curPoint != endPoint {
		/*
			deg := degBetweenI(curPoint.x, curPoint.y, endPoint.x, endPoint.y)
			p(deg, curPoint)
			//pp(3)
			if deg == 0 {
				curPoint.x += 1
			} else {
				pp("unknown deg:", deg)
			}
		*/
		p(curPoint)
		node := path.pop()
		curPoint = CPos{node.x, node.y}

		// Mark tile as river
		c := f.GetCell(curPoint.x, curPoint.y)
		c.river = RiverState_River
		c.tileId = -1
	}
	//pp(2)
}

func (f *Field) generateShores() {
	_log.Dbg("%F")
	minLength := 4 // (2 - passable shore, 2 - corners)
	maxLength := 6 // (4 - passable shore, 2 - corners)
	// Distribution amount
	chanceAmt := 0.5
	//chanceAmt := 0.2
	//chanceAmt := 0.0

	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			c := &f.cells[x][y]
			if c.z != ZLevel_Ground0 {
				continue
			}

			// Traverse up
			traversedCells := make([]*Cell, 0)
			count := 0
			for i := y; i >= 0; i-- {
				p("i:", i)
				c2 := &f.cells[x][i]
				rightCell := f.GetCell(x+1, i)
				leftCell := f.GetCell(x-1, i)
				if c2.z == c.z && (c2.tileId == 36 || c2.tileId == 28) {
					if c2.tileId == 36 && !rightCell.Walkable() {
						//pp(2)
						//p("break")
						break
					}
					if c2.tileId == 28 && !leftCell.Walkable() {
						break
					}
					count++
					traversedCells = append(traversedCells, c2)
				} else {
					break
				}
			}
			p("count:", count, x, y)

			//chanceAmt := 0.1
			chance := rand.Float64() > (1.0 - chanceAmt)
			// TODO: add break chance?

			addedShores := false
			if (count >= minLength) && chance {
				//pp(traversedCells)
				for j, xc := range traversedCells {
					p("set shore at idx:", j)
					xc.shore = ShoreState_Shore
					addedShores = true
				}

				//pdump(traversedCells)
			}

			if addedShores {
				lastBreakIdx := 0
				_ = lastBreakIdx
				for j := 0; j < len(traversedCells)-1; j++ {
					xc := traversedCells[j]
					xcNext := traversedCells[j+1]
					n := minLength + rand.Intn((maxLength-minLength)+1)
					//n := maxLength
					//if j%(n+1) == 0 && j != 0 {
					//if j%(n+1) == 0 {
					if j%(n) == 0 {
						//if j - lastBreakIdx >= minLength {
						{
							p("break shore at idx:", j)
							xc.shore = ShoreState_Corner
							if j > 0 {
								xcNext.shore = ShoreState_Corner
							}
							lastBreakIdx = j
						}
					}
					j += n
				}

				/*
					// Finally
					if lastBreakIdx != len(traversedCells) {
						p("Z", lastBreakIdx, len(traversedCells) - minLength, len(traversedCells))
						//if lastBreakIdx < len(traversedCells) - minLength {
						if true {
							for j := lastBreakIdx; j < len(traversedCells); j++ {
								traversedCells[j].shore = false
								p("unshore idx:", j)
							}
							//pp(2)
						}
					}
				*/
			}

			/*
				if x == 126 && y == 70 {
					pp(2)
				}
			*/

			/*
				if x == 66 && y == 120 {
					pp(2)
				}
			*/

		}
	}
	//pdump(f.cells)
	_log.Dbg("%F done")
}

// Check field manually
func (f *Field) check() {
	//zlevel := ZLevel_Ground3
	//ok, x, y, dir := f.checkMalformedZLevels(zlevel)
	ok, x, y, dir := f.ft.checkMalformedZLevels(1453, true)
	if !ok {
		f.dumpZLevels()
		pp("error: malformed z levels at x:", x, "y:", y, "dir:", dir)
	} else {
		p("field check ok")
	}
}

// XXX FIXME should return pointers?
func (f *Field) GetCells() []*Cell {
	ret := make([]*Cell, f.w*f.h)
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			//p(x, y, f.w, f.h, len(ret), len(f.cells))
			ret[y*f.w+x] = &f.cells[x][y]
			//pp("herp")
			//if f.cells[x][y].z > 0 {
			//	pp("derp")
			//}
		}
	}
	return ret
}

// Get cell (wrapped coords version)
// accepts negative coords
func (f *Field) GetCell(x, y int) *Cell {
	//p("Field.getCell", x, y)
	xw := wrapCoordX(x)
	yw := wrapCoordY(y)
	//p(game.field.w, game.field.h)
	//p(xw, yw)
	cell := &f.cells[xw][yw]
	return cell
}

// Check if field has specified coords
func (f *Field) hasCoords(cx, cy int) bool {
	if cx >= 0 && cx < f.w && cy >= 0 && cy < f.h {
		return true
	}
	return false
}

// Check if field has specified coords and panic
func (f *Field) checkCoords(cx, cy int) {
	if !f.hasCoords(cx, cy) {
		pp("check coords failed: coords:", cx, cy)
	}
}

// Cell height at x, y.
func (f *Field) cellHeightAt(x, y int) ZLevel {
	return f.cells[x][y].z
}

// Heightmap height at x, y.
func (f *Field) heightMapHeightAt(x, y int) int {
	return f.hm.data[x][y]
}

func (f *Field) getCellNeighbours(cx, cy int, includeCentralCell bool) []Cell {
	if !includeCentralCell {
		return f.getCellNeighboursDepth(cx, cy, 1)
	} else {
		cells := f.getCellNeighboursDepth(cx, cy, 1)
		centralCell := f.GetCell(cx, cy)
		// Insert
		//p_("cells before:", cells)
		i := 4
		cells = append(cells[:i], append([]Cell{*centralCell}, cells[i:]...)...)
		//p_("cells after:", cells)
		return cells
	}
	/*
		r := make([]Cell, 0)

		cx, cy := p.x, p.y
		if cx < 0 || cy < 0 {
			pp("cx or cy is less than zero;", cx, cy)
		}

		next := func(x, y int) {
			// Filter negative
			if x < 0 || y < 0 {
				return
			}
			c := f.cells[x][y]
			r = append(r, c)
		}

		next(cx-1, cy+1) // Top Left
		next(cx, cy+1)   // Top Center
		next(cx+1, cy+1) // Top Right

		next(cx-1, cy) // Center Left
		next(cx+1, cy) // Center Right

		next(cx-1, cy-1) // Bottom Left
		next(cx, cy-1)   // Bottom Center
		next(cx+1, cy-1) // Bottom Right

		return r
	*/
}

// Get rectangular slice of cells.
func (f *Field) getRectSlice(cx, cy, w, h int) []Cell {
	//p("field.getRectSlice: cx:", cx, "cy:", cy, "w:", w, "h:", h)
	if w < 0 || h < 0 {
		pp("width or height cannot be negative:", w, h)
	}
	debug := false
	//ret := make([]Cell, w*h)
	ret := make([]Cell, 0)
	if debug {
		p(len(ret), ret)
	}
	for y := cy; y < h+cy; y++ {
		for x := cx; x < w+cx; x++ {
			if debug {
				p("->", x, y)
			}
			/*
				if x < 0 || y < 0 {
					// Skip
					continue
				}
			*/
			//ret = append(ret, f.cells[x][y])
			ret = append(ret, *f.GetCell(x, y))
			//ret[x*w+y] = f.cells[cx+x][cy+y]
		}
	}
	if debug {
		p(len(ret), ret)
	}
	/*
		// Will work only with *nil version
		if Debug && len(ret) != w*h {
			pp("bad len:", len(ret))
		}
	*/
	return ret
}

// Get rectangular cell's neighbours
func (f *Field) getCellNeighboursDepth(cx, cy int, depth int) []Cell {
	//ret := make([]Cell, 0)
	/*
		ns := f.getCellNeighbours(p)
		for _, n := range ns {
			_ = n
			//nns := f.get
		}
	*/
	if depth < 1 {
		pp("depth should be > 0;", depth)
	}
	/*
		if p.x < 0 || p.y < 0 {
			pp("cell pos should be zero or positive;", p)
		}
	*/
	size := 3 + ((depth - 1) * 2)
	//println("size:", size)
	//ret := f.getRectSlice(cx-1, cy-1, size, size) // XXX bug
	ret := f.getRectSlice(cx-depth, cy-depth, size, size)
	//p_("ret before:", ret)
	// Remove central cell
	//i := size / 2 // XXX BAD
	i := len(ret) / 2
	//p_("i:", i, "size:", size)
	//p("drop:", ret[i])
	ret = append(ret[:i], ret[i+1:]...)
	//p_("ret after:", ret)

	return ret
}

func (f *Field) isCellOpen(x, y int) bool {
	//if f.cells[x][y].open {
	c := f.GetCell(x, y)
	if c.open {
		return true
	} else {
		return false
	}
}

func (f *Field) _generateCells() {
	_log.Inf("%F")

	w, h := f.w, f.h

	cells := make([][]Cell, w) // Rows
	for i := range cells {
		cells[i] = make([]Cell, h) // Cols
	}

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			c := newCell()
			c.open = true
			c.z = 0
			c.tileId = -1
			//tileId: 4,
			c.cx = x // Redundant
			c.cy = y // Redundant
			cells[x][y] = c
		}
	}

	f.cells = cells
}

// Update cells z values from hm
func (f *Field) updateZValues() {
	_log.Inf("%F")

	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			//z := f.hm.data[x*2][y*2] // Fixme: use average from nearest cells
			z := f.hm.data[x][y]
			//z := 0

			/*
				zVals := make([]int, 0)
				for i := 0; i < f.hmScale; i++ {
					for j := 0; j < f.hmScale; j++ {
						s := f.hmScale
						zVals = append(zVals, f.hm.data[x*s+i][y*s+j])
					}
				}
				zSum := 0
				for _, v := range zVals {
					zSum += v
				}
				zAvg := zSum / len(zVals)

				p(zVals, zSum, zAvg, f.hm.data[x][y])

				z := zAvg
			*/

			//p(z)
			cell := &f.cells[x][y]
			cell.z = ZLevel(z)
			//cell.z = ZLevel_Sea0
			//p("cell.z=", cell.z)

			//f.hmScaled.data[x][y] = z
		}
	}
	//f.cells[f.w/2][f.h/2].z = 1
	//f.cells[f.w/2][f.h/2].tileId = 0
	//f.cells[f.w/2][f.h/2].tileId = 15
	//pp(2)
}

// Generate tile ids
func (f *Field) updateTileIds() {
	_log.Inf("%F")

	autotile := true
	if autotile {
		f.autotile(0, 0, f.w, f.h, true)
	}

}

func (f *Field) cleanZLevels() {
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			f.cells[x][y].z = 0
		}
	}
}

func (f *Field) dumpZLevels() {
	p("zs:")
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			fmt.Printf("%4d", f.cells[x][y].z)
		}
		println()
	}
}

func (f *Field) saveFile(path string) {

	saveData, xerr := f.saveData()
	if xerr != nil {
		panic(xerr)
	}

	fd, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		panic(err)
	}
	//pp(string(js))
	_, err = fd.Write(saveData)
	if err != nil {
		panic(err)
	}

	if err := fd.Sync(); err != nil {
		panic(err)
	}
	fd.Close()
}

func (f *Field) saveData() ([]byte, error) {
	// Serialize field data
	// XXX not the same as saving state?

	buf := &bytes.Buffer{}
	// Writing
	enc := gob.NewEncoder(buf)
	err := enc.Encode(f)
	if err != nil {
		panic(err)
	}

	b := buf.Bytes()

	return b, nil
}

func (f *Field) loadFile(path string) {
	_log.Dbg("%F path:", path)

	if ok, err := exists(path); !ok {
		if err != nil {
			panic(err)
		}
		pp("file not found: ", path)
	}

	/*
	dat, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	*/

	fd, err := os.Open(path)
	if err != nil {
		println("cant read file:", path)
		panic(err)
	}

	//f.loadData(dat)
	f.loadData(fd)

	// Close
	fd.Close()

	_log.Inf("done %F")
}

//func (f *Field) loadData(b []byte) {
func (f *Field) loadData(buf io.Reader) {
	_log.Dbg("%F")

	sr.DisableLog = true
	// Load field data
	//buf := bytes.NewBuffer(b)
	dec := gob.NewDecoder(buf)
	//pp(buf.Len())

	var newf *Field
	newf = &Field{}
	err := dec.Decode(newf)
	if err != nil {
		panic(err)
	}
	sr.DisableLog = false

	//f = newf
	*f = *newf

	p("field data:")
	p(f.w, f.h, f.generated)
	p(newf.w, newf.h, newf.generated)
	//pp(2)

	// Cleanup

	// Remove buildingId from cells
	// TODO: FIXME add neutral/resource building restoration
	for _, c := range f.GetCells() {
		c.buildingId = -1
	}

	for _, c := range f.GetCells() {
		if c.buildingId != -1 {
			pp(c)
		}
	}
	
	// Unit ids
	for _, c := range f.GetCells() {
		c.unitId = -1
	}

	for _, c := range f.GetCells() {
		if c.unitId != -1 {
			pp(c)
		}
	}

	// XXX fixme?
	//game.gui.gameSheet.updateMinimapData()
}

// Load from heightmap image
func (f *Field) loadImage(path string) {
	_log.Inf("%F path:", path)

	f.hm.load(path, true)

	newW, newH := f.hm.w, f.hm.h
	p("newW:", newW)
	p("newH:", newH)

	// Set new size
	f.setSize(newW, newH)

	// Regenerate tile ids
	f.updateZValues()

	// XXX fixme?: add default fieldgen
	fgOpt := newFieldGenOptions()
	f.fg = newFieldGen(f, fgOpt)

	f.ft.fixMalformedZLevels()

	f.updateTileIds()

	// Check Z levels
	f.check()

	// XXX
	f.generated = true

	_log.Inf("done %F")
}

func (f *Field) update(dt float64) {
	//p("field.update")
	//f.view.draw()
}
