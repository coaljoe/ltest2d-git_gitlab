package xgui

import (
	"github.com/veandco/go-sdl2/sdl"
)

// A widget.
type ImageBox struct {
	*Widget
	Image      *Image
	ScaleImage bool
}

func NewImageBox(rect Rect) *ImageBox {
	ib := &ImageBox{
		Widget:     NewWidget("ImageBox"),
		ScaleImage: false,
	}
	ib.rect = rect
	return ib
}

func (ib *ImageBox) SetImage(path string) {
	if ib.Image == nil {
		ib.Image = NewImage()
	}
	ib.Image.ScaleFilter = true
	ib.Image.Create(5, 5)
	ib.Image.Load(path)
}

func (ib *ImageBox) Render(r *sdl.Renderer) {
	ib.Widget.Render(r)
	defer ib.Widget.RenderChildren(r)

	if ib.Image != nil {
		absRect := ib.GetAbsoluteRect()
		x := absRect.X()
		y := absRect.Y()
		w := ib.Image.W()
		h := ib.Image.H()

		if ib.ScaleImage {
			w = ib.rect.W()
			h = ib.rect.H()
		}

		ib.Image.Render(r, x, y, w, h)
	}
}

func (ib *ImageBox) Update(dt float64) {
	ib.Widget.Update(dt)
}
