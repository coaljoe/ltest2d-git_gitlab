package xgui

import (
	"github.com/veandco/go-sdl2/sdl"
)

// Gui Context.
type Context struct {
	Renderer *sdl.Renderer
	SheetSys *SheetSys
	InputSys *InputSys
	Style    *Style
	//Font         *ft.Font
	TextRenderer *TextRenderer
}

func newContext() *Context {
	return &Context{}
}
