package xgui

import (
	"fmt"
	"os"

	//"github.com/veandco/go-sdl2/sdl"

	ttf "github.com/veandco/go-sdl2/ttf"
)

func LoadFont(path string, size int) *ttf.Font {
	var font *ttf.Font
	var err error
	//if font, err = ttf.OpenFont("res/gui/fonts/DejaVuSans.ttf", 16); err != nil {
	if font, err = ttf.OpenFont(path, size); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open font: %s\n", err)
		panic(2)
	}
	return font
}
