package xgui

import (
	//ps "kristallos.ga/lib/pubsub"
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

type WidgetI interface {
	Name() string
	Id() int
	HasParent() bool
	HasChildren() bool
	Children() []WidgetI
	SetParent(p WidgetI)
	GetAllChildren() []WidgetI
	Rect() Rect
	SetRect(v Rect)
	GetAbsoluteRect() Rect
	Hover() bool
	SetHover(v bool)
	Active() bool
	SetActive(v bool)
	Callback() func(payload interface{})
	SetCallback(cb func(payload interface{}), payload interface{})
	CallbackPayload() interface{}
	PassEvents() bool
	SetPassEvents(v bool)
	Private() bool
	Visible() bool
	Render(r *sdl.Renderer)
	Update(dt float64)
	//Widget() *Widget
}

// XXX rename to BaseWidget?
type Widget struct {
	*node            // Id is here
	bgColor          StyleOption
	Style            *Style
	rect             Rect // x, y, w, h
	border           bool // Outer border
	borderColor      Color
	borderWidth      float64
	active           bool
	hover            bool // Mouse hover
	hoverBorderColor Color
	hoverBorderWidth float64
	visible          bool
	passEvents       bool
	private          bool // Private/hidden node
	parent           WidgetI
	children         []WidgetI
	cb               func(payload interface{}) // Click callback
	cbPayload        interface{}
}

func (w *Widget) SetBgColor(c Color) { w.bgColor.set(c) }
func (w *Widget) BgColor() Color     { return *w.bgColor.value().(*Color) }

//func (w *Widget) Style() Style          { return w.style }
//func (w *Widget) SetStyle(s Style)      { w.style = s }
func (w *Widget) SetName(name string)                 { w.name = name }
func (w *Widget) Rect() Rect                          { return w.rect }
func (w *Widget) SetRect(v Rect)                      { w.rect = v }
func (w *Widget) Children() []WidgetI                 { return w.children }
func (w *Widget) Visible() bool                       { return w.visible }
func (w *Widget) SetVisible(v bool)                   { w.visible = v }
func (w *Widget) Hover() bool                         { return w.hover }
func (w *Widget) SetHover(v bool)                     { w.hover = v }
func (w *Widget) Active() bool                        { return w.active }
func (w *Widget) SetActive(v bool)                    { w.active = v }
func (w *Widget) PassEvents() bool                    { return w.passEvents }
func (w *Widget) SetPassEvents(v bool)                { w.passEvents = v }
func (w *Widget) Private() bool                       { return w.private }
func (w *Widget) Callback() func(payload interface{}) { return w.cb }
func (w *Widget) CallbackPayload() interface{}        { return w.cbPayload }

//func (w *Widget) Widget() *Widget     { return w }

func NewWidget(name string) *Widget {
	styleCopy := *ctx.Style
	w := &Widget{
		node:             newNode(name),
		bgColor:          newStyleOption(&ctx.Style.bgColor),
		Style:            &styleCopy,
		rect:             Rect{0, 0, 0, 0},
		border:           true,
		borderColor:      ctx.Style.borderColor,
		borderWidth:      ctx.Style.borderWidth,
		children:         make([]WidgetI, 0),
		active:           true,
		hover:            false,
		hoverBorderColor: ctx.Style.hoverBorderColor,
		hoverBorderWidth: ctx.Style.hoverBorderWidth,
		visible:          true,
	}
	//Sub(Ev_mouse_enter, w.EvMouseEnter)
	//Sub(Ev_mouse_out, w.EvMouseOut)
	return w
}

/*
// XXX doesn't work well?
func (w *Widget) EvMouseEnter(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != w.Id() {
		return
	}
	//pp("enter")
	w.hover = true
	//xgi.hasMouseFocus = true
}

func (w *Widget) EvMouseOut(ev *ps.Event) {
	if ev.Data.(WidgetI).Id() != w.Id() {
		return
	}
	//pp("out")
	w.hover = false
	//xgi.hasMouseFocus = false
}
*/

func (w *Widget) SetCallback(cb func(payload interface{}), payload interface{}) {
	w.cb = cb
	w.cbPayload = payload
}

func (w *Widget) HasChild(child WidgetI) bool {
	for _, ch := range w.children {
		if ch == child {
			return true
		}
	}
	return false
}

func (w *Widget) AddChild(child WidgetI) {
	if w.HasChild(w) {
		panic("already have child")
	}

	child.SetParent(w)
	w.children = append(w.children, child)
}

func (w *Widget) HasParent() bool {
	return w.parent != nil
}

func (w *Widget) SetParent(p WidgetI) {
	w.parent = p
}

func (w *Widget) HasChildren() bool {
	return len(w.children) > 0
}

// Recursively get all widget's children (deep).
func (w *Widget) GetAllChildren() (ret []WidgetI) {
	// Node: Its possible to use non-recursive
	// version using parent back-links (child-parent links).

	fmt.Println("GetAllChildren", w.Name())
	fmt.Println("ret:", ret)
	if ret == nil {
		ret = make([]WidgetI, 0)
	}

	/*
		var find_rec func(cw WidgetI) []WidgetI
		find_rec = func(cw WidgetI) []WidgetI {
			if !cw.HasChildren() {
				fmt.Println("no children:", cw.Name(), cw)
				ret = append(ret, cw)
			} else {
				for _, c := range cw.Children() {
					//ret = append(ret, c.GetAllChildren()...)
					//d := find_rec()
					//cc := c.(*Widget)
					fmt.Println("process children:", c.Name(), c)
					ret = append(ret, c)
					d := find_rec(c)
					fmt.Println("d:", d)
					//ret = append(ret, d...)
					//ret = append(ret, find_rec(c)...)
				}
			}

			fmt.Println("returning ret:", ret)

			return ret
		}

		find_rec(w)
	*/
	var _find_rec func(cw WidgetI)
	_find_rec = func(cw WidgetI) {
		//fmt.Println("_find_rec:", cw.Name(), cw)
		// Skip the top node
		if cw != w {
			ret = append(ret, cw)
		}
		// Traverse all children recursively
		for _, c := range cw.Children() {
			//fmt.Println("process children:", c.Name(), c)
			_find_rec(c)
		}
	}

	_find_rec(w)

	//fmt.Println("ret:", ret)

	// Reverse ret
	/*
		a := ret
		for i, j := 0, len(a)-1; i < j; i, j = i+1, j-1 {
			a[i], a[j] = a[j], a[i]
		}
		ret = a

		fmt.Println("ret (after reverse):", ret)
	*/

	return ret
}

func (w *Widget) GetAllPublicChildren() []WidgetI {
	ret := make([]WidgetI, 0)
	allChildren := w.GetAllChildren()
	for _, ch := range allChildren {
		if !ch.Private() {
			ret = append(ret, ch)
		}
	}
	return ret
}

// Depth level.
func (w *Widget) Level() int {
	lev := 0
	cw := w
	for {
		if cw.HasParent() {
			lev += 1
			cw = cw.parent.(*Widget)
		} else {
			break
		}
	}
	return lev
}

// Return rect in absolute position relative to its parent.
func (w *Widget) GetAbsoluteRect() Rect {
	if !w.HasParent() {
		return w.rect
	}
	//parentRect := w.parent.Rect()
	parentRect := w.parent.(*Widget).GetAbsoluteRect() // recursively
	/*
		r := Rect{
			parentRect.X() + (w.rect.X() * parentRect.W()),
			parentRect.Y() + (w.rect.Y() * parentRect.H()),
			w.rect.W() * parentRect.W(), w.rect.H() * parentRect.H()}
	*/

	// Expand -1 size hacks
	xw, xh := 0, 0
	if w.rect.W() == -1 {
		xw = parentRect.W()
	} else {
		xw = w.rect.W()
	}

	if w.rect.H() == -1 {
		xh = parentRect.H()
	} else {
		xh = w.rect.H()
	}

	r := Rect{
		parentRect.X() + w.rect.X(),
		parentRect.Y() + w.rect.Y(),
		//parentRect.W() + w.rect.W(),
		//parentRect.H() + w.rect.H()}
		//w.rect.W(),
		//w.rect.H()}
		xw,
		xh}

	//fmt.Printf("-- rect for widget name: %s, parent name: %s, id: %d, p.id: %d\n", w.name, w.parent.Name(), w.Id(), w.parent.Id())
	//fmt.Println("rect:", w.rect, "parent rect:", parentRect, "absolute rect:", r)

	return r
}

func (w *Widget) MarkAsPrivate() {
	w.private = true
}

func (w *Widget) String() string {
	return fmt.Sprintf("Widget<id=%d name=%s>", w.id, w.Name())
}

func (w *Widget) Render(r *sdl.Renderer) {
	if !w.visible {
		return
	}
	//fmt.Printf("Widget.Render; name: %s, id: %d\n", w.Name(), w.Id())

	if w.Style.OuterBorderWidth > 0 {
		absRect := w.GetAbsoluteRect()
		xw := w.Style.OuterBorderWidth

		/*
			rx := absRect.X() - xw
			ry := absRect.Y() - xw
			rw := absRect.W() + xw*2
			rh := absRect.H() + xw*2

			var rectangle sdl.Rect
			rectangle.X = int32(rx)
			rectangle.Y = int32(ry)
			rectangle.W = int32(rw)
			rectangle.H = int32(rh)
		*/
		rx := absRect.X() - xw/2
		ry := absRect.Y() - xw/2
		rw := absRect.W() + (xw/2)*2
		rh := absRect.H() + (xw/2)*2

		c := w.Style.OuterBorderColor
		sdlC := sdl.Color{uint8(c.R()), uint8(c.G()), uint8(c.B()), 255}
		//r.SetDrawColor(uint8(c.R()*255), uint8(c.G()*255), uint8(c.B()*255), 255)
		//r.FillRect(&rectangle)
		drawRectangleColorWidth(r, rx, ry, rw, rh, sdlC, xw)
	}

	/*
		// Call children render code
		for _, ch := range w.children {
			p("Widget.Render: rendering child:", ch)
			ch.Render(r)
		}
	*/

	//println("Widget.Render done")
}

func (w *Widget) RenderChildren(r *sdl.Renderer) {
	//println("Widget.RenderChildren")
	for _, ch := range w.children {
		//p("Widget.Render: rendering child:", ch)
		ch.Render(r)
	}
	//println("done Widget.RenderChildren")
}

var _ = `
func (w *Widget) Render(r *rx.Renderer) {
	if !w.visible {
		return
	}
	fmt.Printf("Widget.Render; name: %s\n", w.Name())

	//if w.name == "root":
	//	continue
	// Self render code
	//fmt.Println("widget render; name:", w.name, "level:", w.Level())

	rect := w.GetAbsoluteRect()

	// Border
	bcolor := w.borderColor
	bwidth := w.borderWidth

	if w.hover {
		bcolor = w.hoverBorderColor
		bwidth = w.hoverBorderWidth
	}

	brect := rect
	brect[0] -= bwidth / 2
	brect[1] -= bwidth / 2
	brect[2] += bwidth / 2
	brect[3] += bwidth / 2

	if true {

		//r.RenderQuad(p.rect.X(), p.rect.Y(), p.rect.Width(), p.rect.Height(), 0)
		rx.DrawBegin()

		if w.border {
			rx.DrawSetColor(bcolor.R(), bcolor.G(), bcolor.B())
			//rx.DrawSetLineWidth(bwidth)
			rx.DrawSetLineWidth(float64(UnitsToPx(bwidth)))
			rx.DrawRectV(Vec4(brect))
		}

		if w.bgColor.isSet() {
			// Bg
			bgColor := w.BgColor()

			rx.DrawSetColor(bgColor.R(), bgColor.G(), bgColor.B())
			rx.DrawQuadV(w.rect.ToVec4())
			//pp(rect)
			//rx.DrawQuadV(rect.ToVec4())
			//rx.DrawQuadV(Vec4{0.5, 0.5, 0.25, 0.25})
		}

		rx.DrawResetColor()

		rx.DrawEnd()
	}

	/*
		fmt.Println(w.GetAbsoluteRect(), w.Rect())
		if w.HasParent() {
			fmt.Println("parent:", w.parent.(*Widget).name)
		}
	*/
	//step := float32(w.Level()) / 10.0
	//fmt.Println("step:", step)
	//gl.PushMatrix()
	//gl.Translatef(0, 0, float32(w.Level()))
	//gl.Translatef(0, 0, step)
	//gl.Translatef(0, 0, +.5)

	// Call children render code
	for _, ch := range w.children {
		ch.Render(r)
	}

	//gl.PopMatrix()

	println("Widget.Render done")
}
`

func (w *Widget) Update(dt float64) {
	//println("Widget.Update; name:", w.Name(), "id:", w.Id())
	//panic(2)
}
