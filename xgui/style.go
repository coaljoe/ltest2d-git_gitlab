package xgui

import (
	"github.com/veandco/go-sdl2/sdl"

	ttf "github.com/veandco/go-sdl2/ttf"
)

// XXX FIXME
var DebugFont *ttf.Font

// Style theme.
type Style struct {
	bgColor             Color //`name:bgColor`
	TextFont            *ttf.Font
	TextColor           Color
	textBgColor         Color
	borderColor         Color
	borderWidth         float64
	hoverBorderColor    Color
	hoverBorderWidth    float64
	OuterBorderWidth    int
	OuterBorderColor    Color
	PanelPatch9         *Patch9
	ButtonPatch9        *Patch9
	ButtonPressedPatch9 *Patch9
	SliderScoreBarColor Color
	Fonts               map[string]*ttf.Font
}

func NewStyle() *Style {
	// Default style

	//if font, err = ttf.OpenFont("res/gui/fonts/DejaVuSans.ttf", 16); err != nil {
	//font := LoadFont("res/gui/fonts/DejaVuSans.ttf", 18)
	//font := LoadFont("res/gui/fonts/FreeSans.ttf", 18)
	//font := LoadFont("res/gui/fonts/FreeMonoBold.ttf", 18)
	//font := LoadFont("res/gui/fonts/unifont.ttf", 20)
	//font := LoadFont("res/gui/fonts/LinLibertine_RI_G.ttf", 22)
	//font := LoadFont("res/gui/fonts/LinLibertine_R_G.ttf", 23)
	//font := LoadFont("res/gui/fonts/LinLibertine_Rah.ttf", 23)
	//font := LoadFont("res/gui/fonts/LinLibertine_Rah.ttf", 22)
	//font := LoadFont("res/gui/fonts/IBMPlexSans-Regular.ttf", 18)
	//font := LoadFont("res/gui/fonts/IBMPlexSans-Regular.ttf", 18)
	//font := LoadFont("res/gui/fonts/IBMPlexSans-Medium.ttf", 18)
	//font := LoadFont("res/gui/fonts/odin-rounded.light.ttf", 22)
	//font := LoadFont("res/gui/fonts/LinLibertine_RZI_G.ttf", 22)
	//font := LoadFont("res/gui/fonts/mplus-1c-regular.ttf", 18)
	font := LoadFont("res/gui/fonts/mplus-1mn-regular.ttf", 20)
	//DebugFont = LoadFont("res/gui/fonts/DejaVuSans.ttf", 16)
	DebugFont = LoadFont("res/gui/fonts/TerminusTTF.ttf", 12)

	s := &Style{
		bgColor:  Color{12, 12, 25, 25},
		TextFont: font,
		//textFont:         newFont(rx.ConfGetResPath()+"/gui/fonts/DejaVuSans.ttf", 16),
		//textColor:           Color{102, 102, 52, 255}, //ColorRed, //ColorBlack,
		//textColor:           Color{102, 102, 52, 255}, //ColorRed, //ColorBlack,
		//TextColor:           Color{50, 50, 50, 255}, //ColorRed, //ColorBlack,
		TextColor:        Color{50, 60, 70, 255}, //ColorRed, //ColorBlack,
		textBgColor:      ColorWhite,
		borderWidth:      PxW(2), // XXX use PxW and PxH?
		borderColor:      Color{25, 102, 102, 255},
		hoverBorderColor: Color{0, 0, 204},
		hoverBorderWidth: PxW(5),
		OuterBorderWidth: 0,
		//OuterBorderWidth:    2,
		OuterBorderColor:    ColorBlack,
		PanelPatch9:         NewPatch9(),
		ButtonPatch9:        NewPatch9(),
		ButtonPressedPatch9: NewPatch9(),
		//SliderScoreBarColor: Color{0, 128, 128, 255},
		//SliderScoreBarColor: Color{54, 117, 136, 255},
		SliderScoreBarColor: Color{122, 134, 146, 255},
		Fonts:               make(map[string]*ttf.Font),
	}

	// Add extra fonts
	boldFont := LoadFont("res/gui/fonts/mplus-1mn-bold.ttf", 20)
	s.Fonts["text_font_bold"] = boldFont
	boldFont2 := LoadFont("res/gui/fonts/mplus-1p-bold.ttf", 22)
	s.Fonts["text_font_bold2"] = boldFont2

	//pp(s.Fonts)

	/*
		s.panelPatch9.Load("tmp/gui_patch9/button9.9.png")
		s.panelPatch9.Rects = [9]sdl.Rect{}
		s.panelPatch9.Rects[0] = sdl.Rect{0, 0, 3, 10} // Top-left
		s.panelPatch9.Rects[1] = sdl.Rect{3, 0, 3, 10} // Top
		s.panelPatch9.Rects[2] = sdl.Rect{int32(s.panelPatch9.im.W() - 3), 0, 3, 10} // Top-right
		s.panelPatch9.Rects[3] = sdl.Rect{0, 3, 3, 10} // Left
		s.panelPatch9.Rects[5] = sdl.Rect{int32(s.panelPatch9.im.W() - 3), 3, 3, 10} // Right
		s.panelPatch9.Rects[6] = sdl.Rect{0, 25, 3, 3} // Bottom-left
		s.panelPatch9.Rects[7] = sdl.Rect{3, 25, 3, 3} // Bottom
		s.panelPatch9.Rects[8] = sdl.Rect{int32(s.panelPatch9.im.W() - 3), 25, 3, 3} // Bottom-right
	*/
	// Panel patch9
	//s.panelPatch9.Load("tmp/gui_patch9/panel.9.png")
	s.PanelPatch9.Load("res/gui/elements/panel.9.png")
	s.PanelPatch9.Rects = [9]sdl.Rect{}
	s.PanelPatch9.Rects[0] = sdl.Rect{0, 0, 3, 3}                                // Top-left
	s.PanelPatch9.Rects[1] = sdl.Rect{3, 0, 3, 3}                                // Top
	s.PanelPatch9.Rects[2] = sdl.Rect{int32(s.PanelPatch9.Im.W() - 3), 0, 3, 3}  // Top-right
	s.PanelPatch9.Rects[3] = sdl.Rect{0, 3, 3, 3}                                // Left
	s.PanelPatch9.Rects[4] = sdl.Rect{3, 3, 16, 16}                              // Central
	s.PanelPatch9.Rects[5] = sdl.Rect{int32(s.PanelPatch9.Im.W() - 3), 3, 3, 3}  // Right
	s.PanelPatch9.Rects[6] = sdl.Rect{0, 25, 3, 3}                               // Bottom-left
	s.PanelPatch9.Rects[7] = sdl.Rect{3, 25, 3, 3}                               // Bottom
	s.PanelPatch9.Rects[8] = sdl.Rect{int32(s.PanelPatch9.Im.W() - 3), 25, 3, 3} // Bottom-right

	// Button patch9
	//s.buttonPatch9.Load("tmp/gui_patch9/button.9.png")
	s.ButtonPatch9.Load("res/gui/elements/button.9.png")
	s.ButtonPatch9.Rects = [9]sdl.Rect{}
	s.ButtonPatch9.Rects[0] = sdl.Rect{0, 0, 3, 3}                                // Top-left
	s.ButtonPatch9.Rects[1] = sdl.Rect{3, 0, 3, 3}                                // Top
	s.ButtonPatch9.Rects[2] = sdl.Rect{int32(s.PanelPatch9.Im.W() - 3), 0, 3, 3}  // Top-right
	s.ButtonPatch9.Rects[3] = sdl.Rect{0, 3, 3, 3}                                // Left
	s.ButtonPatch9.Rects[4] = sdl.Rect{3, 3, 16, 16}                              // Central
	s.ButtonPatch9.Rects[5] = sdl.Rect{int32(s.PanelPatch9.Im.W() - 3), 3, 3, 3}  // Right
	s.ButtonPatch9.Rects[6] = sdl.Rect{0, 25, 3, 3}                               // Bottom-left
	s.ButtonPatch9.Rects[7] = sdl.Rect{3, 25, 3, 3}                               // Bottom
	s.ButtonPatch9.Rects[8] = sdl.Rect{int32(s.PanelPatch9.Im.W() - 3), 25, 3, 3} // Bottom-right

	// Button pressed patch9
	//s.buttonPressedPatch9.Load("tmp/gui_patch9/button_pressed.9.png")
	s.ButtonPressedPatch9.Load("res/gui/elements/button_pressed.9.png")
	s.ButtonPressedPatch9.Rects = [9]sdl.Rect{}
	s.ButtonPressedPatch9.Rects[0] = sdl.Rect{0, 0, 3, 3}                                // Top-left
	s.ButtonPressedPatch9.Rects[1] = sdl.Rect{3, 0, 3, 3}                                // Top
	s.ButtonPressedPatch9.Rects[2] = sdl.Rect{int32(s.PanelPatch9.Im.W() - 3), 0, 3, 3}  // Top-right
	s.ButtonPressedPatch9.Rects[3] = sdl.Rect{0, 3, 3, 3}                                // Left
	s.ButtonPressedPatch9.Rects[4] = sdl.Rect{3, 3, 16, 16}                              // Central
	s.ButtonPressedPatch9.Rects[5] = sdl.Rect{int32(s.PanelPatch9.Im.W() - 3), 3, 3, 3}  // Right
	s.ButtonPressedPatch9.Rects[6] = sdl.Rect{0, 25, 3, 3}                               // Bottom-left
	s.ButtonPressedPatch9.Rects[7] = sdl.Rect{3, 25, 3, 3}                               // Bottom
	s.ButtonPressedPatch9.Rects[8] = sdl.Rect{int32(s.PanelPatch9.Im.W() - 3), 25, 3, 3} // Bottom-right

	//pp(s.panelPatch9.Rects)
	return s
}

//////////////
// StyleOption

type StyleOption struct {
	name    string
	inherit bool
	_val    interface{}
}

func newStyleOption(val interface{}) StyleOption {
	return StyleOption{"", true, val}
}

func (so *StyleOption) set(val interface{}) {
	so.inherit = false
	so._val = val
}

func (so *StyleOption) unset() {
	so.inherit = true
	so._val = nil
}

func (so *StyleOption) value() interface{} {
	return so._val
}

func (so *StyleOption) isSet() bool {
	return so._val != nil
}
