package xgui

import (
	"kristallos.ga/lib/xlog"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"os"
)

// Globals
//var ftctx *ft.Context
var xgi *XGui
var ctx *Context

var (
	_log *xlog.Logger
)

func Init() {
	/*
		var err error
		ftctx, err = ft.Init()

		if err != nil {
			panic(err)
		}

		println("FreeType context initialized")
	*/

	err := ttf.Init()
	if err != nil {
		panic(err)
	}

	// Create logger
	_log = xlog.NewLogger("xgui", xlog.LogDebug)
	if loglev := os.Getenv("LOGLEVEL"); loglev != "" {
		_log.SetLogLevelStr(loglev)
	}
	_log.SetOutputFile("xgui.log")
	_log.SetWriteTime(true)
	_log.SetWriteFuncName(true)
}

type XGui struct {
	//Context *Context
	*Context
	// Enable throttling
	throttling bool
	// Render only Nth frame
	throttlingNth uint64
	enabled       bool
	hasMouseFocus bool
}

func (xg *XGui) Enabled() bool             { return xg.enabled }
func (xg *XGui) SetEnabled(v bool)         { xg.enabled = v }
func (xg *XGui) Throttling() bool          { return xg.throttling }
func (xg *XGui) SetThrottling(v bool)      { xg.throttling = v }
func (xg *XGui) ThrottlingNth() uint64     { return xg.throttlingNth }
func (xg *XGui) SetThrottlingNth(v uint64) { xg.throttlingNth = v }

func NewXGui(r *sdl.Renderer) *XGui {
	Init() // fixme?
	xgi = &XGui{
		enabled:       true,
		throttling:    false,
		throttlingNth: 4,
	}

	xgi.Context = newContext()

	// Set Globals
	ctx = xgi.Context

	// Set renderer
	ctx.Renderer = r

	// Add Subsystems
	ctx.SheetSys = newSheetSys()
	ctx.InputSys = newInputSys()

	// Add extra context
	ctx.Style = NewStyle() // Default style
	//ctx.Font = loadFont("res/fonts/vera.ttf")
	ctx.TextRenderer = newTextRenderer()

	//rxi = rx.Rxi()
	//rxi.Renderer().AddGuiRP(xgi)

	//.Set vars
	SetDefaultVars()
	w, h, err := r.GetOutputSize()
	if err != nil {
		panic(err)
	}
	vars.resX = int(w)
	vars.resY = int(h)

	return xgi
}

func (xg *XGui) Name() string {
	return "XGui"
}

func (xg *XGui) HasMouseFocus() bool {
	//for x := range xg.SheetSys.activeSheet.root {
	//}
	//return true
	return xg.hasMouseFocus
}

// Render the gui.
func (xg *XGui) Render(r *sdl.Renderer) {
	if !xg.enabled {
		return
	}

	// XXX Fixme: first update gui?
	//xg.Update(rxi.App.GetDt())

	//println("XGui: Render")

	sh := ctx.SheetSys.ActiveSheet()
	sh.Render(r)
}

func (xg *XGui) Update(dt float64) {
	if !xg.enabled {
		return
	}
	//println("XGui: Update")

	sh := ctx.SheetSys.ActiveSheet()
	//pdump(sh.Update)
	//pdump(sh)
	//fmt.Printf("-> %T\n", sh)
	//fmt.Printf("-> %v\n", sh)
	//fmt.Printf("-> %#v\n", sh)
	sh.Update(dt)
	//panic(2)
}
